//Server Achievements
//By Fastmapler
//Read the README if you want to be cool and intellegent.

$Server_Achievements::DisplayDebugInfo = true; 

package Server_Achievements
{
	function GameConnection::onClientEnterGame(%this) //Do this in order to get the player's achievements
	{
		parent::onClientEnterGame(%this);
		
		%this.AchievementCount = 0; //So the achievement count does not appear as "" for new players.
		
		%this.ServerAchievementReadPlayerSave();
	}
	
	function GameConnection::onClientLeaveGame(%client)
	{
		%client.ServerAchievementUpdatePlayerFile();
		
		parent::onClientLeaveGame(%client);
	}
};
activatePackage("Server_Achievements");

function GameConnection::ServerAchievementReadPlayerSave(%client)
{
	%client.AchievementCount = 0;
	%file = new FileObject(); //Get their file.
	%file.openForRead("config/Server_Achievements/Saves/" @ %client.bl_id @ ".txt");
	while(!%file.isEOF())
	{
		%line = %file.readLine();
		if(%line !$= "")
		{
			%name = getField(%line, 0);
			%progress = getField(%line, 1);
			%client.AchievementProgress[%name] = %progress;
			
			if (strPos($Server_Achievements::AchievementList,%name) == -1)
				$Server_Achievements::AchievementList = %name TAB $Server_Achievements::AchievementList;
			
			//if (%progress $= "DONE")
				//%client.AchievementCount++;
		}
	}
	%file.close();
	%file.delete();
	
	for (%i = 1; %i <= $Server_Achievements::AchievementCount; %i++)
	{
		%data = $Server_Achievements::Achievements[%i];
		
		if(%client.AchievementProgress[getField(%data,0)] $= "DONE")
				%client.AchievementCount++;
		else if (%client.AchievementProgress[getField(%data,0)] >= getField(%data,3))
			%client.AwardAchievement(%name);
	}
}

function ServerAchievementsDebug(%text) //For debugging purposes only. Feel free to set DisplayDebugInfo to true or false.
{
	if ($Server_Achievements::DisplayDebugInfo)
		echo(" " TAB "Server_Achievements -" SPC %text);
}
function AddServerAchievement(%title,%disc,%displayOptions,%reqPoints)
{
	%LwrName = strLwr(%title);//So we do not have to do strLwr again for each iteration.
	for (%i = 1; %i <= $Server_Achievements::AchievementCount; %i++)
		if(strLwr(getField($Server_Achievements::Achievements[%i],0)) $= %LwrName)
			return; //We already have this achievement registered. We don't need another one.
	
	$Server_Achievements::Achievements[$Server_Achievements::AchievementCount++] = %title TAB %disc TAB %displayOptions TAB %reqPoints;
	ServerAchievementsDebug("Registered Achievement:" SPC %title);
}

exec("./AchievementList.cs");

function ServerCmdAchievement(%client)
{
	ServerCmdAchievements(%client);
}

function ServerCmdAchievements(%client)
{
	%client.chatMessage("<color:ffffff>Achievements: (" @ %client.AchievementCount @ "/" @ $Server_Achievements::AchievementCount @ " Unlocked)");
	for (%i = 1; %i <= $Server_Achievements::AchievementCount; %i++)
	{
		%data = $Server_Achievements::Achievements[%i];
		%title = getField(%data,0);
		%disc = getField(%data,1);
		%displayOptions = getField(%data,2);
		%reqPoints = getField(%data,3);
		%text = "";
		%percent = "";
		
		if (%client.AchievementProgress[%title] $= "")
			%client.AchievementProgress[%title] = 0;
		else if (%client.AchievementProgress[%title] !$= "DONE" && (((%reqPoints != 0 && %client.AchievementProgress[%title] >= %reqPoints) || (%reqPoints == 0 && %client.AchievementProgress[%title] >= 1))))
			%client.AchievementProgress[%title] = "DONE";
			
		if(%client.AchievementProgress[%title] $= "DONE")
			%text = "<color:ffff00><bitmap:base/client/ui/ci/star>" SPC %title SPC "<color:ffffff>-" SPC %disc;
		else if (strPos(%displayOptions,"Hidden") == -1)
		{
			
			if (%reqPoints > 0)
			{
				if (strPos(%displayOptions,"DisplayPoints") > -1)
					%percent = "<color:bbbbbb>(" @ %client.AchievementProgress[%title] @ "/" @ %reqPoints @ ") ";
				if (strPos(%displayOptions,"DisplayPercent") > -1)
					%percent = %percent @ "<color:bbbbbb>(" @ mFloor((%client.AchievementProgress[%title] / %reqPoints) * 100) @ "%)";
			}
			
			if (strPos(%displayOptions,"HideDisc") > -1)
				%disc = "???";
			
			if (strPos(%displayOptions,"HideTitle") > -1)
				%title = "???";
				
			%text = "<color:555555>" @ %title SPC "<color:ffffff>-<color:aaaaaa>" SPC %disc SPC %percent;
		}

		if (%text !$= "")
			%client.chatMessage(%text);
	}
}
//
// Display Options:
//
// HideDisc - Hides the discripton
// HideTitle - Hides the title
// DisplayPoints - Displays how many points a player has for an achievement.
// DisplayPercent - Displays how many points a player has for an achievement, as a percent.
//

function GameConnection::AddAchievementProgress(%client,%name,%points)
{
	%LwrName = strLwr(%name);//So we do not have to do strLwr again for each iteration.
	for (%i = 1; %i <= $Server_Achievements::AchievementCount; %i++)
	{
		if(strLwr(getField($Server_Achievements::Achievements[%i],0)) $= %LwrName)
		{
			%data = $Server_Achievements::Achievements[%i];
			break;
		}
	}

	if (%data $= "" || %client.AchievementProgress[getField(%data,0)] $= "DONE") return;

	%client.AchievementProgress[getField(%data,0)] += %points;
	
	//%client.ServerAchievementUpdatePlayerFile();
	
	if (%client.AchievementProgress[getField(%data,0)] >= getField(%data,3))
		%client.AwardAchievement(%name);
}

function GameConnection::AwardAchievement(%client,%name) //Automatically awards our achievement, regardless of points.
{
	%LwrName = strLwr(%name);//So we do not have to do strLwr again for each iteration.
	for (%i = 1; %i <= $Server_Achievements::AchievementCount; %i++)
	{
		if(strLwr(getField($Server_Achievements::Achievements[%i],0)) $= %LwrName)
		{
			%data = $Server_Achievements::Achievements[%i];
			break;
		}
	}

	if (%data $= "" || %client.AchievementProgress[getField(%data,0)] $= "DONE") return;
	
	if (isObject(%player = %client.player))
	{
		%player.emote(winStarProjectile, 1);
		%player.playAudio(0,rewardSound);
	}
	else
	{
		%client.play2d(rewardSound);
	}
	
	
	for (%i = 0; %i < ClientGroup.getCount(); %i++)
	{
		if ((%target = ClientGroup.getObject(%i)) != %client.getSimpleName())
			%target.chatMessage("<color:00ffff><bitmap:base/client/ui/ci/star>" SPC %client.getSimpleName() SPC "<color:ffffff>got the<color:ffff00>" SPC getField(%data,0) SPC "<color:ffffff>achievement!");
	}
	
	//messageAll('',"<color:00ffff><bitmap:base/client/ui/ci/star>" SPC %client.getSimpleName() SPC "<color:ffffff>got the<color:ffff00>" SPC getField(%data,0) SPC "<color:ffffff>achievement!");
	%client.chatMessage("<color:ffffff><bitmap:base/client/ui/ci/star> You got the<color:ffff00>" SPC getField(%data,0) SPC "<color:ffffff>achievement!");
	
	%client.AchievementProgress[getField(%data,0)] = "DONE";
	%client.AchievementCount++;
	
	%client.OnAchievementGet(getField(%data,0));
	%client.ServerAchievementUpdatePlayerFile();
}

function GameConnection::OnAchievementGet(%client,%achievement)
{
	//Do nothing. Devs can use this for their own code.
}

function GameConnection::ServerAchievementUpdatePlayerFile(%client)
{
	%file = new FileObject();
	%file.openForWrite("config/Server_Achievements/Saves/" @ %client.bl_id @ ".txt");
	//export("$Server_Achievements::Achievements*","config/Server_Achievements/Saves/" @ %client.bl_id @ ".txt");
	for (%i = 1; %i <= $Server_Achievements::AchievementCount; %i++)
	{
		%name = getField($Server_Achievements::Achievements[%i],0);
		%savedAchievements = %name TAB %savedAchievements;
		if (%client.AchievementProgress[%name] $= "")
			%client.AchievementProgress[%name] = 0;
		else if (%client.AchievementProgress[%name] != 0 || %client.AchievementProgress[%name] $= "DONE")
			%file.writeLine(%name TAB %client.AchievementProgress[%name]);
	}
	
	for (%j = 0; getField($Server_Achievements::AchievementList,%j) !$= ""; %j++)
	{
		%name = getField($Server_Achievements::AchievementList,%j);
		if (strPos(%savedAchievements,%name) == -1)
		{
			if (%client.AchievementProgress[%name] $= "")
				%client.AchievementProgress[%name] = 0;
			else if (%client.AchievementProgress[%name] != 0 || %client.AchievementProgress[%name] $= "DONE")
				%file.writeLine(%name TAB %client.AchievementProgress[%name]);
		}
	}

	%file.close();
	%file.delete();
}

function ServerCmdListDataBlocks(%client,%db)
{
	%cnt = datablockGroup.getCount();
	for(%i=0;%i<%cnt;%i++)
	{
		%obj = datablockGroup.getObject(%i);
		if(%obj.getClassName() $= %db)
			echo(%obj.getName());
	}
}