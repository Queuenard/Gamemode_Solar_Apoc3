//Enemy Datablocks

//Unfleshed. Pretty much a basic zombie.

datablock PlayerData(UnfleshedHoleBot : PlayerStandardArmor)
{
	uiName = "";
	minJetEnergy = 0;
	jetEnergyDrain = 0;
	canJet = 0;
	maxItems   = 0;
	maxWeapons = 0;
	maxTools = 0;
	runforce = 60 * 90;
	maxForwardSpeed = 3;
	maxBackwardSpeed = 1.5;
	maxSideSpeed = 3;
	attackpower = 10; //What does this do?
	rideable = false;
	canRide = false;
	maxdamage = 75;//Health
	jumpSound = "";
	
	//Hole Attributes
	isHoleBot = 1;

	//Solar Apoc
	immuneToSun = false;
	immuneToFire = false;
	immuneToNight = true;
	tripleFire = false;
	
	//Spawning option
	hSpawnTooClose = 0;//Doesn't spawn when player is too close and can see it
	  hSpawnTCRange = 8;//above range, set in brick units
	hSpawnClose = 0;//Only spawn when close to a player, can be used with above function as long as hSCRange is higher than hSpawnTCRange
	  hSpawnCRange = 64;//above range, set in brick units

	hType = enemy; //Enemy,Friendly, Neutral
	  hNeutralAttackChance = 100;
	//can have unique types, nazis will attack zombies but nazis will not attack other bots labeled nazi
	hName = "Unfleshed";//cannot contain spaces
	hTickRate = 3000;
	
	//Wander Options
	hWander = 1;//Enables random walking
	  hSmoothWander = 1;
	  //hReturnToSpawn = 1;//Returns to spawn when too far //Always false
	  //hSpawnDist = 32;//Defines the distance bot can travel away from spawnbrick //Always 10000
	
	//Searching options
	hSearch = 1;//Search for Players
	  hSearchRadius = 256;//in brick units
	  hSight = 1;//Require bot to see player before pursuing
	  hStrafe = 1;//Randomly strafe while following player
	hSearchFOV = 1;//if enabled disables normal hSearch
	  hFOVRadius = 32;//max 10

	  hAlertOtherBots = 1;//Alerts other bots when he sees a player, or gets attacked

	//Attack Options
	hMelee = 1;//Melee
	  hAttackDamage = 15;//15;//Melee Damage
	hShoot = 0;
	  hWep = "gunImage";
	  hShootTimes = 4;//Number of times the bot will shoot between each tick
	  hMaxShootRange = 30;//The range in which the bot will shoot the player
	  hAvoidCloseRange = 1;//
		hTooCloseRange = 7;//in brick units
	//hHerding = 0;
	//hSound = 1;
	//hSpawnDetect = -1;//Will not spawn when user is too close and can see spawn
	

	
	//Misc options
	hAvoidObstacles = 1;
	hSuperStacker = 1;
	hSpazJump = 0;//Makes bot jump when the user their following is higher than them

	hAFKOmeter = 1;//Determines how often the bot will wander or do other idle actions, higher it is the less often he does things
	hIdle = 1;// Enables use of idle actions, actions which are done when the bot is not doing anything else
	  hIdleAnimation = 0;//Plays random animations/emotes, sit, click, love/hate/etc
	  hIdleLookAtOthers = 1;//Randomly looks at other players/bots when not doing anything else
	    hIdleSpam = 0;//Makes them spam click and spam hammer/spraycan
	  hSpasticLook = 1;//Makes them look around their environment a bit more.
	hEmote = 1;
	
	//Item Drops
	materialDrop = "Wood"; //Name of material to drop.
	materialChance = 500; //Chance to get material out of 1000.
	materialAmount = "100 300"; //Amount of materials you will get * 3.
	//First num is min, second is max. Inclusive.
	
	wolframAmount = 180; //Amount of wolfram you will get per kill, max. Use a number divisable by 3.
	
	itemDrop = HealthExpanderItem; //Item to drop. put NONE as a string for nothing.
	itemChance = 20; //Chance to get item out of 1000.
	itemFlavor = "The unfleshed droped a shiny thinket!"; //Message to killer when item is dropped.
	
	dropList = "Bones\t1\t200" NL "Leather\t1\t300" NL "Essence\t1\t200";
	
	blueprintChance = 150;
	
	score = 4;
	
	killAchievement = "Zombie Hunter";
};

//Husk. Like the unfleshed but daytime, and deadlier.

datablock PlayerData(HuskHoleBot : PlayerStandardArmor)
{
	uiName = "";
	minJetEnergy = 0;
	jetEnergyDrain = 0;
	canJet = 0;
	maxItems   = 0;
	maxWeapons = 0;
	maxTools = 0;
	runforce = 80 * 90;
	maxForwardSpeed = 5.3;
	maxBackwardSpeed = 2.65;
	maxSideSpeed = 5.3;
	attackpower = 10; //What does this do?
	rideable = false;
	canRide = false;
	maxdamage = 100;//Health
	jumpSound = "";
	
	//Hole Attributes
	isHoleBot = 1;
	
	//Spawning option
	hSpawnTooClose = 0;//Doesn't spawn when player is too close and can see it
	  hSpawnTCRange = 8;//above range, set in brick units
	hSpawnClose = 0;//Only spawn when close to a player, can be used with above function as long as hSCRange is higher than hSpawnTCRange
	  hSpawnCRange = 64;//above range, set in brick units

	hType = enemy; //Enemy,Friendly, Neutral
	  hNeutralAttackChance = 100;
	//can have unique types, nazis will attack zombies but nazis will not attack other bots labeled nazi
	hName = "Husk";//cannot contain spaces
	hTickRate = 3000;
	
	//Wander Options
	hWander = 1;//Enables random walking
	  hSmoothWander = 1;
	  //hReturnToSpawn = 1;//Returns to spawn when too far //Always false
	  //hSpawnDist = 32;//Defines the distance bot can travel away from spawnbrick //Always 10000
	
	//Searching options
	hSearch = 1;//Search for Players
	  hSearchRadius = 256;//in brick units
	  hSight = 1;//Require bot to see player before pursuing
	  hStrafe = 1;//Randomly strafe while following player
	hSearchFOV = 1;//if enabled disables normal hSearch
	  hFOVRadius = 32;//max 10

	  hAlertOtherBots = 1;//Alerts other bots when he sees a player, or gets attacked

	//Attack Options
	hMelee = 1;//Melee
	  hAttackDamage = 20;//15;//Melee Damage
	hShoot = 0;
	  hWep = "gunImage";
	  hShootTimes = 4;//Number of times the bot will shoot between each tick
	  hMaxShootRange = 30;//The range in which the bot will shoot the player
	  hAvoidCloseRange = 1;//
		hTooCloseRange = 7;//in brick units
	//hHerding = 0;
	//hSound = 1;
	//hSpawnDetect = -1;//Will not spawn when user is too close and can see spawn
	

	
	//Misc options
	hAvoidObstacles = 1;
	hSuperStacker = 1;
	hSpazJump = 1;//Makes bot jump when the user their following is higher than them

	hAFKOmeter = 1;//Determines how often the bot will wander or do other idle actions, higher it is the less often he does things
	hIdle = 1;// Enables use of idle actions, actions which are done when the bot is not doing anything else
	  hIdleAnimation = 0;//Plays random animations/emotes, sit, click, love/hate/etc
	  hIdleLookAtOthers = 1;//Randomly looks at other players/bots when not doing anything else
	    hIdleSpam = 0;//Makes them spam click and spam hammer/spraycan
	  hSpasticLook = 1;//Makes them look around their environment a bit more.
	hEmote = 1;
	
	//Item Drops
	materialDrop = "Metal"; //Name of material to drop.
	materialChance = 750; //Chance to get material out of 1000.
	materialAmount = "5 15"; //Amount of materials you will get * 3.
	//First num is min, second is max. Inclusive.
	
	wolframAmount = 240; //Amount of wolfram you will get per kill, max. Use a number divisable by 3.
	
	itemDrop = HealthExpanderItem; //Item to drop. put NONE as a string for nothing.
	itemChance = 75; //Chance to get item out of 1000.
	itemFlavor = "The husk droped a shiny thinket!"; //Message to killer when item is dropped.
	
	dropList = "Bones\t1\t200" NL "Leather\t2 3\t400" NL"Coal\t1\t100" NL "Essence\t1\t400";
	
	blueprintChance = 250;
	
	score = 7;
	
	killAchievement = "Zombie Hunter";
};

datablock PlayerData(SwarmerHoleBot : PlayerStandardArmor) //Swarmer. Tiny little biters can only spawn in swarm encounters.
{
	uiName = "";
	minJetEnergy = 0;
	jetEnergyDrain = 0;
	canJet = 0;
	maxItems   = 0;
	maxWeapons = 0;
	maxTools = 0;
	runforce = 60 * 90;
	maxForwardSpeed = 6;
	maxBackwardSpeed = 3;
	maxSideSpeed = 8;
	attackpower = 10; //What does this do?
	rideable = false;
	canRide = false;
	maxdamage = 50;//Health
	jumpSound = "";
	
	//Hole Attributes
	isHoleBot = 1;

	//Solar Apoc
	immuneToSun = false;
	immuneToFire = false;
	immuneToNight = true;
	tripleFire = false;
	
	//Spawning option
	hSpawnTooClose = 0;//Doesn't spawn when player is too close and can see it
	  hSpawnTCRange = 8;//above range, set in brick units
	hSpawnClose = 0;//Only spawn when close to a player, can be used with above function as long as hSCRange is higher than hSpawnTCRange
	  hSpawnCRange = 64;//above range, set in brick units

	hType = enemy; //Enemy,Friendly, Neutral
	  hNeutralAttackChance = 100;
	//can have unique types, nazis will attack zombies but nazis will not attack other bots labeled nazi
	hName = "Swarmer";//cannot contain spaces
	hTickRate = 3000;
	
	//Wander Options
	hWander = 1;//Enables random walking
	  hSmoothWander = 1;
	  //hReturnToSpawn = 1;//Returns to spawn when too far //Always false
	  //hSpawnDist = 32;//Defines the distance bot can travel away from spawnbrick //Always 10000
	
	//Searching options
	hSearch = 1;//Search for Players
	  hSearchRadius = 256;//in brick units
	  hSight = 1;//Require bot to see player before pursuing
	  hStrafe = 1;//Randomly strafe while following player
	hSearchFOV = 1;//if enabled disables normal hSearch
	  hFOVRadius = 32;//max 10

	  hAlertOtherBots = 1;//Alerts other bots when he sees a player, or gets attacked

	//Attack Options
	hMelee = 1;//Melee
	  hAttackDamage = 10;//15;//Melee Damage
	hShoot = 0;
	  hWep = "gunImage";
	  hShootTimes = 4;//Number of times the bot will shoot between each tick
	  hMaxShootRange = 30;//The range in which the bot will shoot the player
	  hAvoidCloseRange = 1;//
		hTooCloseRange = 7;//in brick units
	hHerding = 1;
	//hSound = 1;
	//hSpawnDetect = -1;//Will not spawn when user is too close and can see spawn
	

	
	//Misc options
	hAvoidObstacles = 1;
	hSuperStacker = 1;
	hSpazJump = 1;//Makes bot jump when the user their following is higher than them

	hAFKOmeter = 1;//Determines how often the bot will wander or do other idle actions, higher it is the less often he does things
	hIdle = 1;// Enables use of idle actions, actions which are done when the bot is not doing anything else
	  hIdleAnimation = 0;//Plays random animations/emotes, sit, click, love/hate/etc
	  hIdleLookAtOthers = 1;//Randomly looks at other players/bots when not doing anything else
	    hIdleSpam = 0;//Makes them spam click and spam hammer/spraycan
	  hSpasticLook = 1;//Makes them look around their environment a bit more.
	hEmote = 1;
	
	//Item Drops
	materialDrop = "Copper"; //Name of material to drop.
	materialChance = 50; //Chance to get material out of 1000.
	materialAmount = "1 2"; //Amount of materials you will get * 3.
	//First num is min, second is max. Inclusive.
	
	wolframAmount = 90; //Amount of wolfram you will get per kill, max. Use a number divisable by 3.
	
	itemDrop = "NONE"; //Item to drop. put NONE as a string for nothing.
	itemChance = -1; //Chance to get item out of 1000.
	itemFlavor = "---"; //Message to killer when item is dropped.
	
	dropList = "Bones\t1\t100" NL "Leather\t1\t100" NL "Ashes\t1\t250";
	
	blueprintChance = 25;
	
	score = 2;
	
	killAchievement = "Zombie Hunter";
};

datablock ShapeBaseImageData(ribcageImage) //Ribcage for the skeleton. Uses Phydeoux's skeleton model :)
{
	shapeFile = "./models/ribcage.dts";
	emap = true;
	mountPoint = 2;
	offset = "0 0 -0.6";
	eyeOffset = "0 0 0.4";
	rotation = eulerToMatrix("0 0 0");
	scale = "1 1 1";
	doColorShift = false;
	colorShiftColor = "0.000 0.500 0.250 1.000";
};

datablock PlayerData(RevenantHoleBot : PlayerStandardArmor) //Revenant. Minecraft skeletons basically.
{
	uiName = "";
	minJetEnergy = 0;
	jetEnergyDrain = 0;
	canJet = 0;
	maxItems   = 0;
	maxWeapons = 0;
	maxTools = 0;
	runforce = 60 * 90;
	maxForwardSpeed = 4;
	maxBackwardSpeed = 2;
	maxSideSpeed = 4;
	attackpower = 10; //What does this do?
	rideable = false;
	canRide = false;
	maxdamage = 20;//Health
	jumpSound = "";
	
	//Hole Attributes
	isHoleBot = 1;
	
	//Spawning option
	hSpawnTooClose = 0;//Doesn't spawn when player is too close and can see it
	  hSpawnTCRange = 8;//above range, set in brick units
	hSpawnClose = 0;//Only spawn when close to a player, can be used with above function as long as hSCRange is higher than hSpawnTCRange
	  hSpawnCRange = 64;//above range, set in brick units

	hType = enemy; //Enemy,Friendly, Neutral
	  hNeutralAttackChance = 100;
	//can have unique types, nazis will attack zombies but nazis will not attack other bots labeled nazi
	hName = "Revenant";//cannot contain spaces
	hTickRate = 7000;
	
	//Wander Options
	hWander = 1;//Enables random walking
	  hSmoothWander = 1;
	  //hReturnToSpawn = 1;//Returns to spawn when too far //Always false
	  //hSpawnDist = 32;//Defines the distance bot can travel away from spawnbrick //Always 10000
	
	//Searching options
	hSearch = 1;//Search for Players
	  hSearchRadius = 256;//in brick units
	  hSight = 1;//Require bot to see player before pursuing
	  hStrafe = 1;//Randomly strafe while following player
	hSearchFOV = 1;//if enabled disables normal hSearch
	  hFOVRadius = 32;//max 10

	  hAlertOtherBots = 1;//Alerts other bots when he sees a player, or gets attacked

	//Attack Options
	hMelee = 0;//Melee
	  hAttackDamage = 20;//15;//Melee Damage
	hShoot = 1;
	  hWep = bowImage;
	  hShootTimes = 3;//Number of times the bot will shoot between each tick
	  hMaxShootRange = 64;//The range in which the bot will shoot the player
	  hAvoidCloseRange = 1;//
		hTooCloseRange = 7;//in brick units
	//hHerding = 0;
	//hSound = 1;
	//hSpawnDetect = -1;//Will not spawn when user is too close and can see spawn
	

	
	//Misc options
	hAvoidObstacles = 1;
	hSuperStacker = 1;
	hSpazJump = 0;//Makes bot jump when the user their following is higher than them

	hAFKOmeter = 1;//Determines how often the bot will wander or do other idle actions, higher it is the less often he does things
	hIdle = 1;// Enables use of idle actions, actions which are done when the bot is not doing anything else
	  hIdleAnimation = 0;//Plays random animations/emotes, sit, click, love/hate/etc
	  hIdleLookAtOthers = 1;//Randomly looks at other players/bots when not doing anything else
	    hIdleSpam = 0;//Makes them spam click and spam hammer/spraycan
	  hSpasticLook = 1;//Makes them look around their environment a bit more.
	hEmote = 1;
	
	//Item Drops
	materialDrop = "Glass"; //Name of material to drop.
	materialChance = 400; //Chance to get material out of 1000.
	materialAmount = "3 9"; //Amount of materials you will get * 3.
	//First num is min, second is max. Inclusive.
	
	wolframAmount = 60; //Amount of wolfram you will get per kill, max. Use a number divisable by 3.
	
	itemDrop = bowItem; //Item to drop. put NONE as a string for nothing.
	itemChance = 100; //Chance to get item out of 1000.
	itemFlavor = "Hey, looks like I could use this bow!"; //Message to killer when item is dropped.
	
	dropList = "Bones\t1 2\t1000" NL "Leather\t1\t150";
	
	blueprintChance = 150;
	
	score = 6;
	
	killAchievement = "Bone Picker";
};

datablock PlayerData(ChaosElementalHoleBot : PlayerStandardArmor)
{
	uiName = "";
	minJetEnergy = 0;
	jetEnergyDrain = 0;
	canJet = 0;
	maxItems   = 0;
	maxWeapons = 0;
	maxTools = 0;
	runforce = 60 * 90;
	maxForwardSpeed = 7;
	maxBackwardSpeed = 4;
	maxSideSpeed = 7;
	attackpower = 10; //What does this do?
	rideable = false;
	canRide = false;
	maxdamage = 125;//Health
	jumpSound = "";
	
	//Hole Attributes
	isHoleBot = 1;
	
	//Spawning option
	hSpawnTooClose = 0;//Doesn't spawn when player is too close and can see it
	  hSpawnTCRange = 8;//above range, set in brick units
	hSpawnClose = 0;//Only spawn when close to a player, can be used with above function as long as hSCRange is higher than hSpawnTCRange
	  hSpawnCRange = 64;//above range, set in brick units

	hType = enemy; //Enemy,Friendly, Neutral
	  hNeutralAttackChance = 100;
	//can have unique types, nazis will attack zombies but nazis will not attack other bots labeled nazi
	hName = "ChaosElemental";//cannot contain spaces
	hTickRate = 2000;
	
	//Wander Options
	hWander = 1;//Enables random walking
	  hSmoothWander = 1;
	  //hReturnToSpawn = 1;//Returns to spawn when too far //Always false
	  //hSpawnDist = 32;//Defines the distance bot can travel away from spawnbrick //Always 10000
	
	//Searching options
	hSearch = 1;//Search for Players
	  hSearchRadius = 256;//in brick units
	  hSight = 1;//Require bot to see player before pursuing
	  hStrafe = 1;//Randomly strafe while following player
	hSearchFOV = 1;//if enabled disables normal hSearch
	  hFOVRadius = 32;//max 10

	  hAlertOtherBots = 1;//Alerts other bots when he sees a player, or gets attacked

	//Attack Options
	hMelee = 0;//Melee
	  hAttackDamage = 20;//15;//Melee Damage
	hShoot = 1;
	  hWep = ShockWaveImage; //LdragonMeleeImage;
	  hShootTimes = 4;//Number of times the bot will shoot between each tick
	  hMaxShootRange = 64;//The range in which the bot will shoot the player
	  hAvoidCloseRange = 0;//
		hTooCloseRange = 7;//in brick units
	//hHerding = 0;
	//hSound = 1;
	//hSpawnDetect = -1;//Will not spawn when user is too close and can see spawn
	

	
	//Misc options
	hAvoidObstacles = 1;
	hSuperStacker = 1;
	hSpazJump = 1;//Makes bot jump when the user their following is higher than them

	hAFKOmeter = 1;//Determines how often the bot will wander or do other idle actions, higher it is the less often he does things
	hIdle = 1;// Enables use of idle actions, actions which are done when the bot is not doing anything else
	  hIdleAnimation = 0;//Plays random animations/emotes, sit, click, love/hate/etc
	  hIdleLookAtOthers = 1;//Randomly looks at other players/bots when not doing anything else
	    hIdleSpam = 0;//Makes them spam click and spam hammer/spraycan
	  hSpasticLook = 1;//Makes them look around their environment a bit more.
	hEmote = 1;
	
	//Item Drops
	materialDrop = "Sturdium"; //Name of material to drop.
	materialChance = 500; //Chance to get material out of 1000.
	materialAmount = "9 14"; //Amount of materials you will get * 3.
	//First num is min, second is max. Inclusive.
	
	wolframAmount = 150; //Amount of wolfram you will get per kill, max. Use a number divisable by 3.
	
	itemDrop = ShockWaveItem; //Item to drop. put NONE as a string for nothing.
	itemChance = 100; //Chance to get item out of 1000.
	itemFlavor = "That tool it dropped looks useful."; //Message to killer when item is dropped.
	
	dropList = "Leather\t2 3\t500" NL "Essence\t2 3\t1000" NL "Demon Soul\t1\t500";
	
	blueprintChance = 450;
	
	score = 15;
	
	killAchievement = "Janitor";
};

datablock PlayerData(ElementalHoleBot : PlayerStandardArmor)
{
	uiName = "";
	minJetEnergy = 0;
	jetEnergyDrain = 0;
	canJet = 0;
	maxItems   = 0;
	maxWeapons = 0;
	maxTools = 0;
	runforce = 60 * 90;
	maxForwardSpeed = 7;
	maxBackwardSpeed = 4;
	maxSideSpeed = 5;
	attackpower = 10; //What does this do?
	rideable = false;
	canRide = false;
	maxdamage = 100;//Health
	jumpSound = "";
	
	//Hole Attributes
	isHoleBot = 1;
	
	//Spawning option
	hSpawnTooClose = 0;//Doesn't spawn when player is too close and can see it
	  hSpawnTCRange = 8;//above range, set in brick units
	hSpawnClose = 0;//Only spawn when close to a player, can be used with above function as long as hSCRange is higher than hSpawnTCRange
	  hSpawnCRange = 64;//above range, set in brick units

	hType = enemy; //Enemy,Friendly, Neutral
	  hNeutralAttackChance = 100;
	//can have unique types, nazis will attack zombies but nazis will not attack other bots labeled nazi
	hName = "FireElemental";//cannot contain spaces
	hTickRate = 5000;
	
	//Wander Options
	hWander = 1;//Enables random walking
	  hSmoothWander = 1;
	  //hReturnToSpawn = 1;//Returns to spawn when too far //Always false
	  //hSpawnDist = 32;//Defines the distance bot can travel away from spawnbrick //Always 10000
	
	//Searching options
	hSearch = 1;//Search for Players
	  hSearchRadius = 256;//in brick units
	  hSight = 1;//Require bot to see player before pursuing
	  hStrafe = 1;//Randomly strafe while following player
	hSearchFOV = 1;//if enabled disables normal hSearch
	  hFOVRadius = 32;//max 10

	  hAlertOtherBots = 1;//Alerts other bots when he sees a player, or gets attacked

	//Attack Options
	hMelee = 0;//Melee
	  hAttackDamage = 20;//15;//Melee Damage
	hShoot = 1;
	  hWep = DragonFireBallImage;
	  hShootTimes = 1;//Number of times the bot will shoot between each tick
	  hMaxShootRange = 64;//The range in which the bot will shoot the player
	  hAvoidCloseRange = 1;//
		hTooCloseRange = 7;//in brick units
	//hHerding = 0;
	//hSound = 1;
	//hSpawnDetect = -1;//Will not spawn when user is too close and can see spawn
	

	
	//Misc options
	hAvoidObstacles = 1;
	hSuperStacker = 1;
	hSpazJump = 0;//Makes bot jump when the user their following is higher than them

	hAFKOmeter = 1;//Determines how often the bot will wander or do other idle actions, higher it is the less often he does things
	hIdle = 1;// Enables use of idle actions, actions which are done when the bot is not doing anything else
	  hIdleAnimation = 0;//Plays random animations/emotes, sit, click, love/hate/etc
	  hIdleLookAtOthers = 1;//Randomly looks at other players/bots when not doing anything else
	    hIdleSpam = 0;//Makes them spam click and spam hammer/spraycan
	  hSpasticLook = 1;//Makes them look around their environment a bit more.
	hEmote = 1;
	
	//Item Drops
	materialDrop = "Lead"; //Name of material to drop.
	materialChance = 250; //Chance to get material out of 1000.
	materialAmount = "5 10"; //Amount of materials you will get * 3.
	//First num is min, second is max. Inclusive.
	
	wolframAmount = 150; //Amount of wolfram you will get per kill, max. Use a number divisable by 3.
	
	itemDrop = DragonFireBallItem; //Item to drop. put NONE as a string for nothing.
	itemChance = 100; //Chance to get item out of 1000.
	itemFlavor = "Hey, it dropped a magic item!"; //Message to killer when item is dropped.
	
	dropList = "Bones\t1 2\t1000" NL "Leather\t2 3\t500" NL "Essence\t1 2\t1000" NL "Demon Soul\t1\t100";
	
	blueprintChance = 350;
	
	score = 10;
	
	killAchievement = "Janitor";
};

datablock PlayerData(BanditHoleBot : PlayerStandardArmor) //Bandits. Really annoying.
{
	uiName = "";
	minJetEnergy = 0;
	jetEnergyDrain = 0;
	canJet = 0;
	maxItems   = 0;
	maxWeapons = 0;
	maxTools = 0;
	runforce = 60 * 90;
	maxForwardSpeed = 4;
	maxBackwardSpeed = 2;
	maxSideSpeed = 4;
	attackpower = 10; //What does this do?
	rideable = false;
	canRide = false;
	maxdamage = 100;//Health
	jumpSound = "";
	
	//Hole Attributes
	isHoleBot = 1;
	
	//Spawning option
	hSpawnTooClose = 0;//Doesn't spawn when player is too close and can see it
	  hSpawnTCRange = 8;//above range, set in brick units
	hSpawnClose = 0;//Only spawn when close to a player, can be used with above function as long as hSCRange is higher than hSpawnTCRange
	  hSpawnCRange = 64;//above range, set in brick units

	hType = enemy; //Enemy,Friendly, Neutral
	  hNeutralAttackChance = 100;
	//can have unique types, nazis will attack zombies but nazis will not attack other bots labeled nazi
	hName = "Bandit";//cannot contain spaces
	hTickRate = 7000;
	
	//Wander Options
	hWander = 1;//Enables random walking
	  hSmoothWander = 1;
	  //hReturnToSpawn = 1;//Returns to spawn when too far //Always false
	  //hSpawnDist = 32;//Defines the distance bot can travel away from spawnbrick //Always 10000
	
	//Searching options
	hSearch = 1;//Search for Players
	  hSearchRadius = 256;//in brick units
	  hSight = 1;//Require bot to see player before pursuing
	  hStrafe = 1;//Randomly strafe while following player
	hSearchFOV = 1;//if enabled disables normal hSearch
	  hFOVRadius = 32;//max 10

	  hAlertOtherBots = 1;//Alerts other bots when he sees a player, or gets attacked

	//Attack Options
	hMelee = 0;//Melee
	  hAttackDamage = 20;//15;//Melee Damage
	hShoot = 1;
	  hWep = "";
	  hShootTimes = 3;//Number of times the bot will shoot between each tick
	  hMaxShootRange = 64;//The range in which the bot will shoot the player
	  hAvoidCloseRange = 1;//
		hTooCloseRange = 7;//in brick units
	//hHerding = 0;
	//hSound = 1;
	//hSpawnDetect = -1;//Will not spawn when user is too close and can see spawn
	

	
	//Misc options
	hAvoidObstacles = 1;
	hSuperStacker = 1;
	hSpazJump = 0;//Makes bot jump when the user their following is higher than them

	hAFKOmeter = 1;//Determines how often the bot will wander or do other idle actions, higher it is the less often he does things
	hIdle = 1;// Enables use of idle actions, actions which are done when the bot is not doing anything else
	  hIdleAnimation = 0;//Plays random animations/emotes, sit, click, love/hate/etc
	  hIdleLookAtOthers = 1;//Randomly looks at other players/bots when not doing anything else
	    hIdleSpam = 0;//Makes them spam click and spam hammer/spraycan
	  hSpasticLook = 1;//Makes them look around their environment a bit more.
	hEmote = 1;
	
	//Item Drops
	materialDrop = "Glass"; //Name of material to drop.
	materialChance = 400; //Chance to get material out of 1000.
	materialAmount = "3 9"; //Amount of materials you will get * 3.
	//First num is min, second is max. Inclusive.
	
	wolframAmount = 60; //Amount of wolfram you will get per kill, max. Use a number divisable by 3.
	
	itemDrop = "HELD_ITEM"; //Item to drop. put NONE as a string for nothing.
	itemChance = 100; //Chance to get item out of 1000.
	itemFlavor = "Hey, looks like this gun is still usable!"; //Message to killer when item is dropped.
	
	dropList = "Bones\t1\t750" NL "Metal Ingot\t1\t150";
	
	blueprintChance = 400;
	
	score = 12;
	
	killAchievement = "Rais Begone";
};

//Dragon, a Miniboss

datablock PlayerData(DragonHoleBot : AirDragonArmor)
{
	uiName = "";
	maxItems   = 0;
	maxWeapons = 0;
	maxTools = 0;
	attackpower = 10; //What does this do?
	rideable = false;
	canRide = false;
	maxdamage = 500;//Health
	canJet = 1;
	//mass = 0.1;
	
	//Hole Attributes
	isHoleBot = 1;
	
	//Spawning option
	hSpawnTooClose = 0;//Doesn't spawn when player is too close and can see it
	  hSpawnTCRange = 8;//above range, set in brick units
	hSpawnClose = 0;//Only spawn when close to a player, can be used with above function as long as hSCRange is higher than hSpawnTCRange
	  hSpawnCRange = 64;//above range, set in brick units

	hType = mercenary; //Enemy,Friendly, Neutral
	  hNeutralAttackChance = 100;
	//can have unique types, nazis will attack zombies but nazis will not attack other bots labeled nazi
	hName = "Dragon";//cannot contain spaces
	hTickRate = 1000;
	
	//Wander Options
	hWander = 1;//Enables random walking
	  hSmoothWander = 1;
	  //hReturnToSpawn = 1;//Returns to spawn when too far //Always false
	  //hSpawnDist = 32;//Defines the distance bot can travel away from spawnbrick //Always 10000
	
	//Searching options
	hSearch = 1;//Search for Players
	  hSearchRadius = 256;//in brick units
	  hSight = 1;//Require bot to see player before pursuing
	  hStrafe = 1;//Randomly strafe while following player
	hSearchFOV = 1;//if enabled disables normal hSearch
	  hFOVRadius = 32;//max 10

	  hAlertOtherBots = 1;//Alerts other bots when he sees a player, or gets attacked

	//Attack Options
	hMelee = 0;//Melee
	  hAttackDamage = 20;//15;//Melee Damage
	hShoot = 1;
	  hWep = DragonFireBarrageImage;
	  hShootTimes = 3;//Number of times the bot will shoot between each tick
	  hMaxShootRange = 64;//The range in which the bot will shoot the player
	  hAvoidCloseRange = 1;//
		hTooCloseRange = 7;//in brick units
	//hHerding = 0;
	//hSound = 1;
	//hSpawnDetect = -1;//Will not spawn when user is too close and can see spawn
	

	
	//Misc options
	hAvoidObstacles = 1;
	hSuperStacker = 0;
	hSpazJump = 0;//Makes bot jump when the user their following is higher than them

	hAFKOmeter = 1;//Determines how often the bot will wander or do other idle actions, higher it is the less often he does things
	hIdle = 1;// Enables use of idle actions, actions which are done when the bot is not doing anything else
	  hIdleAnimation = 1;//Plays random animations/emotes, sit, click, love/hate/etc
	  hIdleLookAtOthers = 1;//Randomly looks at other players/bots when not doing anything else
	    hIdleSpam = 0;//Makes them spam click and spam hammer/spraycan
	  hSpasticLook = 1;//Makes them look around their environment a bit more.
	hEmote = 1;
	
	//Item Drops
	materialDrop = "Copper"; //Name of material to drop.
	materialChance = 800; //Chance to get material out of 1000.
	materialAmount = "5 15"; //Amount of materials you will get * 3.
	//First num is min, second is max. Inclusive.
	
	wolframAmount = 300; //Amount of wolfram you will get per kill, max. Use a number divisable by 3.
	
	itemDrop = DragonFireBarrageItem; //Item to drop. put NONE as a string for nothing.
	itemChance = 500; //Chance to get item out of 1000.
	itemFlavor = "Hey, it dropped a magic item!"; //Message to killer when item is dropped.
	
	dropList = "Bones\t3 5\t1000" NL "Leather\t3 5\t1000" NL "Essence\t10\t1000" NL "Dragon Scale\t1\t1000";
	
	blueprintChance = 1000;
	
	score = 15;
	
	killAchievement = "Dragon Slayer";
};

//Sturdium Beast, a Miniboss

datablock PlayerData(SturdiumBeastHoleBot : PlayerStandardArmor)
{
	shapeFile = "Add-Ons/Bot_Zombie_Tank/zTank.dts";
	uiName = "";
	minJetEnergy = 0;
	jetEnergyDrain = 0;
	canJet = 0;
	maxItems   = 0;
	maxWeapons = 0;
	maxTools = 0;
	runforce = 40 * 90;
	maxForwardSpeed = 9;
	maxBackwardSpeed = 9;
	maxSideSpeed = 12;
	attackpower = 10; //What does this do?
	rideable = false;
	canRide = false;
	maxdamage = 4000;//Health
	jumpSound = "";
	
	//Hole Attributes
	isHoleBot = 1;

	//Solar Apoc
	immuneToSun = true;
	immuneToFire = true;
	immuneToNight = true;
	tripleFire = true;
	
	//Spawning option
	hSpawnTooClose = 0;//Doesn't spawn when player is too close and can see it
	  hSpawnTCRange = 8;//above range, set in brick units
	hSpawnClose = 0;//Only spawn when close to a player, can be used with above function as long as hSCRange is higher than hSpawnTCRange
	  hSpawnCRange = 64;//above range, set in brick units

	hType = enemy; //Enemy,Friendly, Neutral
	  hNeutralAttackChance = 100;
	//can have unique types, nazis will attack zombies but nazis will not attack other bots labeled nazi
	hName = "Sturdium Beast";//cannot contain spaces
	hTickRate = 3000;
	
	//Wander Options
	hWander = 1;//Enables random walking
	  hSmoothWander = 1;
	  //hReturnToSpawn = 1;//Returns to spawn when too far //Always false
	  //hSpawnDist = 32;//Defines the distance bot can travel away from spawnbrick //Always 10000
	
	//Searching options
	hSearch = 1;//Search for Players
	  hSearchRadius = 256;//in brick units
	  hSight = 1;//Require bot to see player before pursuing
	  hStrafe = 1;//Randomly strafe while following player
	hSearchFOV = 1;//if enabled disables normal hSearch
	  hFOVRadius = 32;//max 10
	   hHearing = 1;//If it hears a player it'll look in the direction of the sound

	  hAlertOtherBots = 1;//Alerts other bots when he sees a player, or gets attacked

	//Attack Options
	hMelee = 0;//Melee
	  hAttackDamage = 15;//15;//Melee Damage
	hShoot = 1;
	  hWep = SturdiumBeastWepImage;
	  hShootTimes = 4;//Number of times the bot will shoot between each tick
	  hMaxShootRange = 30;//The range in which the bot will shoot the player
	  hAvoidCloseRange = 1;//
		hTooCloseRange = 7;//in brick units
	//hHerding = 0;
	//hSound = 1;
	//hSpawnDetect = -1;//Will not spawn when user is too close and can see spawn
	

	
	//Misc options
	hAvoidObstacles = 1;
	hSuperStacker = 0;
	hSpazJump = 0;//Makes bot jump when the user their following is higher than them

	hAFKOmeter = 1;//Determines how often the bot will wander or do other idle actions, higher it is the less often he does things
	hIdle = 1;// Enables use of idle actions, actions which are done when the bot is not doing anything else
	  hIdleAnimation = 0;//Plays random animations/emotes, sit, click, love/hate/etc
	  hIdleLookAtOthers = 1;//Randomly looks at other players/bots when not doing anything else
	    hIdleSpam = 0;//Makes them spam click and spam hammer/spraycan
	  hSpasticLook = 1;//Makes them look around their environment a bit more.
	hEmote = 1;
	
	//Item Drops
	materialDrop = "Sturdiun"; //Name of material to drop.
	materialChance = 1000; //Chance to get material out of 1000.
	materialAmount = "10 30"; //Amount of materials you will get * 3.
	//First num is min, second is max. Inclusive.
	
	wolframAmount = 90; //Amount of wolfram you will get per kill, max. Use a number divisable by 3.
	
	itemDrop = "NONE"; //Item to drop. put NONE as a string for nothing.
	itemChance = -1; //Chance to get item out of 1000.
	itemFlavor = "Hey, he dropped something!"; //Message to killer when item is dropped.
	
	score = 50;
	
	blueprintChance = 1000;
};

//Behemoth, the Final Boss

datablock PlayerData(BehemothHoleBot : PlayerStandardArmor)
{
	uiName = "";
	minJetEnergy = 0;
	jetEnergyDrain = 0;
	canJet = 1;
	maxItems   = 0;
	maxWeapons = 0;
	maxTools = 0;
	runforce = 180 * 90;
	maxForwardSpeed = 9;
	maxBackwardSpeed = 5;
	maxSideSpeed = 16;
	attackpower = 10; //What does this do?
	rideable = false;
	canRide = false;
	maxdamage = 2500;//Health
	jumpSound = "";
	
	//Hole Attributes
	isHoleBot = 1;
	
	//Spawning option
	hSpawnTooClose = 0;//Doesn't spawn when player is too close and can see it
	  hSpawnTCRange = 8;//above range, set in brick units
	hSpawnClose = 0;//Only spawn when close to a player, can be used with above function as long as hSCRange is higher than hSpawnTCRange
	  hSpawnCRange = 64;//above range, set in brick units

	hType = enemy; //Enemy,Friendly, Neutral
	  hNeutralAttackChance = 100;
	//can have unique types, nazis will attack zombies but nazis will not attack other bots labeled nazi
	hName = "Behemoth";//cannot contain spaces
	hTickRate = 3000;
	
	//Wander Options
	hWander = 1;//Enables random walking
	  hSmoothWander = 1;
	  //hReturnToSpawn = 1;//Returns to spawn when too far //Always false
	  //hSpawnDist = 32;//Defines the distance bot can travel away from spawnbrick //Always 10000
	
	//Searching options
	hSearch = 1;//Search for Players
	  hSearchRadius = 1028;//in brick units
	  hSight = 0;//Require bot to see player before pursuing
	  hStrafe = 1;//Randomly strafe while following player
	hSearchFOV = 0;//if enabled disables normal hSearch
	  hFOVRadius = 64;//max 10

	  hAlertOtherBots = 1;//Alerts other bots when he sees a player, or gets attacked

	//Attack Options
	hMelee = 0;//Melee
	  hAttackDamage = 20;//15;//Melee Damage
	hShoot = 1;
	  hWep = DragonFireBarrageImage;
	  hShootTimes = 2;//Number of times the bot will shoot between each tick
	  hMaxShootRange = 64;//The range in which the bot will shoot the player
	  hAvoidCloseRange = 1;//
		hTooCloseRange = 7;//in brick units
	//hHerding = 0;
	//hSound = 1;
	//hSpawnDetect = -1;//Will not spawn when user is too close and can see spawn
	

	
	//Misc options
	hAvoidObstacles = 1;
	hSuperStacker = 1;
	hSpazJump = 1;//Makes bot jump when the user their following is higher than them

	hAFKOmeter = 1;//Determines how often the bot will wander or do other idle actions, higher it is the less often he does things
	hIdle = 1;// Enables use of idle actions, actions which are done when the bot is not doing anything else
	  hIdleAnimation = 0;//Plays random animations/emotes, sit, click, love/hate/etc
	  hIdleLookAtOthers = 1;//Randomly looks at other players/bots when not doing anything else
	    hIdleSpam = 0;//Makes them spam click and spam hammer/spraycan
	  hSpasticLook = 1;//Makes them look around their environment a bit more.
	hEmote = 1;
	
	//Item Drops
	materialDrop = "Lead"; //Name of material to drop.
	materialChance = -1; //Chance to get material out of 1000.
	materialAmount = "5 10"; //Amount of materials you will get * 3.
	//First num is min, second is max. Inclusive.
	
	wolframAmount = 150; //Amount of wolfram you will get per kill, max. Use a number divisable by 3.
	
	itemDrop = DragonFireBallItem; //Item to drop. put NONE as a string for nothing.
	itemChance = -1; //Chance to get item out of 1000.
	itemFlavor = "Hey, it dropped a magic spell!"; //Message to killer when item is dropped.
};

function spawnNewZombie(%trans,%hBotType) //spawnNewZombie("64 64 128 0 0 0 0",UnfleshedHoleBot);
{
	if(!isObject(FakeBotSpawnBrick))
	{
		new FxDtsBrick(FakeBotSpawnBrick)
		{
			datablock = brick1x1Data;
			isPlanted = false;
			itemPosition = 1;
			position = "0 0 -2000";
		};
	}
	%spawnBrick = FakeBotSpawnBrick;
	
	//echo("hBotType: " @ %hBotType); //Debug
	
	if(%hBotType $= "")
		%hBotType = ZombieHoleBot;
		
	//echo("hBotType Two: " @ %hBotType); //Debug
	
	%player = new AIPlayer()
	{
		dataBlock = %hBotType;
		path = "";
		spawnBrick = %spawnBrick;
		mini = $defaultMinigame;
		
		position = getWords(%trans, 0, 2);
		hGridPosition = getWords(%trans, 0, 2);
		rotation = getWords(%trans, 3, 6);
		
		//Apply attributes to Bot
		client = 0;//findClientByName("Fastmapler");
		isHoleBot = 1;
			
		//Apply attributes to Bot
		Name = %hBotType.hName;
		hType = "enemy"; //%hBotType.hType;
		hDamageType = (strLen(%damageType) > 0 ? $DamageType["::" @ %damageType] : $DamageType::HoleMelee);
		hSearchRadius = %hBotType.hSearchRadius;
		hSearch = %hBotType.hSearch;
		hSight = %hBotType.hSight;
		hWander = %hBotType.hWander;
		hGridWander = 256;
		hReturnToSpawn = false; //%hBotType.hReturnToSpawn;
		hSpawnDist = 10000;
		hHerding = %hBotType.hHerding;
		hMelee = %hBotType.hMelee;
		hAttackDamage = %hBotType.hAttackDamage;
		hSpazJump = %hBotType.hSpazJump;
		hSearchFOV = %hBotType.hSearchFOV;
		hFOVRadius = %hBotType.hFOVRadius;
		hTooCloseRange = %hBotType.hTooCloseRange;
		hAvoidCloseRange = %hBotType.hAvoidCloseRange;
		hShoot = %hBotType.hShoot;
		hMaxShootRange = %hBotType.hMaxShootRange;
		hStrafe = %hBotType.hStrafe;
		hAlertOtherBots = %hBotType.hAlertOtherBots;
		hIdleAnimation = %hBotType.hIdleAnimation;
		hSpasticLook = %hBotType.hSpasticLook;
		hAvoidObstacles = %hBotType.hAvoidObstacles;
		hIdleLookAtOthers = %hBotType.hIdleLookAtOthers;
		hIdleSpam = false; //%hBotType.hIdleSpam;
		hAFKOmeter = %hBotType.hAFKOmeter + getRandom( 0, 2 );
		hHearing = true;//%hBotType.hHearing;
		hIdle = %hBotType.hIdle;
		hSmoothWander = %hBotType.hSmoothWander;
		hEmote = %hBotType.hEmote;
		hSuperStacker = %hBotType.hSuperStacker;
		hNeutralAttackChance = %hBotType.hNeutralAttackChance;
		hFOVRange = %hBotType.hFOVRange;
		hMoveSlowdown = %hBotType.hMoveSlowdown;
		hMaxMoveSpeed = 1.0;
		hActivateDirection = %hBotType.hActivateDirection;
	};
		
	missionCleanup.add(%player);
		
	EOTWEnemies.add(%player);
	
	applyBotSkin(%player);
	//echo(%hBotType.hWep);
	if (%hBotType.hShoot)
		%player.mountImage(%hBotType.hWep,0);
	
	%player.hGridPosition = getWords(%trans, 0, 2);
	%player.scheduleNoQuota(10,spawnProjectile,"audio2d","spawnProjectile","0 0 0", 1);
	
	if (%hBotType.getName() $= "BehemothHoleBot")
		$EOTW::BossText = "<br>BEHEMOTH: LURKING - 100% HP";	
	if (%hBotType.getName() $= "SturdiumBeastHoleBot")
		$EOTW::BossText = "<br>THE BEAST: LURKING - 100% HP";
	
	return %player;
}

function applyBotSkin(%obj)
{
	//echo(%obj.getDataBlock().getName() SPC %obj.dataBlock SPC "UnfleshedHoleBot");
	if (%obj.getDataBlock().getName() $= "UnfleshedHoleBot")
	{
		%obj.playthread(1,"ArmReadyBoth");
		%obj.hDefaultThread = "ArmReadyBoth";

		%llegColor =  "0 0.141 0.333 1";
		%secondPackColor =  "0 0.435 0.831 1";
		%lhand =  "0";
		%hip =  "0";
		%faceName =  "asciiTerror";
		%rarmColor =  "0.626 0.71 0.453 1";
		%hatColor =  "1 1 1 1";
		%hipColor =  "0 0.141 0.333 1";
		%chest =  "0";
		%rarm =  "0";
		%packColor =  "0.2 0 0.8 1";
		%pack =  "0";
		%decalName =  "HCZombie";
		%larmColor =  "0.593 0 0 1";
		%secondPack =  "0";
		%larm =  "1";
		%chestColor =  "0.626 0.71 0.453 1";
		%accentColor =  "0.990 0.960 0 0.700";
		%rhandColor =  "0.626 0.71 0.453 1";
		%rleg =  "0";
		%rlegColor =  "0 0.141 0.333 1";
		%accent =  "1";
		%headColor =  "0.626 0.71 0.453 1";
		%rhand =  "0";
		%lleg =  "0";
		%lhandColor =  "0.626 0.71 0.453 1";
		%hat =  "0";
		
		//Solar Apoc
		%obj.immuneToSun = false;
		%obj.immuneToFire = false;
		%obj.immuneToNight = true;
		%obj.tripleFire = false;
	}
	else if (%obj.getDataBlock().getName() $= "SwarmerHoleBot")
	{
		%obj.playthread(1,"ArmReadyBoth");
		%obj.hDefaultThread = "ArmReadyBoth";

		%llegColor =  "0.626 1.000 0.453 1";
		%secondPackColor =  "0 0.435 0.831 1";
		%lhand =  "0";
		%hip =  "0";
		//%faceName =  "asciiTerror";
		%rarmColor =  "0.626 1.000 0.453 1";
		%hatColor =  "1 1 1 1";
		%hipColor =  "0 0.141 0.333 1";
		%chest =  "0";
		%rarm =  "0";
		%packColor =  "0.2 0 0.8 1";
		%pack =  "0";
		//%decalName =  "HCZombie";
		%larmColor =  "0.626 1.000 0.453 1";
		%secondPack =  "0";
		%larm =  "1";
		%chestColor =  "0.626 1.000 0.453 1";
		%accentColor =  "0.990 0.960 0 0.700";
		%rhandColor =  "0.626 1.000 0.453 1";
		%rleg =  "1";
		%rlegColor =  "0.626 1.000 0.453 1";
		%accent =  "1";
		%headColor =  "0.626 1.000 0.453 1";
		%rhand =  "0";
		%lleg =  "1";
		%lhandColor =  "0.626 1.000 0.453 1";
		%hat =  "0";
		
		//Solar Apoc
		%obj.immuneToSun = false;
		%obj.immuneToFire = false;
		%obj.immuneToNight = true;
		%obj.tripleFire = false;
		
		%obj.setPlayerScale("0.8 0.8 0.8");
	}
	else if (%obj.getDataBlock().getName() $= "HuskHoleBot") //0.914 0.898 0.629 1
	{
		%obj.playthread(1,"ArmReadyBoth");
		%obj.hDefaultThread = "ArmReadyBoth";
		
		%llegColor =  "0.914 0.898 0.629 1";
		%secondPackColor =  "0 0.435 0.831 1";
		%lhand =  "0";
		%hip =  "0";
		%faceName =  "Orc";
		%rarmColor =  "0.914 0.898 0.629 1";
		%hatColor =  "1 1 1 1";
		%hipColor =  "0 0 0 1";
		%chest =  "0";
		%rarm =  "0";
		%packColor =  "0.2 0 0.8 1";
		%pack =  "0";
		%decalName =  "HCZombie";
		%larmColor =  "0.914 0.898 0.629 1";
		%secondPack =  "0";
		%larm =  "1";
		%chestColor =  "0.914 0.898 0.629 1";
		%accentColor =  "0.990 0.960 0 0.700";
		%rhandColor =  "1 0 0 1";
		%rleg =  "0";
		%rlegColor =  "0.914 0.898 0.629 1 1";
		%accent =  "1";
		%headColor =  "0.914 0.898 0.629 1 1";
		%rhand =  "0";
		%lleg =  "0";
		%lhandColor =  "1 0 0 1";
		%hat =  "0";
		
		//Solar Apoc
		%obj.immuneToSun = true;
		%obj.immuneToFire = true;
		%obj.immuneToNight = true;
		%obj.tripleFire = true;
		
		%obj.setPlayerScale("1.1 1.1 1.1");
	}
	else if (%obj.getDatablock().getName() $= "RevenantHoleBot")
	{
		%obj.playthread(1,"ArmReadyBoth");
		%obj.hDefaultThread = "ArmReadyBoth";
		
		%lhand =  "0";
		%hip =  "0";
		%faceName =  "SkeletonEvil";
		%rarmColor =  "1 1 1 1";
		%hipColor =  "1 1 1 1";
		%chest =  "0";
		%rarm =  "1";
		%decalName =  "AAA-None";
		%larmColor =  "1 1 1 1";
		%larm =  "1";
		%chestColor =  "1 1 1 1";
		%accentColor =  "1 1 1 1";
		%rhandColor =  "1 1 1 1";
		%rleg =  "0";
		%accent =  "1";
		%headColor =  "1 1 1 1";
		%rhand =  "0";
		%lleg =  "0";
		%lhandColor =  "1 1 1 1";
	
		%hat =  "0";
		%hatColor =  "1 1 1 1";
		%pack =  "0";
		%packColor =  "1 1 1 1";
		%secondPack =  "0";
		%secondPackColor =  "1 1 1 1";
		%llegColor =  "1 1 1 1";
		%rlegColor =  "1 1 1 1";
		
		//Solar Apoc
		%obj.immuneToSun = false;
		%obj.immuneToFire = false;
		%obj.immuneToNight = true;
		%obj.tripleFire = false;
		
		%obj.schedule(1000,"hideNode","chest");
		%obj.mountimage(ribcageImage,2); //why is this broken????
		
		%obj.setPlayerScale("0.8 0.8 0.8");
	}
	else if (%obj.getDatablock().getName() $= "ElementalHoleBot")
	{
		%obj.playthread(1,"ArmReadyBoth");
		%obj.hDefaultThread = "ArmReadyBoth";
		
		%lhand =  "0";
		%hip =  "0";
		%faceName =  "blank";
		%rarmColor =  "0.5 0 0 0.75";
		%hipColor =  "0.5 0 0 0.75";
		%chest =  "0";
		%rarm =  "1";
		%decalName =  "AAA-None";
		%larmColor =  "0.5 0 0 0.75";
		%larm =  "1";
		%chestColor =  "0.5 0 0 0.75";
		%accentColor =  "0.5 0 0 0.75";
		%rhandColor =  "0.5 0 0 0.75";
		%rleg =  "0";
		%accent =  "1";
		%headColor =  "0.5 0 0 0.75";
		%rhand =  "0";
		%lleg =  "0";
		%lhandColor =  "0.5 0 0 0.75";
	
		%hat =  "0";
		%hatColor =  "0.5 0 0 0.75";
		%pack =  "0";
		%packColor =  "0.5 0 0 0.75";
		%secondPack =  "0";
		%secondPackColor =  "0.5 0 0 0.75";
		%llegColor =  "0.5 0 0 0.75";
		%rlegColor =  "0.5 0 0 0.75";
		
		//Solar Apoc
		%obj.immuneToSun = true;
		%obj.immuneToFire = true;
		%obj.immuneToNight = true;
		%obj.tripleFire = true;
		
		%obj.setPlayerScale("2.0 2.0 2.0");
		%obj.setNodeColor("ALL","0 0 0 0.5");
		%obj.hideNode("lshoe");
		%obj.hideNode("Rshoe");
	}
	else if (%obj.getDatablock().getName() $= "ChaosElementalHoleBot")
	{
		%obj.playthread(1,"ArmReadyBoth");
		%obj.hDefaultThread = "ArmReadyBoth";
		
		%lhand =  "0";
		%hip =  "0";
		%faceName =  "blank";
		%rarmColor =  "0.5 0 0 0.75";
		%hipColor =  "0.5 0 0 0.75";
		%chest =  "0";
		%rarm =  "1";
		%decalName =  "AAA-None";
		%larmColor =  "0.5 0 0 0.75";
		%larm =  "1";
		%chestColor =  "0.5 0 0 0.75";
		%accentColor =  "0.5 0 0 0.75";
		%rhandColor =  "0.5 0 0 0.75";
		%rleg =  "0";
		%accent =  "1";
		%headColor =  "0.5 0 0 0.75";
		%rhand =  "0";
		%lleg =  "0";
		%lhandColor =  "0.5 0 0 0.75";
	
		%hat =  "0";
		%hatColor =  "0.5 0 0 0.75";
		%pack =  "0";
		%packColor =  "0.5 0 0 0.75";
		%secondPack =  "0";
		%secondPackColor =  "0.5 0 0 0.75";
		%llegColor =  "0.5 0 0 0.75";
		%rlegColor =  "0.5 0 0 0.75";
		
		//Solar Apoc
		%obj.immuneToSun = true;
		%obj.immuneToFire = true;
		%obj.immuneToNight = true;
		%obj.tripleFire = true;
		
		%obj.setPlayerScale("2.0 2.0 2.0");
		%obj.setNodeColor("ALL","0 0 0 0.5");
		%obj.hideNode("lshoe");
		%obj.hideNode("Rshoe");
	}
	else if (%obj.getDataBlock().getName() $= "BanditHoleBot")
	{
		%obj.playthread(1,"ArmReadyBoth");
		%obj.hDefaultThread = "ArmReadyBoth";
		
		%llegColor =  "0 0 1 1";
		%secondPackColor =  "0 0 0 1";
		%lhand =  "0";
		%hip =  "0";
		%faceName =  "asciiTerror";
		%rarmColor =  "0 0 1 1";
		%hatColor =  "1 1 1 1";
		%hipColor =  "0 0 1 1";
		%chest =  "0";
		%rarm =  "0";
		%packColor =  "0 0 0 1";
		%pack =  "0";
		//%decalName =  "HCZombie";
		%larmColor =  "0.9 0 0 1";
		%secondPack =  "0";
		%larm =  "0";
		%chestColor =  "0.9 0.9 0.9 1";
		%accentColor =  "0 0 0 1";
		%rhandColor =  "1.000 0.878 0.611 1";
		%rleg =  "0";
		%rlegColor =  "0 0.141 0.333 1";
		%accent =  "1";
		%headColor =  "1.000 0.878 0.611 1";
		%rhand =  "0";
		%lleg =  "0";
		%lhandColor =  "1.000 0.878 0.611 1";
		%hat =  "0";
		
		//Solar Apoc
		%obj.immuneToSun = false;
		%obj.immuneToFire = false;
		%obj.immuneToNight = true;
		%obj.tripleFire = false;
		
		%rng = getRandom(1,100);
		
			if (%rng < 10)
			%obj.schedule(300,"setWeapon",revolverImage);
		else if (%rng < 30)
			%obj.schedule(300,"setWeapon",ak47Image);
		else if (%rng < 60)
			%obj.schedule(300,"setWeapon",mac10Image);
		else if (%rng < 100)
			%obj.schedule(300,"setWeapon",pistolImage);
		else
			%obj.schedule(300,"setWeapon",bfg10kImage);
	}
	else if (%obj.getDataBlock().getName() $= "BehemothHoleBot") //0.914 0.898 0.629 1
	{
		%obj.playthread(1,"ArmReadyBoth");
		%obj.hDefaultThread = "ArmReadyBoth";
		
		%llegColor =  "0 0 0 1";
		%secondPackColor =  "0 0 0 1";
		%lhand =  "1";
		%hip =  "1";
		%faceName =  "Orc";
		%rarmColor =  "0 0 0 1";
		%hatColor =  "0 0 0 1";
		%hipColor =  "0 0 0 1";
		%chest =  "0";
		%rarm =  "0";
		%packColor =  "0 0 0 1";
		%pack =  "0";
		%decalName =  "HCZombie";
		%larmColor =  "0 0 0 1";
		%secondPack =  "0";
		%larm =  "1";
		%chestColor =  "0 0 0 1";
		%accentColor =  "0 0 0";
		%rhandColor =  "1 0 0 1";
		%rleg =  "0";
		%rlegColor =  "0 0 0 1";
		%accent =  "1";
		%headColor =  "0 0 0 1";
		%rhand =  "1";
		%lleg =  "0";
		%lhandColor =  "0 0 0 1";
		%hat =  "0";
		
		//Solar Apoc
		%obj.immuneToSun = true;
		%obj.immuneToFire = true;
		%obj.immuneToNight = true;
		%obj.tripleFire = true;
		
		%obj.setPlayerScale("4 4 4");
	}
	else if (striPos(%obj.getDataBlock().getName(),"InfernalBlob") != -1)
	{
		%obj.playthread(1,"ArmReadyBoth");
		%obj.hDefaultThread = "ArmReadyBoth";

		%llegColor =  "0.75 0.75 0.75 1";
		%secondPackColor =  "0 0 0 1";
		%lhand =  "0";
		%hip =  "1";
		%faceName =  "asciiTerror";
		%rarmColor =  "0 0 0 1";
		%hatColor =  "0 0 0 1";
		%hipColor =  "0.5 0.5 0 1";
		%chest =  "0";
		%rarm =  "0";
		%packColor =  "0 0 0 1";
		%pack =  "0";
		%decalName =  "AAA-None";
		%larmColor =  "0 0 0 1";
		%secondPack =  "0";
		%larm =  "1";
		%chestColor =  "0 0 0 1";
		%accentColor =  "0 0 0 1";
		%rhandColor =  "0 0 0 1";
		%rleg =  "0";
		%rlegColor =  "0.75 0.75 0.75 1";
		%accent =  "1";
		%headColor =  "0 0 0 1";
		%rhand =  "0";
		%lleg =  "0";
		%lhandColor =  "0 0 0 1";
		%hat =  "0";
		
		if (striPos(%obj.getDataBlock().getName(),"Melee") != -1)
		{
			%llegColor =  "0.750 0.000 0.000 1";
			%rlegColor =  "0.750 0.000 0.000 1";
		}
		else if (striPos(%obj.getDataBlock().getName(),"Ranged") != -1)
		{
			%llegColor =  "0.000 0.750 0.000 1";
			%rlegColor =  "0.000 0.750 0.000 1";
		}
		else if (striPos(%obj.getDataBlock().getName(),"Magic") != -1)
		{
			%llegColor =  "0.000 0.000 0.750 1";
			%rlegColor =  "0.000 0.000 0.750 1";
		}
	}
	else //Default Zombie
	{
	
		%obj.playthread(1,"ArmReadyBoth");
		%obj.hDefaultThread = "ArmReadyBoth";
		
		%llegColor =  "0 0.141 0.333 1";
		%secondPackColor =  "0 0.435 0.831 1";
		%lhand =  "0";
		%hip =  "0";
		%faceName =  "asciiTerror";
		%rarmColor =  "0.593 0 0 1";
		%hatColor =  "1 1 1 1";
		%hipColor =  "0 0.141 0.333 1";
		%chest =  "0";
		%rarm =  "0";
		%packColor =  "0.2 0 0.8 1";
		%pack =  "0";
		%decalName =  "AAA-None";
		%larmColor =  "0.593 0 0 1";
		%secondPack =  "0";
		%larm =  "0";
		%chestColor =  "0.75 0.75 0.75 1";
		%accentColor =  "0.990 0.960 0 0.700";
		%rhandColor =  "0.626 0.71 0.453 1";
		%rleg =  "0";
		%rlegColor =  "0 0.141 0.333 1";
		%accent =  "1";
		%headColor =  "0.626 0.71 0.453 1";
		%rhand =  "0";
		%lleg =  "0";
		%lhandColor =  "0.626 0.71 0.453 1";
		%hat =  "0";
		
		//Solar Apoc
		%obj.immuneToSun = false;
		%obj.immuneToFire = false;
		%obj.immuneToNight = true;
		%obj.tripleFire = false;
	}
	
	if (getRandom(0,100) == 1) //rare memes!!! //
		%faceName = "memeYaranika";
	
	servercmdupdatebodyparts(%obj, %hat, %accent, %pack, %secondPack, %chest, %hip, %LLeg, %RLeg, %LArm, %RArm, %LHand, %RHand);
	servercmdupdatebodycolors(%obj, %headColor, %hatColor, %accentColor, %packColor, %secondPackColor, %chestColor, %hipColor, %LLegColor, %RLegColor, %LArmColor, %RArmColor, %LHandColor, %RHandColor, %decalName, %faceName);
	
	GameConnection::ApplyBodyParts(%obj);
	GameConnection::ApplyBodyColors(%obj);
	
	if (%obj.getDatablock().getName() $= "ChaosElementalHoleBot")
	{
		%obj.setPlayerScale("2.0 2.0 2.0");
		%obj.setNodeColor("ALL","0 0 0 0.5");
	}
	
	if (striPos(%obj.getDataBlock().getName(),"InfernalBlob") != -1)
	{
		%obj.hideNode("ALL");
		%obj.unHideNode("skirtHip");
		%obj.unHideNode("lShoe");
		%obj.unHideNode("rshoe");
		
		if (%obj.getDataBlock().getName() $= "InfernalBlobHoleBot")
			%obj.setPlayerScale("2 2 2");
			
		%obj.setNodeColor("lShoe",%llegColor);
		%obj.setNodeColor("rShoe",%rlegColor);
	}
	
}

function EnemySpawnLoop()
{
	cancel($EnemySpawnLoop);
	if (!isObject(EOTWEnemies)) new SimGroup(EOTWEnemies);
	//echo($EOTW::Time);
	if($EOTW::Day >= 11 && $EOTW::Day < 40 && $EOTW::Time > 180) for(%i=0;%i<33;%i++) %type[-1+%types++] = UnfleshedHoleBot; //Day 19
	if($EOTW::Day >= 24 && $EOTW::Day < 44 && $EOTW::Time < 180) for(%i=0;%i<21;%i++) %type[-1+%types++] = HuskHoleBot;
	if($EOTW::Day >= 29 && $EOTW::Day < 49 && $EOTW::Time > 180) for(%i=0;%i<15;%i++) %type[-1+%types++] = RevenantHoleBot;
	if($EOTW::Day >= 34 && $EOTW::Day < 54 && $EOTW::Time > 180) for(%i=0;%i<4;%i++) %type[-1+%types++] = ElementalHoleBot;
	if($EOTW::Day >= 36 && $EOTW::Day < 56 && $EOTW::Time > 180) for(%i=0;%i<6;%i++) %type[-1+%types++] = BanditHoleBot;
	if($EOTW::Day >= 40 && $EOTW::Day < 54 && $EOTW::Time > 180) for(%i=0;%i<2;%i++) %type[-1+%types++] = ChaosElementalHoleBot;
	if($EOTW::Day >= 42 && $EOTW::Day < 54 && $EOTW::Time > 180) for(%i=0;%i<1;%i++) %type[-1+%types++] = DragonHoleBot;
	if($EOTW::Day >= 39 && $EOTW::Day < 55 && $EOTW::Time > 180) for(%i=0;%i<3;%i++) %type[-1+%types++] = InfernalBlobHoleBot;
	//if($EOTW::Day >= 0 && $EOTW::Day < 40 && $EOTW::Time > 180) for(%i=0;%i<33;%i++) %type[-1+%types++] = UnfleshedHoleBot;
	//if($EOTW::Day >= 0 && $EOTW::Day < 44 && $EOTW::Time < 180) for(%i=0;%i<21;%i++) %type[-1+%types++] = HuskHoleBot;
	//if($EOTW::Day >= 0 && $EOTW::Day < 49 && $EOTW::Time > 180) for(%i=0;%i<15;%i++) %type[-1+%types++] = RevenantHoleBot;
	//if($EOTW::Day >= 0 && $EOTW::Day < 54) for(%i=0;%i<11;%i++) %type[-1+%types++] = ElementalHoleBot;
	//if($EOTW::Day >= 0 && $EOTW::Day < 56 && $EOTW::Time > 180) for(%i=0;%i<7;%i++) %type[-1+%types++] = BanditHoleBot;
	if(isObject(EOTWEnemies) && EOTWEnemies.getCount() < $Server::Pref::EOTWMaxEnemies && ClientGroup.getCount() > 0 && $EOTW::TimeScale > 0)
	{
		%victim = ClientGroup.getObject(getRandom(0,ClientGroup.getCount() - 1)).player;
		%select = %type[getRandom(0, 99)];  //0, 99
		
		if(%select !$= "" && isObject(%victim) && (getSimTime() - %victim.lastRepellant) > 4000)
		{
			//%trans = (getRandom(0, 1664) / 2 + 0.25) SPC (getRandom(0, 1664) / 2 + 0.25) SPC 128; //Spawn them over the map, rather than using raycast.
			%initPos = (getWord(%victim.getPosition(),0) + getRandom(-128, 128)) TAB (getWord(%victim.getPosition(),1) + getRandom(-128, 128));
			
			%spawnCount = 1;
			if (%select.getName() $= "UnfleshedHoleBot") %spawnCount = getRandom(2,5);
			if (%select.getName() $= "HuskHoleBot") %spawnCount = getRandom(1,3);
			if (%select.getName() $= "RevenantHoleBot") %spawnCount = getRandom(2,3);
			if (%select.getName() $= "ElementalHoleBot") %spawnCount = getRandom(1,2);
			if (%select.getName() $= "BanditHoleBot") %spawnCount = getRandom(1,2);
			
			for (%i = 0; %i<%spawnCount; %i++)
			{
				%trans = EOTW_getEnemyRaycastSpawnLocation(%initPos);
				%trans = %trans SPC eulerToAxis("0 0" SPC getRandom(0,359));
				if (getWord(%trans,0) > 64 && getWord(%trans,0) < 1660 && getWord(%trans,1) > 64 && getWord(%trans,1) < 1660)
					spawnNewZombie(%trans,%select);
			}

		}		
	}
	if ($EOTW::Day < 41)
		%respawn = 8100 - ($EOTW::Day * 0.1) - $EOTW::MonSpawnMod;
	else
		%respawn = 4000 - $EOTW::MonSpawnMod;
		
	$EnemySpawnLoop = schedule(%respawn, 0, "EnemySpawnLoop");
}

function EnemySpawnSwarm(%monster,%counter)
{
	cancel($EnemySpawnSwarm);
	echo(%monster TAB %counter);
	if ($EnemySpawnSwarm != 0 && %monster !$= "")
	{
		if (!isObject(EOTWEnemies)) new SimGroup(EOTWEnemies);
		
		if (%counter > 9)
		{
			messageAll('MsgAdminForce',"\c5The " @ %monster.hName @ " swarm has ended.");
			$EnemySpawnSwarm = "";
			return;
		}
		if(isObject(EOTWEnemies) && EOTWEnemies.getCount() < ($Server::Pref::EOTWMaxEnemies * 1.5))
		{
			//TODO: empty client group causes error here:
			%victim = ClientGroup.getObject(getRandom(0,ClientGroup.getCount() - 1)).player;
			
			if(%monster !$= "" && isObject(%victim) && (getSimTime() - %victim.lastRepellant) > 4000)
			{
				%initPos = (getWord(%victim.getPosition(),0) + getRandom(-128, 128)) TAB (getWord(%victim.getPosition(),1) + getRandom(-128, 128));
				
				
				if (%monster.getName() $= "UnfleshedHoleBot") %spawnCount = getRandom(2,5);
				if (%monster.getName() $= "SwarmerHoleBot") %spawnCount = getRandom(4,6);
				if (%monster.getName() $= "RevenantHoleBot") %spawnCount = getRandom(2,3);
				
				for (%i = 0; %i<%spawnCount; %i++)
				{
					%trans = EOTW_getEnemyRaycastSpawnLocation(%initPos);
					%trans = %trans SPC eulerToAxis("0 0" SPC getRandom(0,359));
					if (getWord(%trans,0) > 64 && getWord(%trans,0) < 1660 && getWord(%trans,1) > 64 && getWord(%trans,1) < 1660)
						spawnNewZombie(%trans,%monster);
				}
			}
		}
		
		if ($EOTW::Day < 41)
			%respawn = 8100 - ($EOTW::Day * 0.1) - $EOTW::MonSpawnMod;
		else
			%respawn = 4000 - $EOTW::MonSpawnMod;
		
		$EnemySpawnSwarm = schedule((%respawn / 2), 0, "EnemySpawnSwarm",%monster,%counter + 1);
		echo($EnemySpawnSwarm);
	}
	else if ($EnemySpawnSwarm == 0 && %monster $= "")
	{
		if (!isObject(EOTWEnemies)) new SimGroup(EOTWEnemies);
		
		if($EOTW::Day >= 11 && $EOTW::Day < 40 && $EOTW::Time > 180) for(%i=0;%i<33;%i++) %type[-1+%types++] = UnfleshedHoleBot; //Day 19
		if($EOTW::Day >= 11 && $EOTW::Day < 44 && $EOTW::Time > 180) for(%i=0;%i<21;%i++) %type[-1+%types++] = SwarmerHoleBot;
		if($EOTW::Day >= 29 && $EOTW::Day < 49 && $EOTW::Time > 180) for(%i=0;%i<15;%i++) %type[-1+%types++] = RevenantHoleBot;
		if($EOTW::Day >= 39 && $EOTW::Day < 55 && $EOTW::Time > 180) for(%i=0;%i<15;%i++) %type[-1+%types++] = InfernalBlobHoleBot;
		
		//for(%i=0;%i<33;%i++) %type[-1+%types++] = UnfleshedHoleBot; //Day 19
		//for(%i=0;%i<21;%i++) %type[-1+%types++] = SwarmerHoleBot;
		//for(%i=0;%i<15;%i++) %type[-1+%types++] = RevenantHoleBot;
		
		%select = %type[getRandom(0, %types)];
		
		if (%select !$= "")
		{
			%messages = -1;
			%messageList[%messages++] = "Arm yourselves!";
			%messageList[%messages++] = "Don't die.";
			%messageList[%messages++] = "Buck up and get ready!";
			%messageList[%messages++] = "Uh oh, spaghetti o's!";
			messageAll('MsgAdminForce',"\c5A swarm of " @ %select.hName @ " is approaching! " @ %messageList[getRandom(0,%messages)]);
			$EnemySpawnSwarm = schedule(1000,EOTWEnemies,"EnemySpawnSwarm",%select,0);
		}
	}
}

function EOTW_getEnemyRaycastSpawnLocation(%initPos)
{
	if (%initPos !$= "")
		%eye = (getField(%initPos,0) + getRandom(-32, 32) / 2 + 0.25) SPC (getField(%initPos,1) + getRandom(-32, 32) / 2 + 0.25) SPC 128; //getRandom(0, 1664)
	else
		%eye = (getRandom(128, 1600) / 2 + 0.25) SPC (getRandom(128, 1600) / 2 + 0.25) SPC 128;
	%dir = "0 0 -1";
	%for = "0 1 0";
	%face = getWords(vectorScale(getWords(%for, 0, 1), vectorLen(getWords(%dir, 0, 1))), 0, 1) SPC getWord(%dir, 2);
	%mask = $Typemasks::fxBrickAlwaysObjectType | $Typemasks::TerrainObjectType;
	%ray = containerRaycast(%eye, vectorAdd(%eye, vectorScale(%face, 256)), %mask, %this);
	//echo(%eye TAB %dir TAB %for TAB %face TAB %mask TAB %ray);
	%pos = getWord(%ray,1) SPC getWord(%ray,2) SPC (getWord(%ray,3) + 0.1);
	if(isObject(%hit = firstWord(%ray)))
	{
		%pos = vectorAdd(%pos,"0 0 5");
		return %pos;
	}
}

function ServerCmdKillBots(%cl, %mod)
{
	if (!%cl.isAdmin) return;
	
	%kills = 0;
	if (%mod $= 0) %mod = 1;
	
	for (%i=0;%i<EOTWEnemies.getCount();%i++)
	{
		%bot = EOTWEnemies.getObject(%i);
		
		if ((%i % %mod) == 0)
		{
			%bot.kill();
			%kills++;
		}
	
	}
	
	talk("Killed " @ %kills @ "/" @ EOTWEnemies.getCount() @ " bots.");
}

function EOTW_PlayerSearchLoop()
{
	if (!isObject(EOTWEnemies)) new SimGroup(EOTWEnemies);
	
	for (%i=0;%i<EOTWEnemies.getCount();%i++)
	{
		%bot = EOTWEnemies.getObject(%i);
		
		initContainerRadiusSearch(%bot.getTransform(), 128, $TypeMasks::PlayerObjectType);
		while(isObject(%obj = containerSearchNext()))
		{
			if (isObject(%obj.client))
			{
				%playerExists = true;
				break;
			}
		}
		
		if (%bot.getDataBlock().getName() !$= "SturdiumBeastHoleBot" && !%playerExists)
		{
			%bot.InactiveLevel++;
			
			if (%bot.InactiveLevel > 10)
				%bot.kill();
		}
		else
		{
			%bot.InactiveLevel = 0;
		}
	}
	
	$EOTWPlayerSearch = schedule(10000,0,"EOTW_PlayerSearchLoop");
}
