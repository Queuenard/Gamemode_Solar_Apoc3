datablock ItemData(InvExpanderItem)
{
	category = "Weapon";  // Mission editor category
	className = "Weapon"; // For inventory system

	 // Basic Item Properties
	shapeFile = "./models/ISBackPackItem.dts";
	rotate = false;
	mass = 1;
	density = 0.2;
	elasticity = 0.2;
	friction = 0.6;
	emap = true;

	//gui stuff
	uiName = "Inventory Expander";
	iconName = "./images/BackpackIcon";
	doColorShift = false;

	 // Dynamic properties defined by the scripts
	image = InvExpanderImage;
	canDrop = true;
};

datablock ShapeBaseImageData(InvExpanderImage)
{
   // Basic Item properties
   shapeFile = "./models/ISBackPackItem.dts";
   emap = true;

   // Specify mount point & offset for 3rd person, and eye offset
   // for first person rendering.
   mountPoint = 0;
   offset = "0.09 -0.07 -0.2";
   eyeOffset = 0; //"0.7 1.2 -0.5";
   rotation = eulerToMatrix( "0 0 0" );

   className = "WeaponImage";
   item = InvExpanderItem;

   //raise your arm up or not
   armReady = true;

   doColorShift = false;

   // Initial start up state
   
    stateName[0]                     = "Activate";
	stateTimeoutValue[0]             = 0.1;
	stateTransitionOnTimeout[0]      = "Ready";
	
	stateName[1]                     = "Ready";
	stateTransitionOnTriggerDown[1]  = "Fire";
	stateAllowImageChange[1]         = true;

	stateName[2]                     = "Fire";
	stateTransitionOnTimeout[2]      = "Ready";
	stateAllowImageChange[2]         = true;
	stateScript[2]                   = "onFire";
	stateTimeoutValue[2]		     = 1.0;
};

function InvExpanderImage::onFire(%this,%obj,%slot)
{
	%cl = %obj.client;
	%currSlot = %obj.currTool;
	
	
	%cl.AwardAchievement("Time for an Upgrade");
	
	if ($EOTW::PlayerInvSize[%cl.bl_id] >= 11)
	{
		%cl.AwardAchievement("Maxed Out!");
		%cl.centerPrint("\c0Whoops!<br>\c6You already maxed out your inventory space!",3);
		%cl.play2d(errorSound);
		return;
	}
	
	%obj.emote(InvExpandImage,1);
	%obj.tool[%currSlot] = 0;
	%obj.weaponCount--;
	
	if(isObject(%cl))
	{
		messageClient(%cl,'MsgItemPickup','',%currSlot,0);
		serverCmdUnUseTool(%cl);
	}
	
	$EOTW::PlayerInvSize[%cl.bl_id]++;
	%obj.changeDatablock("Player" @ $EOTW::PlayerInvSize[%cl.bl_id] @ "SlotPlayer");
	if ($EOTW::PlayerInvSize[%cl.bl_id] >= 11)
	{
		%cl.AwardAchievement("Maxed Out!");
		%cl.centerPrint("\c4You increased your inventory space by 1!<br>The effect will stay until the server ends.<br>Your inventory space has maxed out.",5);
	}
	else
		%cl.centerPrint("\c4You increased your inventory space by 1!<br>The effect will stay until the server ends.",3);
		
	%obj.unMountImage(%slot);
}

datablock ItemData(HealthExpanderItem)
{
	category = "Weapon";  // Mission editor category
	className = "Weapon"; // For inventory system

	 // Basic Item Properties
	shapeFile = "./models/Heart.dts";
	rotate = false;
	mass = 1;
	density = 0.2;
	elasticity = 0.2;
	friction = 0.6;
	emap = true;

	//gui stuff
	uiName = "Heart Vessel";
	//iconName = "./images/BackpackIcon";
	doColorShift = false;

	 // Dynamic properties defined by the scripts
	image = HealthExpanderImage;
	canDrop = true;
};

datablock ShapeBaseImageData(HealthExpanderImage)
{
   // Basic Item properties
   shapeFile = "./models/Heart.dts";
   emap = true;

   // Specify mount point & offset for 3rd person, and eye offset
   // for first person rendering.
   mountPoint = 0;
   offset = "0.09 -0.07 -0.2";
   eyeOffset = 0; //"0.7 1.2 -0.5";
   rotation = eulerToMatrix( "0 0 0" );

   className = "WeaponImage";
   item = HealthExpanderItem;

   //raise your arm up or not
   armReady = true;

   doColorShift = false;

   // Initial start up state
   
    stateName[0]                     = "Activate";
	stateTimeoutValue[0]             = 0.15;
	stateTransitionOnTimeout[0]      = "Ready";
	
	stateName[1]                     = "Ready";
	stateTransitionOnTriggerDown[1]  = "Fire";
	stateAllowImageChange[1]         = true;

	stateName[2]                     = "Fire";
	stateTransitionOnTimeout[2]      = "Ready";
	stateAllowImageChange[2]         = true;
	stateScript[2]                   = "onFire";
	stateTimeoutValue[2]		     = 1.0;
};

function HealthExpanderImage::onFire(%this,%obj,%slot)
{
	%cl = %obj.client;
	%currSlot = %obj.currTool;
	
	%cl.AwardAchievement("Anti-Pomeg");
	
	if ($EOTW::PlayerMaxHealth[%cl.bl_id] >= 180)
	{
		%cl.AwardAchievement("Superpower");
		
		%cl.centerPrint("\c0Whoops!<br>\c6You already maxed out your health!",3);
		%cl.play2d(errorSound);
		return;
	}
	
	//%obj.emote(InvExpandImage,1);
	%obj.setWhiteOut(0.5);
	%obj.tool[%currSlot] = 0; 
	%obj.weaponCount--;
	
	if(isObject(%cl))
	{
		messageClient(%cl,'MsgItemPickup','',%currSlot,0);
		serverCmdUnUseTool(%cl);
	}
		
	
	$EOTW::PlayerMaxHealth[%cl.bl_id] += 10;
	%obj.setMaxHealth($EOTW::PlayerMaxHealth[%cl.bl_id]);
	EOTW_UpdatePlayerStats(%cl);
	if ($EOTW::PlayerMaxHealth[%cl.bl_id] >= 180)
	{
		%cl.AwardAchievement("Superpower");
		%cl.centerPrint("\c4You increased your max health by 10!<br>The effect will stay until the server ends.<br>Your max health has maxed out.",5);
	}
	else
		%cl.centerPrint("\c4You increased your max health by 10!<br>The effect will stay until the server ends.",3);
		
	%obj.unMountImage(%slot);
}

function HealthExpanderImage::onMount(%this, %obj, %slot)
{
		parent::onMount(%this, %obj, %slot);
		
		%obj.client.centerPrint("\c6Left click to use this item.",3);
}
