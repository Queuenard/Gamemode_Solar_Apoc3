function fxDtsBrick::EOTW_GrowGrass(%brick)
{
	if(!isObject(%brick) || %brick.isDead()) return;
	if(!isObject(PlantGroup)) new SimSet(PlantGroup);
	if(!PlantGroup.isMember(%brick)) PlantGroup.add(%brick);
	if(!isObject("Plants_" @ %brick.getGroup().bl_id)) new SimSet("Plants_" @ %brick.getGroup().bl_id);
	if(!("Plants_" @ %brick.getGroup().bl_id).isMember(%brick)) ("Plants_" @ %brick.getGroup().bl_id).add(%brick);
	%time = vectorDist(%brick.last, %now = getSimTime()) / 1000;
	%pi = 3.14159265; %val = ($EOTW::Time / 180) * %pi;
	%ang = ($EnvGuiServer::SunAzimuth / 180) * %pi;
	%dir = vectorScale(mSin(%ang) * mCos(%val) SPC mCos(%ang) * mCos(%val) SPC mSin(%val), 512);
	%depth = 0;
	
	//findclientbyname("fast").chatMessage(%brick.material @ " " @ %brick @ " dark:" @ %brick.darkTime @ " grow:" @ %brick.growTime @ " fry:" @ %brick.fryTime @ " time:" @ %time);
	
	for(%i=0;%i<3;%i++)
	{
		%ray = containerRaycast(vectorAdd(%pos = %brick.getPosition(), %dir), %pos, $Typemasks::fxBrickAlwaysObjectType, %brick, %hit0, %hit1, %hit2);
		if(isObject(%hit = firstWord(%ray)) && %hit != %brick)
			if(getWord(getColorIDTable(%hit.getColorID()), 3) > 0.95 && %hit.isRendering() && getWord(%name = %hit.getDatablock().uiName, getWordCount(%name) - 1) !$= "Window") { %dark = 1; %depth = 1; }
			else { %hit[%i] = %hit; %depth = 1; }
	}
	if($EOTW::Time > 180)
	{
		%brick.darkTime += %time;
		%brick.growTime = 0;
		%brick.fryTime = 0;
	}
	else if((%depth == 0 && $EOTW::Day >= 7) || $EOTW::Day >= 37)
	{
		%brick.darkTime = 0;
		%brick.growTime = 0;
		%brick.fryTime += %time;
		if(%brick.fryTime >= 5)
		{
			%brick.dontRefund = 1;
			%brick.isIgnitable = 1;
			%brick.unrecolorable = 0;
			%brick.setColor($EOTW::Colors::Dead);
			%brick.material = "Dead_Plant";
			%brick.unrecolorable = 1;
			
			PlantGroup.remove(%brick);
			("Plants_" @ %brick.getGroup().bl_id).remove(%brick);
					
			return;
		}
	}
	else if(!%dark && ("Plants_" @ %brick.getGroup().bl_id).getCount() < 250)
	{
		%brick.darkTime = 0;
		%brick.growTime += %time;
		%brick.fryTime = 0;
		if(%brick.growTime >= 15)
		{
			%brick.growTime -= 15;
			%dir = getField("0.0 0.5 0.0	0.5 0.0 0.0	0.0 -0.5 0.0	-0.5 0.0 0.0", getRandom(0, 3));
			if(isObject(%ray = containerRaycast(%pos = %brick.getPosition(), %spawn = vectorAdd(%pos, %dir), $Typemasks::fxBrickAlwaysObjectType)))
			{
				if(%ray.material !$= "Dead_Plant" && %ray.material !$= "Grass" && %ray.material !$= "Vine" && %ray.isColliding())
				if(!isObject(containerRaycast(%pos, %high = vectorAdd(%pos, "0.0 0.0 0.2"), $Typemasks::fxBrickAlwaysObjectType)))
					if(!isObject(containerRaycast(%high, %spawn = vectorAdd(%high, %dir), $Typemasks::fxBrickAlwaysObjectType)))
						%spawnPos = %spawn;
			}
			else if(isObject(%ray = containerRaycast(%spawn, %low = vectorAdd(%spawn, "0.0 0.0 -0.2"), $Typemasks::fxBrickAlwaysObjectType)) && %ray.isColliding())
			{ if(%ray.material !$= "Dead_Plant" && %ray.material !$= "Grass" && %ray.material !$= "Vine") %spawnPos = %spawn; }
			else if(isObject(%ray = containerRaycast(%low, %bottom = vectorAdd(%low, "0.0 0.0 -0.2"), $Typemasks::fxBrickAlwaysObjectType)) && %ray.isColliding())
			{ if(%ray.material !$= "Dead_Plant" && %ray.material !$= "Grass" && %ray.material !$= "Vine") %spawnPos = %low; }
			if(%spawnPos !$= "")
			{
				%data = EOTW_CreateBrick(%brick.getGroup(), %brick.getDatablock(), %spawnPos, $EOTW::Colors::Grass);
				%newBrick = getField(%data, 0);
				if(getField(%data, 1))
					%newBrick.delete();
				else
				{
					%newBrick.canBeFireballed = 1;
					%newBrick.isBreakable = 1;
					%newBrick.isFlammable = 1;
					%newBrick.material = "Grass";
					%newBrick.unrecolorable = 1;
					%newBrick.setColliding(0);
					%newBrick.last = getSimTime();
					
					if (isObject(%client = findClientByBL_ID(%brick.getGroup().bl_id)))
						%client.AddAchievementProgress("Botanist",1);
					
					PlantGroup.add(%newBrick);
					("Plants_" @ %brick.getGroup().bl_id).add(%newBrick);
				}
			}
		}
	}
	else
	{
		%brick.darkTime += %time;
		%brick.growTime = 0;
		%brick.fryTime = 0;
	}
	if(%brick.darkTime >= 2100)
	{
		%brick.dontRefund = 1;
		%brick.isIgnitable = 1;
		%brick.unrecolorable = 0;
		%brick.setColor($EOTW::Colors::Dead);
		%brick.material = "Dead_Plant";
		%brick.unrecolorable = 1;
		return;
	}
	
	%brick.last = %now;
}

function fxDtsBrick::EOTW_GrowVine(%brick)
{
	if(!isObject(%brick) || %brick.isDead()) return;
	if(!isObject(PlantGroup)) new SimSet(PlantGroup);
	if(!PlantGroup.isMember(%brick)) PlantGroup.add(%brick);
	if(!isObject("Plants_" @ %brick.getGroup().bl_id)) new SimSet("Plants_" @ %brick.getGroup().bl_id);
	if(!("Plants_" @ %brick.getGroup().bl_id).isMember(%brick)) ("Plants_" @ %brick.getGroup().bl_id).add(%brick);
	
	%time = vectorDist(%brick.last, %now = getSimTime()) / 1000;
	%pi = 3.14159265; %val = ($EOTW::Time / 180) * %pi;
	%ang = ($EnvGuiServer::SunAzimuth / 180) * %pi;
	%dir = vectorScale(mSin(%ang) * mCos(%val) SPC mCos(%ang) * mCos(%val) SPC mSin(%val), 512);
	%depth = 0;
	
	//findclientbyname("fast").chatMessage(%brick.material @ " " @ %brick @ " dark:" @ %brick.darkTime @ " grow:" @ %brick.growTime @ " fry:" @ %brick.fryTime @ " time:" @ %time);
	
	for(%i=0;%i<3;%i++)
	{
		%ray = containerRaycast(vectorAdd(%pos = %brick.getPosition(), %dir), %pos, $Typemasks::fxBrickAlwaysObjectType, %brick, %hit0, %hit1, %hit2);
		if(isObject(%hit = firstWord(%ray)) && %hit != %brick)
			if(getWord(getColorIDTable(%hit.getColorID()), 3) > 0.95 && %hit.isRendering() && getWord(%name = %hit.getDatablock().uiName, getWordCount(%name) - 1) !$= "Window") { %dark = 1; %depth = 1; }
			else { %hit[%i] = %hit; %depth = 1; }
	}
	if($EOTW::Time > 180)
	{
		%brick.darkTime += %time;
		%brick.growTime = 0;
		%brick.fryTime = 0;
	}
	else if((%depth == 0 && $EOTW::Day >= 14) || $EOTW::Day >= 47)
	{
		%brick.darkTime = 0;
		%brick.growTime = 0;
		%brick.fryTime += %time;
		if(%brick.fryTime >= 5)
		{
			%brick.dontRefund = 1;
			%brick.isIgnitable = 1;
			%brick.unrecolorable = 0;
			%brick.setColor($EOTW::Colors::Dead);
			%brick.material = "Dead_Plant";
			%brick.unrecolorable = 1;
			
			PlantGroup.remove(%brick);
			("Plants_" @ %brick.getGroup().bl_id).remove(%brick);
			
			return;
		}
	}
	else if(!%dark && ("Plants_" @ %brick.getGroup().bl_id).getCount() < 250)
	{
		%brick.darkTime = 0;
		%brick.growTime += %time;
		%brick.fryTime = 0;
		initContainerBoxSearch(%pos = %brick.getPosition(), "0.55 0.55 0.25", $Typemasks::fxBrickAlwaysObjectType);
		while(isObject(%test = containerSearchNext()))
			if(%test.material $= "Wood" && %test.isColliding())
				%wooden = 1;
		if(%brick.growTime >= (%wooden ? 5 : 15))
		{
			%brick.growTime -= 3;
			%dir = getField("0.0 0.5 0.0	0.5 0.0 0.0	0.0 -0.5 0.0	-0.5 0.0 0.0	0.0 0.0 0.2	0.0 0.0 -0.2", getRandom(0, 5));
			if(!isObject(%ray = containerRaycast(%pos, %spawn = vectorAdd(%pos, %dir), $Typemasks::fxBrickAlwaysObjectType)) && getWord(%spawn, 2) > 0.05)
			{
				initContainerBoxSearch(%spawn, "0.55 0.55 0.25", $Typemasks::fxBrickAlwaysObjectType);
				while(isObject(%test = containerSearchNext()))
					if(%test.material !$= "Dead_Plant" && %test.material !$= "Grass" && %test.material !$= "Vine" && %test.isColliding())
						%supported = 1;
				if(%supported)
				{
					%data = EOTW_CreateBrick(%brick.getGroup(), %brick.getDatablock(), %spawn, $EOTW::Colors::Vine);
					%newBrick = getField(%data, 0);
					if((%error = getField(%data, 1)) && %error != 2)
						%newBrick.delete();
					else
					{
						%brick.growTime -= 12;
						%newBrick.canBeFireballed = 1;
						%newBrick.isBreakable = 1;
						%newBrick.isFlammable = 1;
						%newBrick.material = "Vine";
						%newBrick.unrecolorable = 1;
						%newBrick.setColliding(0);
						%newBrick.last = getSimTime();
						
						if (isObject(%client = findClientByBL_ID(%brick.getGroup().bl_id)))
							%client.AddAchievementProgress("Botanist",1);
						
						PlantGroup.add(%newBrick);
						("Plants_" @ %brick.getGroup().bl_id).add(%newBrick);
					}
				}
			}
		}
	}
	else
	{
		%brick.darkTime += %time;
		%brick.growTime = 0;
		%brick.fryTime = 0;
	}
	if(%brick.darkTime >= 6000)
	{
		%brick.dontRefund = 1;
		%brick.isIgnitable = 1;
		%brick.unrecolorable = 0;
		%brick.setColor($EOTW::Colors::Dead);
		%brick.material = "Dead_Plant";
		%brick.unrecolorable = 1;
		return;
	}
	
	%brick.last = %now;
}

/////////////////////

function EOTWPlantLoop(%iteration, %previous)
{
	cancel($EOTW::PlantLoop);
	
	%iteration = %iteration + 0;
	
	for (%i = 0; %i < 20; %i++)
	{
		if (%iteration < PlantGroup.getCount())
		{
			
			if(isObject(%brick = PlantGroup.getObject(%iteration)))
			{
				if (%brick.Material $= "Vine")
				{
					%brick.EOTW_GrowVine();
				}
				else if (%brick.Material $= "Grass")
				{
					%brick.EOTW_GrowGrass();
				}
				
				%previous = %brick;
			}
			
			%iteration++;
		}
		else
		{
			%iteration = 0;
			break;
		}
	}
	
	$EOTW::PlantLoop = schedule(15,0,"EOTWPlantLoop", %iteration, %previous); //16 * PlantGroup.getCount()
}

if(!isObject(PlantGroup))
{
	new SimSet(PlantGroup);
	//EOTWPlantLoop();
} 

package EOTW_PlantLoading
{
	function fxDTSBrick::onLoadPlant(%data, %obj)
	{
		if(isObject(%obj) && %obj.getDatablock().getName() $= "brick1x1fData")
		{
			%obj.schedule(getRandom(1000,3000), "checkIfPlant");
			
		}
		
		return Parent::onLoadPlant(%data, %obj);
	}
	function fxDTSBrick::checkIfPlant(%obj)
	{
		if ((%obj.Material $= "Grass" || %obj.Material $= "Vine") && isObject(%obj.getGroup()) && %obj.getGroup().bl_id !$= 50 && %obj.getGroup().bl_id !$= 888888)
		{
			%obj.last = getSimTime();
			if(!isObject(PlantGroup)) new SimSet(PlantGroup);
			if(!PlantGroup.isMember(%obj)) PlantGroup.add(%obj);
			if(!isObject("Plants_" @ %obj.getGroup().bl_id)) new SimSet("Plants_" @ %obj.getGroup().bl_id);
			if(!("Plants_" @ %obj.getGroup().bl_id).isMember(%obj)) ("Plants_" @ %obj.getGroup().bl_id).add(%obj);
		}
	}
};
activatePackage("EOTW_PlantLoading");
