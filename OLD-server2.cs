function newSolarApoc()
{
	//discoverFile
	exec("Add-Ons/Gamemode_Solar_Apoc3/server.cs");
}

if(!isObject(MapPlane))
	new SimGroup(MapPlane);

if(!isObject(Gatherables))
	MainBrickgroup.add(new SimGroup(Gatherables) { bl_id = 50; name = "<color:FFFFFF>God<color:FF0000>"; });

if(!isObject(Chests))
	MainBrickgroup.add(new SimGroup(Chests) { bl_id = 51; name = "<color:FFFFFF>Jesus<color:FF0000>"; });


$Server::Pref::EOTWMaxGatherables = 7500;
$Server::Pref::EOTWMaxEnemies = 35;
$Game::Item::PopTime = 30 * 1000;
$EOTW::TimeScale = 1;

$Fastmapler::ScoreToCredit = 120;
$Fastmapler::PointsSave = "SolarApoc";

AddDamageType("BurnedToDeath", '%1 burned to death!', '%1 burned to death!', 1, 1);
AddDamageType("EOTWFireball", '%1 was hit by a meteor!', '%1 was hit by a meteor!', 1, 1); //This is fine since they wont happen as often.
//AddDamageType("Sun", '%1 got fried by the Sun!', '%1 got fried by the Sun!', 1, 1);  //Just. No.
//AddDamageType("BurnedToDeath", '', '', 1, 1);
//AddDamageType("EOTWFireball", '', '', 1, 1);
//AddDamageType("Sun", '', '', 1, 1);

//Crafting Recipes. Usage: [%ImageName] = %mat1 TAB %mat1amt TAB %mat2 TAB %mat2amt (and so on)
//$EOTWCrafting["sbShotgunItem"] = "Copper" TAB "15" TAB "Metal" TAB "100";
$EOTWCrafting["ar15Item"] = "Copper" TAB "10" TAB "Metal" TAB "250" TAB "Wolfram" TAB "250";
$EOTWCrafting["cShotgunItem"] = "Copper" TAB "15" TAB "Metal" TAB "150" TAB "Wolfram" TAB "200";
$EOTWCrafting["dbShotgunItem"] = "Copper" TAB "10" TAB "Metal" TAB "150" TAB "Wolfram" TAB "100";
$EOTWCrafting["mac10Item"] = "Copper" TAB "15" TAB "Metal" TAB "150" TAB "Wolfram" TAB "150";
$EOTWCrafting["pistolItem"] = "Copper" TAB "5" TAB "Metal" TAB "100" TAB "Wolfram" TAB "50";
$EOTWCrafting["revolverItem"] = "Lead" TAB "15" TAB "Metal" TAB "100" TAB "Wolfram" TAB "300";
$EOTWCrafting["sniperItem"] = "Copper" TAB "25" TAB "Lead" TAB "25" TAB "Wolfram" TAB "400";
$EOTWCrafting["m60Item"] = "Lead" TAB "100" TAB "Copper" TAB "60" TAB "Metal" TAB "600";
$EOTWCrafting["grenadeLauncherItem"] = "Lead" TAB "50" TAB "Copper" TAB "30" TAB "Metal" TAB "100";
$EOTWCrafting["bfg10kItem"] = "Lead" TAB "100" TAB "Copper" TAB "30" TAB "Sturdium" TAB "50";
$EOTWCrafting["fragItem"] = "Wood" TAB "10";

$EOTWCrafting["ump45Item"] = "Copper" TAB "25" TAB "Metal" TAB "200" TAB "Wolfram" TAB "200";
$EOTWCrafting["tommyGunItem"] = "Lead" TAB "25" TAB "Metal" TAB "250" TAB "Wolfram" TAB "400";
$EOTWCrafting["leverRifleItem"] = "Copper" TAB "15" TAB "Lead" TAB "15" TAB "Wolfram" TAB "300";
$EOTWCrafting["flareGunItem"] = "Metal" TAB "50" TAB "Stone" TAB "375" TAB "Wolfram" TAB "50";
$EOTWCrafting["flamerItem"] = "25" TAB "Lead" TAB "25" TAB "Copper" TAB "60" TAB "Metal" TAB "Wolfram" TAB "300";
$EOTWCrafting["crossbowItem"] = "Metal" TAB "250" TAB "Wolfram" TAB "250";

$EOTWCrafting["VapeItem"] = "Vine" TAB "42";
$EOTWCrafting["VapeOrangeItem"] = "Vine" TAB "42";
$EOTWCrafting["VapeYellowItem"] = "Vine" TAB "42";
$EOTWCrafting["VapeGreenItem"] = "Vine" TAB "42";
$EOTWCrafting["VapeBlueItem"] = "Vine" TAB "42";
$EOTWCrafting["VapePurpleItem"] = "Vine" TAB "42";

$EOTWCrafting["VRPItem"] = "Sturdium" TAB "50";
$EOTWCrafting["rocketLauncherItem"] = "Sturdium" TAB "150";

$EOTWCrafting["EOTWArrowBundleItem"] = "Wood" TAB "500" TAB "Stone" TAB "75";
$EOTWCrafting["EOTWRecurveBowItem"] = "Wood" TAB "2000";
$EOTWCrafting["EOTWMetalRecurveBowItem"] = "Metal" TAB "100";

$EOTWCrafting["ammoBox9mmItemA"] = "Wolfram" TAB "30";
$EOTWCrafting["ammoBox556ItemA"] = "Wolfram" TAB "20";
$EOTWCrafting["ammoBoxRevolverItemA"] = "Lead" TAB "10" TAB "Wolfram" TAB "15";
$EOTWCrafting["ammoBox308ItemA"] = "Lead" TAB "15" TAB "Wolfram" TAB "10";
$EOTWCrafting["ammoBoxShotgunItemA"] = "Copper" TAB "10" TAB "Wolfram" TAB "20";
$EOTWCrafting["ammoBoxM60ItemA"] = "Lead" TAB "50" TAB "Copper" TAB "30" TAB "Wolfram" TAB "50";
$EOTWCrafting["ammo40mmGrenadesItem"] = "Lead" TAB "15" TAB "Copper" TAB "15" TAB "Wolfram" TAB "120";
$EOTWCrafting["ammo45ACPItem"] = "Metal" TAB "200" TAB "Wolfram" TAB "20";
$EOTWCrafting["ammoBoltItem"] = "Metal" TAB "100" TAB "Wood" TAB "250" TAB "Wolfram" TAB "10";
$EOTWCrafting["ammoFlamerFuelItem"] = "Vine" TAB "50" TAB "Grass" TAB "10" TAB "Wolfram" TAB "50";
$EOTWCrafting["ammoFlareItem"] = "Glass" TAB "10" TAB "Wolfram" TAB "10";

$EOTWCrafting["swordItem"] = "Stone" TAB "500";
$EOTWCrafting["baseballBatItem"] = "Wood" TAB "1500";
$EOTWCrafting["macheteItem"] = "Copper" TAB "30" TAB "Metal" TAB "100";
$EOTWCrafting["meatcleaverItem"] = "Copper" TAB "15" TAB "Metal" TAB "50";
$EOTWCrafting["pipeWrenchItem"] = "Metal" TAB "200";
$EOTWCrafting["sledgeHammerItem"] = "Lead" TAB "75" TAB "Wood" TAB "1500";
$EOTWCrafting["hockeystickItem"] = "Stone" TAB "500" TAB "Wood" TAB "1500";
$EOTWCrafting["shovelItem"] = "Metal" TAB "300" TAB "Wood" TAB "1500";
$EOTWCrafting["pitchforkItem"] = "Metal" TAB "300" TAB "Wood" TAB "1500";
$EOTWCrafting["spikeBatItem"] = "Metal" TAB "150" TAB "Wood" TAB "1500";
$EOTWCrafting["kurganItem"] = "Sturdium" TAB "50";

$EOTWCrafting["handDrillItem"] = "Copper" TAB "5" TAB "Wood" TAB "1000";
$EOTWCrafting["handDrill2Item"] = "Lead" TAB "25" TAB "Copper" TAB "15" TAB "Metal" TAB "200";
$EOTWCrafting["InvExpanderItem"] = "Copper" TAB "15" TAB "Wood" TAB "2000";

$EOTWCrafting["potionHealingItem"] = "Glass" TAB "2" TAB "Vine" TAB "15";
$EOTWCrafting["potionRegenItem"] = "Glass" TAB "2" TAB "Grass" TAB "10";
$EOTWCrafting["potionSpeedItem"] = "Glass" TAB "2" TAB "Vine" TAB "10" TAB "Grass" TAB "5";
$EOTWCrafting["potionPowerItem"] = "Glass" TAB "2" TAB "Wood" TAB "1000";
$EOTWCrafting["potionPickupItem"] = "Glass" TAB "2" TAB "Copper" TAB "5";
$EOTWCrafting["potionSunInvulItem"] = "Glass" TAB "2" TAB "Metal" TAB "100" TAB "Lead" TAB "50";
$EOTWCrafting["pillItem"] = "Vine" TAB "10";
$EOTWCrafting["shieldLIOOItem"] = "Lead" TAB "50" TAB "Copper" TAB "30" TAB "Metal" TAB "100";
$EOTWCrafting["shieldHIOOItem"] = "Sturdium" TAB "50";

//Craft score. The amount of  score you get for crafting the item.

$EOTWCraftPoints["ar15Item"] = 4;
$EOTWCraftPoints["cShotgunItem"] = 4;
$EOTWCraftPoints["dbShotgunItem"] = 2;
$EOTWCraftPoints["mac10Item"] = 3;
$EOTWCraftPoints["pistolItem"] = 2;
$EOTWCraftPoints["revolverItem"] = 4;
$EOTWCraftPoints["sniperItem"] = 5;
$EOTWCraftPoints["m60Item"] = 6;
$EOTWCraftPoints["grenadeLauncherItem"] = 7;

$EOTWCraftPoints["ump45Item"] = 4;
$EOTWCraftPoints["tommyGunItem"] = 5;
$EOTWCraftPoints["leverRifleItem"] = 4;
$EOTWCraftPoints["flareGunItem"] = 2;
$EOTWCraftPoints["flamerItem"] = 5;
$EOTWCraftPoints["crossbowItem"] = 3;

$EOTWCraftPoints["VapeItem"] = 2;
$EOTWCraftPoints["VapeOrangeItem"] = 2;
$EOTWCraftPoints["VapeYellowItem"] = 2;
$EOTWCraftPoints["VapeGreenItem"] = 2;
$EOTWCraftPoints["VapeBlueItem"] = 2;
$EOTWCraftPoints["VapePurpleItem"] = 2;

$EOTWCraftPoints["VRPItem"] = 5;
$EOTWCraftPoints["rocketLauncherItem"] = 10;
$EOTWCraftPoints["BFG10kItem"] = 10;

$EOTWCraftPoints["EOTWArrowBundleItem"] = 1;
$EOTWCraftPoints["EOTWRecurveBowItem"] = 2;
$EOTWCraftPoints["EOTWMetalRecurveBowItem"] = 2;

$EOTWCraftPoints["ammoBox9mmItemA"] = 1;
$EOTWCraftPoints["ammoBox556ItemA"] = 1;
$EOTWCraftPoints["ammoBoxRevolverItemA"] = 2;
$EOTWCraftPoints["ammoBox308ItemA"] = 3;
$EOTWCraftPoints["ammoBoxShotgunItemA"] = 1;
$EOTWCraftPoints["ammoBoxM60ItemA"] = 3;
$EOTWCraftPoints["ammo40mmGrenadesItem"] = 4;
$EOTWCraftPoints["ammo45ACPItem"] = 2;
$EOTWCraftPoints["ammoBoltItem"] = 1;
$EOTWCraftPoints["ammoFlamerFuelItem"] = 2;
$EOTWCraftPoints["ammoFlareItem"] = 1;

$EOTWCraftPoints["swordItem"] = 5;
$EOTWCraftPoints["baseballBatItem"] = 1;
$EOTWCraftPoints["macheteItem"] = 4;
$EOTWCraftPoints["meatcleaverItem"] = 3;
$EOTWCraftPoints["pipeWrenchItem"] = 2;
$EOTWCraftPoints["sledgeHammerItem"] = 4;
$EOTWCraftPoints["hockeystickItem"] = 2;
$EOTWCraftPoints["shovelItem"] = 3;
$EOTWCraftPoints["pitchforkItem"] = 3;
$EOTWCraftPoints["spikeBatItem"] = 3;
$EOTWCraftPoints["kurganItem"] = 10;

$EOTWCraftPoints["handDrillItem"] = 2;
$EOTWCraftPoints["handDrill2Item"] = 4;
$EOTWCraftPoints["InvExpanderItem"] = 5;

$EOTWCraftPoints["potionHealingItem"] = 2;
$EOTWCraftPoints["potionRegenItem"] = 2;
$EOTWCraftPoints["potionSpeedItem"] = 2;
$EOTWCraftPoints["potionPowerItem"] = 2;
$EOTWCraftPoints["potionPickupItem"] = 2;
$EOTWCraftPoints["potionSunInvulItem"] = 4;
$EOTWCraftPoints["pillItem"] = 1;
$EOTWCraftPoints["shieldLIOOItem"] = 5;
$EOTWCraftPoints["shieldHIOOItem"] = 10;

//Item discriptions. Only appears if the item must be crafted.
$EOTW::ItemDisc["Hand Drill I"] = "A tool that passively increases gathering speed by 25%. Does not stack with other Drills.";
$EOTW::ItemDisc["Hand Drill II"] = "A tool that passively increases gathering speed by 35%. Does not stack with other Drills.";

$EOTW::ItemDisc["Baseball Bat"] = "A relatively fast, weak, cheap melee weapon.";
$EOTW::ItemDisc["Frying Pan"] = "A metal pan that deals heavy damage upon hit.";
$EOTW::ItemDisc["Machete"] = "A lethargic blade that can deal massave damage.";

$EOTW::ItemDisc["Spike Bat"] = "A baseball bat with brutal nails.";
$EOTW::ItemDisc["Sledgehammer"] = "Super heavy, marks doom to whoever is it by it.";
$EOTW::ItemDisc["Shovel"] = "Used to dig into zombies, not dirt.";
$EOTW::ItemDisc["Pitchfork"] = "Long distance spear weapon.";
$EOTW::ItemDisc["Pipe Wrench"] = "Decent damage bludgeon tool.";
$EOTW::ItemDisc["Meat Cleaver"] = "Very short range, but deals damage at a high rate.";
$EOTW::ItemDisc["Kurgan Great Sword"] = "Once held by an immortal being, now crafted for your own hands.";
$EOTW::ItemDisc["Hockey Stick"] = "Suprisingly strong, and is moreso of a blunt machete.";

$EOTW::ItemDisc["VRP"] = "Virtual Richoet Projectile. Allows dashing with Jet Binding.";
$EOTW::ItemDisc["BFG10k"] = "Automatic Plasma Launcher. Blows up if overheated.";

$EOTW::ItemDisc["Vape"] = "Owned by only the top men in the vine growing industry.";
$EOTW::ItemDisc["Vape Red"] = "Today, I am on a mission to rip the FATTEST vape.";
$EOTW::ItemDisc["Vape Yellow"] = "So I am heading down to my favorate vape shop in NYC to VAPE up.";
$EOTW::ItemDisc["Vape Green"] = "Welcome to vape city, the home of the FATTEST vape rippers in the world.";
$EOTW::ItemDisc["Vape Blue"] = "Here, I am kicking it up a notch, and I will become one of the best. Lets check it out.";
$EOTW::ItemDisc["Vape Purple"] = "'Hey man what is going on here?' 'hey dude' 'I heard y'all had vapes here.";

$EOTW::ItemDisc["50-HP Shield"] = "A shield that protects you from enemies and the sun.";
$EOTW::ItemDisc["400-HP Shield"] = "A shield that protects you from enemies and the sun.";

$EOTW::ItemDisc["Pistol"] = "A simple, cheap firearm. Uses Small Ammo.";
$EOTW::ItemDisc["SMG"] = "The one and only bullet spammer. Uses Small Ammo.";
$EOTW::ItemDisc["Assault Rifle"] = "An automatic gun with decent damage and spread. Uses Medium Ammo.";
$EOTW::ItemDisc["Combat Shotgun"] = "A speedy shotgun with limited range. Uses Shotgun Ammo.";
$EOTW::ItemDisc["DB Shotgun"] = "Delivers a super-powerful blast at short range. Uses Shotgun Ammo.";
$EOTW::ItemDisc["Revolver"] = "A slow sidearm with an insti-kill headshot. Uses Magnum ammo.";
$EOTW::ItemDisc["Wasteland Sniper"] = "An accurate scope-only gun with an insti-kill headshot. Uses Sniper Ammo.";
$EOTW::ItemDisc["LMG"] = "A weapon to suprass (kinda) all other weapons. Uses Heavy Ammo.";
$EOTW::ItemDisc["Grenade Launcher"] = "Makes monsters go boom boom. Uses 40mm Grenades.";

$EOTW::ItemDisc["UMP"] = "A mix of a SMG and an Assault Rifle. Uses Small Ammo.";
$EOTW::ItemDisc["Tommy Gun"] = "The LMG but for classy people. Uses 45 ACP Ammo.";
$EOTW::ItemDisc["Lever Rifle"] = "The faster variant of the sniper. Uses Magnum Ammo.";
$EOTW::ItemDisc["Flare Gun"] = "Weak, but useful for sending of signals. Uses Flare Ammo.";
$EOTW::ItemDisc["Flamer"] = "Who says you can't fight fire with fire? Uses Flamer Ammo.";
$EOTW::ItemDisc["Crossbow"] = "Who says you can't fight fire with fire? Uses Bolts Ammo.";

$EOTW::ItemDisc["Wood R. Bow"] = "A wood bow that becomes accurate if charged. Charge by holding MOUSEFIRE.";
$EOTW::ItemDisc["Metal R. Bow"] = "Slower than its wooden version, but has a flaming overcharge ability.";
$EOTW::ItemDisc["Arrows"] = "A bundle of 5 arrows for the Wood R. Bow and the Metal R. Bow.";

$EOTW::ItemDisc["Inventory Expander"] = "Permanently adds one item slot. Stacks up to 11 total item slots.";

$EOTW::ItemDisc["Potion (Healing)"] = "A potion that heals 100 HP instantly.";
$EOTW::ItemDisc["Potion (Regen)"] = "A potion that heals 5 HP per second over a span of 30 seconds.";
$EOTW::ItemDisc["Potion (Speed)"] = "A potion that triples max walk speed for 10 seconds.";
$EOTW::ItemDisc["Potion (Power)"] = "A potion that doubles melee damage and halves damage taken for 45 seconds.";
$EOTW::ItemDisc["Potion (Pickup)"] = "Decreases gather time by 25% for 3 minutes. Can be used with other powerups.";
$EOTW::ItemDisc["Potion (SunInvul)"] = "Renders you immune to the sun's rays for one minute. Can be used with other powerups.";

//Item blueprint. If the item needs a blue print in order to be crafted. Number is day when blueprint will spawn.

$EOTW::BlueprintItem["SMG"] = 18;
$EOTW::BlueprintItem["Assault Rifle"] = 15;
$EOTW::BlueprintItem["Combat Shotgun"] = 24;
$EOTW::BlueprintItem["Dual Barrel Shotgun"] = 21;
$EOTW::BlueprintItem["Revolver"] = 27;
$EOTW::BlueprintItem["Rifle"] = 30;
$EOTW::BlueprintItem["LMG"] = 35;

$EOTW::BlueprintItem["50-HP Shield"] = 20;
$EOTW::BlueprintItem["Machete"] = 12;

//Item Tiers. Put behind the name of each item. Also used for blueprints.

$EOTW::ItemTierCount = 0;
//$EOTW::ItemTier["50-HP Shield"] = "Tool";
$EOTW::ItemTier["Skis"] = "Tool"; $EOTW::BlueprintCount++; 
//$EOTW::ItemTier["Ball Basketball"] = "Misc";
//$EOTW::ItemTier["Ball Football"] = "Misc";
//$EOTW::ItemTier["Ball Soccer Ball"] = "Misc";
//$EOTW::ItemTier["Ball Dodgeball"] = "Misc";
$EOTW::ItemTier["Torch"] = "Tool"; $EOTW::BlueprintCount++; 
$EOTW::ItemTier["Vape"] = "Misc"; $EOTW::BlueprintCount++; 
$EOTW::ItemTier["Vape Red"] = "Misc"; $EOTW::BlueprintCount++; 
$EOTW::ItemTier["Vape Orange"] = "Misc"; $EOTW::BlueprintCount++; 
$EOTW::ItemTier["Vape Yellow"] = "Misc"; $EOTW::BlueprintCount++; 
$EOTW::ItemTier["Vape Green"] = "Misc"; $EOTW::BlueprintCount++; 
$EOTW::ItemTier["Vape Blue"] = "Misc"; $EOTW::BlueprintCount++; 
$EOTW::ItemTier["Vape Purple"] = "Misc"; $EOTW::BlueprintCount++; 
$EOTW::ItemTier["Flare Gun"] = "I";
$EOTW::ItemTier["Wood R. Bow"] = "I";
$EOTW::ItemTier["Push Broom"] = "I";
$EOTW::ItemTier["Pistol"] = "I";
$EOTW::ItemTier["Crossbow"] = "II"; $EOTW::BlueprintCount++; 
$EOTW::ItemTier["DB Shotgun"] = "II"; $EOTW::BlueprintCount++; 
$EOTW::ItemTier["Metal R. Bow"] = "II"; $EOTW::BlueprintCount++; 
$EOTW::ItemTier["SMG"] = "II"; $EOTW::BlueprintCount++; 
$EOTW::ItemTier["UMP"] = "II"; $EOTW::BlueprintCount++; 
$EOTW::ItemTier["Revolver"] = "III"; $EOTW::BlueprintCount++; 
$EOTW::ItemTier["Lever Rifle"] = "III"; $EOTW::BlueprintCount++; 
$EOTW::ItemTier["Combat Shotgun"] = "III"; $EOTW::BlueprintCount++; 
$EOTW::ItemTier["Assault Rifle"] = "III"; $EOTW::BlueprintCount++; 
$EOTW::ItemTier["Tommy Gun"] = "III"; $EOTW::BlueprintCount++; 
$EOTW::ItemTier["Wasteland Sniper"] = "IV"; $EOTW::BlueprintCount++; 
$EOTW::ItemTier["LMG"] = "IV"; $EOTW::BlueprintCount++; 
$EOTW::ItemTier["Grenade Launcher"] = "IV"; $EOTW::BlueprintCount++; 
$EOTW::ItemTier["Flamer"] = "IV"; $EOTW::BlueprintCount++; 
$EOTW::ItemTier["Kurgan Great Sword"] = "S"; $EOTW::BlueprintCount++; 
$EOTW::ItemTier["VRP"] = "S"; $EOTW::BlueprintCount++; 
$EOTW::ItemTier["Rocket L."] = "S"; $EOTW::BlueprintCount++; 
//$EOTW::ItemTier["400-HP Shield"] = "S";
$EOTW::ItemTier["BFG10k"] = "S"; $EOTW::BlueprintCount++; 
$EOTW::ItemTier["Arrows"] = "Ammo";
$EOTW::ItemTier["Ammo Small"] = "Ammo"; //$EOTW::ItemTier["Ammo 9mm"] = "Ammo";
$EOTW::ItemTier["Ammo Shotgun"] = "Ammo";
$EOTW::ItemTier["Ammo Magnum"] = "Ammo"; //$EOTW::ItemTier["Ammo .357"] = "Ammo";
$EOTW::ItemTier["Ammo Medium"] = "Ammo"; //$EOTW::ItemTier["Ammo 5.56"] = "Ammo";
$EOTW::ItemTier["Ammo Sniper"] = "Ammo";
$EOTW::ItemTier["Ammo Heavy"] = "Ammo";
$EOTW::ItemTier["Ammo 40mm Grenades"] = "Ammo";
$EOTW::ItemTier["Ammo 45 ACP Drum"] = "Ammo";
$EOTW::ItemTier["Ammo Bolts"] = "Ammo";
$EOTW::ItemTier["Ammo Flamer Fuel"] = "Ammo";
$EOTW::ItemTier["Ammo Flares"] = "Ammo";
$EOTW::ItemTier["Baseball Bat"] = "Melee"; $EOTW::BlueprintCount++; 
$EOTW::ItemTier["Spike Bat"] = "Melee"; $EOTW::BlueprintCount++; 
$EOTW::ItemTier["Machete"] = "Melee"; $EOTW::BlueprintCount++; 
$EOTW::ItemTier["Frying Pan"] = "Melee"; $EOTW::BlueprintCount++; 
$EOTW::ItemTier["Pipe Wrench"] = "Melee"; $EOTW::BlueprintCount++; 
$EOTW::ItemTier["Meat Cleaver"] = "Melee"; $EOTW::BlueprintCount++; 
$EOTW::ItemTier["Sledgehammer"] = "Melee"; $EOTW::BlueprintCount++; 
$EOTW::ItemTier["Hockey Stick"] = "Melee"; $EOTW::BlueprintCount++; 
$EOTW::ItemTier["Shovel"] = "Melee"; $EOTW::BlueprintCount++; 
$EOTW::ItemTier["Pitchfork"] = "Melee"; $EOTW::BlueprintCount++; 
$EOTW::ItemTier["Hand Drill I"] = "Tool"; $EOTW::BlueprintCount++; 
$EOTW::ItemTier["Hand Drill II"] = "Tool"; $EOTW::BlueprintCount++; 
$EOTW::ItemTier["Inventory Expander"] = "Tool"; $EOTW::BlueprintCount++; 
$EOTW::ItemTier["Boombox"] = "Tool"; $EOTW::BlueprintCount++; 
$EOTW::ItemTier["Potion (Healing)"] = "Misc"; $EOTW::BlueprintCount++; 
$EOTW::ItemTier["Potion (Regen)"] = "Misc"; $EOTW::BlueprintCount++; 
$EOTW::ItemTier["Potion (Speed)"] = "Misc"; $EOTW::BlueprintCount++; 
$EOTW::ItemTier["Potion (Power)"] = "Misc"; $EOTW::BlueprintCount++; 
$EOTW::ItemTier["Potion (Pickup)"] = "Misc"; $EOTW::BlueprintCount++; 
$EOTW::ItemTier["Potion (SunInvul)"] = "Misc"; $EOTW::BlueprintCount++; 
$EOTW::ItemTier["Heart Vessel"] = "U";
$EOTW::ItemTier["Blueprint"] = "U";
$EOTW::ItemTier["Bow"] = "U";
$EOTW::ItemTier["D - Fireball"] = "U";
$EOTW::ItemTier["Magic Spellbook"] = "U";
$EOTW::ItemTier["AK47"] = "U";

//Setup Swollow's Ammo System.
$Pref::SwolAmmo_SpawnAmmo["9mm"] = 0;
$Pref::SwolAmmo_SpawnAmmo["357"] = 0;
$Pref::SwolAmmo_SpawnAmmo["556"] = 0;
$Pref::SwolAmmo_SpawnAmmo["308"] = 0;
$Pref::SwolAmmo_SpawnAmmo["shotgun_shells"] = 8*0;
$Pref::SwolAmmo_SpawnAmmo["heavy"] = 0;

//Item blacklist. Use uiName of item to blacklist it.

$EOTWBacklistedItem[-1 + $EOTWBacklistedItems++] = "Duplicator";
//$EOTWBacklistedItem[-1 + $EOTWBacklistedItems++] = "Bow";
$EOTWBacklistedItem[-1 + $EOTWBacklistedItems++] = "Gun";
$EOTWBacklistedItem[-1 + $EOTWBacklistedItems++] = "Guns Akimbo";
$EOTWBacklistedItem[-1 + $EOTWBacklistedItems++] = "Horse Ray";
//$EOTWBacklistedItem[-1 + $EOTWBacklistedItems++] = "Rocket L.";
$EOTWBacklistedItem[-1 + $EOTWBacklistedItems++] = "Spear";
$EOTWBacklistedItem[-1 + $EOTWBacklistedItems++] = "Sword";
//$EOTWBacklistedItem[-1 + $EOTWBacklistedItems++] = "LMG";
//$EOTWBacklistedItem[-1 + $EOTWBacklistedItems++] = "D - Fireball";
$EOTWBacklistedItem[-1 + $EOTWBacklistedItems++] = "D - Firebarrage";
$EOTWBacklistedItem[-1 + $EOTWBacklistedItems++] = "D - Firebreath";
//$EOTWBacklistedItem[-1 + $EOTWBacklistedItems++] = "Ammo Heavy";
//$EOTWBacklistedItem[-1 + $EOTWBacklistedItems++] = "Heart Vessel";
$EOTWBacklistedItem[-1 + $EOTWBacklistedItems++] = "test";
$EOTWBacklistedItem[-1 + $EOTWBacklistedItems++] = "Test II";
$EOTWBacklistedItem[-1 + $EOTWBacklistedItems++] = "C4";
$EOTWBacklistedItem[-1 + $EOTWBacklistedItems++] = "Molotov";
$EOTWBacklistedItem[-1 + $EOTWBacklistedItems++] = "Syringe ";
$EOTWBacklistedItem[-1 + $EOTWBacklistedItems++] = "Frag Grenade";

brickVehicleSpawnData.restrictMat = "Metal";
brickDoorGlassOpenCWData.EOTWCost = 72;
brickDoorGlassOpenCCWData.EOTWCost = 72;
brickDoorHouseOpenCWData.EOTWCost = 72;
brickDoorHouseOpenCCWData.EOTWCost = 72;
brickDoorHouseWindowOpenCWData.EOTWCost = 72;
brickDoorHouseWindowOpenCCWData.EOTWCost = 72;
brickDoorJailOpenCWData.EOTWCost = 72;
brickDoorJailOpenCCWData.EOTWCost = 72;
brickDoorPlainOpenCWData.EOTWCost = 72;
brickDoorPlainOpenCCWData.EOTWCost = 72;
//arrowProjectile.directDamage *= 0.5;
//gunProjectile.directDamage *= 0.5;

//TODO move this
fistItem.canDrop = false;

function updateItemNames()
{
	%cnt = datablockGroup.getCount();
	for(%i=0;%i<%cnt;%i++)
	{
		%obj = datablockGroup.getObject(%i);
		if(%obj.getClassName() $= "ItemData")
		{
			for(%j=0;%j<$EOTWBacklistedItems;%j++)
			{
				//echo(%obj.uiName SPC $EOTWBacklistedItem[%j]);
				
				if (%obj.uiName $= $EOTWBacklistedItem[%j])
				{
					%obj.uiName = "";
					break;
				}
			}
			
			if (getField($EOTW::ItemTier[%obj.uiName],0) !$= "") 
			{
				%obj.uiName = "[" @ getField($EOTW::ItemTier[%obj.uiName],0) @ "]" SPC %obj.uiName;
				%obj.needsBlueprint = true;
			}
		}
	}
	
	createuinametable();
	transmitdatablocks();
	commandtoall('missionstartphase3');
}

function getSturdiumName()
{
	%name[-1 + %names++] = "Sturdium"; //We should only use "sturdium" for flunecy.
	%name[-1 + %names++] = "Adminium";
	%name[-1 + %names++] = "Eternium";
	return %name[getRandom(0, %names - 1)];
}
if($SturdiumName $= "") $SturdiumName = getSturdiumName();

if($Pref::EOTW::AllowWrench $= "")
	$Pref::EOTW::AllowWrench = 1;

datablock AudioProfile(EOTW_CritSound)
{
   filename    = "./sounds/crit_hit.wav";
   description = AudioClose3d; 
   preload = true;
};

package EOTW
{
	function GameConnection::autoAdminCheck(%client)
	{	
		//schedule(100,%client,%client.chatMessage,"\c6Notable Updates:");
		//schedule(100,%client,%client.chatMessage,"\c5 12/8/17 - Machines placed by different players can now interact with");
		//schedule(100,%client,%client.chatMessage,"\c5 eachother if the two players have atleast build trust.");
		
		if ($EOTW::IsDay) %timeText = "Day";
		else %timeText = "Night";
		
		commandToClient(%client, 'MessageBoxOK', "Solar Apocalypse 3", "<just:left>During the night, you try to gather materials.<br>During the day, you hide from the sun.<br><br>Place checkpoints in your home to respawn in the shade.<br><br>To use machinery: build a power supplier, battery, and the machine.");
		
		schedule(500,%client,%client.chatMessage,"\c3 It is currently \c6: " @ $LastEOTWTime @ " \c3on\c6 " @ %timeText SPC $EOTW::Day @ ".");
		
		loadInvData(%client);
		
		return Parent::autoAdminCheck(%client);
	}
	
	function fxDtsBrick::setColor(%brick, %color)
	{ // && !%brick.getDatablock().uncolorable
		if(!%brick.unrecolorable && (getWord(getColorIDTable(%color), 3) > 0.95 || (%brick.material $= "Glass" || %brick.material $= "Plastic" || %brick.material $= ""))) 
			Parent::setColor(%brick, %color);
	}
	
	function servercmdEnvGui_SetVar(%cl, %var, %arg)
	{
		if(!%cl.environMaster)
			if(!$EnvGuiTesting)
			{
				%cl.centerPrint("<color:FF8000>Only the Environment Master bot can edit these settings.", 15);
				return;
			}
			else
				echo(%var SPC %arg);
		Parent::servercmdEnvGui_SetVar(%cl, %var, %arg);
	}
	
	function servercmdPlantBrick(%cl)
	{
		if(isEventPending($EnvMasterLoop) && !%cl.builderMode)
		{
			if(isObject(%pl = %cl.player) && isObject(%temp = %pl.tempbrick))
			{
				%data = %temp.getDatablock();
				%mat = %cl.material;
				if(firstWord(%data.uiName) $= "Coffin") return;
				if (%data.EOTWCost !$= "")
					%volume = %data.EOTWCost;
				else
					%volume = %data.brickSizeX * %data.brickSizeY * %data.brickSizeZ;
				//if(%data.uiName $= "Global Storage") %volume = 0;
				if (%data.restrictMat !$= "" && %mat !$= %data.restrictMat)
				{
					if (%data.restrictMat $= "Sturdium") %cl.centerPrint("\c0Whoops!<br>\c6You need a special material to place this block.", 3);
					else %cl.centerPrint("\c0Whoops!<br>\c6You need " @ %data.restrictMat @ " to place this block.", 3);
				}
				else if((%mat $= "Dead_Plant" || %mat $= "Grass" || %mat $= "Vine") && %data.getName() !$= "brick1x1FData" && %data.getName() !$= "brickEOTWOreGrowerData")
					%cl.centerPrint("\c0Whoops!<br>\c6Plants must be a 1x1F brick or an Ore Grower.", 3);
				else if (false)
					%cl.centerPrint("\c0Whoops!<br>\c6You can't build with" SPC %mat @ ". Use it for crafting items.", 3);
				else if(strPos(%data.uiName,"Hole") > -1)
					%cl.centerPrint("\c0Whoops!<br>\c6You can't place bot holes.", 3);
				else if(!$EOTW::Freebuild && $EOTW::Material[%cl.bl_id, %mat] < %volume)
					%cl.centerPrint("\c0Whoops!<br>\c6You don't have enough " @ %cl.material @ " to place that brick!<br>\c6You need" SPC (%volume - $EOTW::Material[%cl.bl_id, %mat]) SPC "more.", 3);
				else
				{
					%brick = Parent::servercmdPlantBrick(%cl); if(!isObject(%brick)) return %brick;
					if (!$EOTW::Freebuild) $EOTW::Material[%cl.bl_id, %mat] = $EOTW::Material[%cl.bl_id, %mat] - %volume;
					%brick.material = %mat; %brick.setColor($EOTW::Colors["::"@(%mat$="Dead_Plant"?"Dead":%mat)]);
					switch$(%mat)
					{
						case "Dead_Plant":
							%brick.canBeFireballed = 1;
							%brick.isBreakable = 1;
							%brick.isFlammable = 1;
							%brick.isIgnitable = 1;
							%brick.unrecolorable = 1;
							%brick.setColliding(0);
							%brick.burnLoop(getSimTime());
							%cl.centerPrint("<color:FFFFFF>Well done, Sally Mc Smarty Pants. You found a way to build Dead Plant.", 3);
						case "Glass":
							%brick.canBeFireballed = 1;
							%brick.isBreakable = 1;
							%brick.smashable = 1;
						case "Grass":
							%brick.canBeFireballed = 1;
							%brick.isBreakable = 1;
							%brick.isFlammable = 1;
							%brick.unrecolorable = 1;
							%brick.setColliding(0);
							if (%data.getName() $= "brick1x1FData")
							{
								//%brick.schedule(48, "EOTW_GrowGrass", 0, 0, 0, getSimTime());
								%brick.last = getSimTime();
								PlantGroup.add(%brick);
							}							
						case "Metal":
							%brick.isBreakable = 1;
						case "Stone":
							%brick.canBeFireballed = 1;
							%brick.isBreakable = 1;
						case "Vine":
							%brick.canBeFireballed = 1;
							%brick.isBreakable = 1;
							%brick.isFlammable = 1;
							%brick.unrecolorable = 1;
							%brick.setColliding(0);
							if (%data.getName() $= "brick1x1FData")
							{
								//%brick.schedule(48, "EOTW_GrowVine", 0, 0, 0, getSimTime());
								%brick.last = getSimTime();
								PlantGroup.add(%brick);
							}							
						case "Wood":
							%brick.canBeFireballed = 1;
							%brick.isBreakable = 1;
							%brick.isFlammable = 1;
							%brick.isIgnitable = 1;
						case "Copper":
							%brick.isBreakable = 1;
						case "Lead":
							%brick.isBreakable = 1;
					}
					%cl.showMaterials();
					
					if (%data.getName() $= "brickEOTWOreGrowerData" && !%cl.warnOreGrower)
					{
						%cl.chatMessage("\c4NOTE\c5: To pickup ores generated by the ore grower, you must use the Extraction Wand item on it.");
						%cl.warnOreGrower = true;
					}
				}
			}
		}
		else Parent::servercmdPlantBrick(%cl);
	}
	
	function servercmdSetWrenchData(%cl, %data)
	{
		//echo(%data);
		if(isEventPending($EnvMasterLoop))
		{
			if (getWord(getField(%data, 4), 0) !$= "IDB")
			{
				Parent::servercmdSetWrenchData(%cl, %data);
				return;
			}
			%item = getWord(getField(%data, 4), 1);
			
			if (!isObject(%item)) Parent::servercmdSetWrenchData(%cl, %data);
			
			for(%i=0; %item != 0 && %i<$EOTWBacklistedItems;%i++)
			{
				// if ($EOTW::Day < 40 && strPos($EOTWCrafting[%item.getName()],"Sturdium") > -1)
				// {
					// %data = setField(%data, 4, "IDB 0");
					// %cl.chatMessage("<color:FFFFFF>The item you selected can not be spawned... Yet.");
					// %blacklisted = true;
					// break;
				// }
				if(%item.uiName $= $EOTWBacklistedItem[%i])
				{
					%data = setField(%data, 4, "IDB 0");
					%cl.chatMessage("<color:FFFFFF>The item you selected can not be spawned.");
					%blacklisted = true;
					break;
				}
			}
			%name = getSubStr(%item.uiName,striPos(%item.uiName,"]") + 2,100);
			if (!%blacklisted && getField($EOTW::ItemTier[%name],0) !$= "" && getField($EOTW::ItemTier[%name],0) !$= "I" && getField($EOTW::ItemTier[%name],0) !$= "Ammo" && !$EOTW::BluePrintInv[%cl.bl_id,%name])
			{
				%data = setField(%data, 4, "IDB 0");
				
				if (getField($EOTW::ItemTier[%name],0) $= "U")
				{
					%cl.chatMessage("<color:FFFFFF>This unique item can not be spawned on a brick. Try finding it another way!");
				}
				else
				{
					%cl.chatMessage("<color:00FFFF>You do not have the blueprint for this item.");
					if (!%cl.warnAboutBluePrint)
					{
						%cl.chatMessage("<color:00FFFF>Blueprints can sometimes be found in chests or monsters.");
						%cl.chatMessage("<color:00FFFF>Any item that is Tier I, Ammo, or has no type does not need a blueprint.");
						%cl.warnAboutBluePrint = true;
					}
				}
			}
		}
		Parent::servercmdSetWrenchData(%cl, %data);
	}
	
	function fxDtsBrick::onRemove(%brick)
	{
		if(!%brick.dontRefund)
		{
			%data = %brick.getDatablock();
			if (%data.EOTWCost !$= "")
			{
				$EOTW::Material[%brick.getGroup().bl_id, %brick.material] += %data.EOTWCost;
			}
			else
				$EOTW::Material[%brick.getGroup().bl_id, %brick.material] += %data.brickSizeX * %data.brickSizeY * %data.brickSizeZ;
		}
		
		Parent::onRemove(%brick);
	}
	
	function fxDtsBrick::onDeath(%brick)
	{
		// if(%brick.fromOreGrower)
		// {
			// %amt = 1;
			// if (%brick.material $= "Glass") %amt = 1;
			// else if (%brick.material $= "Metal") %amt = 2;
			// else if (%brick.material $= "Stone") %amt = 3;
			// else if (%brick.material $= "Wood") %amt = 10;
			// else if (%brick.material $= "Wolfram") %amt = 2;
			// else if (%brick.material $= "Copper") %amt = 1;
			// else if (%brick.material $= "Lead") %amt = 1;
			// else if (%brick.material $= "Sturdium") %amt = 1;
			
			// //EOTW_SpawnOreDrop(%brick.material,%amt,vectorAdd(%brick.getPosition(),"0.0 0.0 0.1"));
			// schedule(getRandom(100,300), 0, "EOTW_SpawnOreDrop", %brick.material, %amt, vectorAdd(%brick.getPosition(),"0.0 0.0 0.1"));
		// }		
		
		Parent::onDeath(%brick);
	}
	
	function GameConnection::createPlayer(%cl, %trans)
	{
		if($EOTW::Hardcore && $EOTW::HasDied[%cl.bl_id])
		{
			if(isObject(%cl.getControlObject()))
				%cl.chatMessage("\c6You have died. Please wait for the next round to try again.");
			else { (%cam = %cl.camera).setTransform(%trans); %cl.setControlObject(%cam); }
			return;
		}
		else if($EOTW::PermaDeath && $EOTW::Day >= 10 && $EOTW::IsDay && %cl.brickgroup.spawnBrickCount == 0)
		{
			if(isObject(%cl.getControlObject()))
			{
				if (!%cl.warnAboutDeath)
				{
					%cl.chatMessage("\c6You can't spawn during the day; you could end up dying instantly. Please be patient :)");
					%cl.warnAboutDeath = true;
				}
			}
			else { (%cam = %cl.camera).setTransform(%trans); %cl.setControlObject(%cam); }
			return;
		}
		
		Parent::createPlayer(%cl, %trans);
		
		if (%cl.selectedLight $= "")
			%cl.selectedLight = BrightAmbientLight;
			
		%cl.warnAboutDeath = false;
			
		if(isEventPending($EnvMasterLoop) && isObject(%pl = %cl.player))
		{
			if ($EOTW::Day >= 10)
			{
				%pl.immuneToSun = true;
				%pl.immuneToBurn = true;
				%pl.immuneToFire = true;
				%pl.schedule(15000,"RemoveInvul");
				%cl.centerPrint("\c6You are now invulnerable to sun for 15 seconds.");
			
			}
			
			%pl.EOTW_Arrows += 5;
			
			%pl.pickupSpeed = 1;
			%pl.damageMult = 1;			
			//%pl.setShapeNameDistance(0);
			
			if ($EOTW::PlayerInvSize[%cl.bl_id] == 0)
				$EOTW::PlayerInvSize[%cl.bl_id] = 5;

			%pl.changeDatablock("Player" @ $EOTW::PlayerInvSize[%cl.bl_id] @ "SlotPlayer");

			if ($EOTW::PlayerMaxHealth[%cl.bl_id] == 0)
				$EOTW::PlayerMaxHealth[%cl.bl_id] = 100;

			%pl.setMaxHealth($EOTW::PlayerMaxHealth[%cl.bl_id]);
			%pl.clearTools();

			if(!isObject(%pl.client.checkpointBrick))
				%pl.setTransform(EOTW_getEnemyRaycastSpawnLocation()); //getRandom(64,768) SPC getRandom(64,768) SPC 128);
			
			
			//Unconditional items
			%tool[-1+%tools++] = "0 0 HammerItem";

			if(%cl.isSuperAdmin || $Pref::EOTW::AllowWrench)
				%tool[-1+%tools++] = "0 0 WrenchItem";

			//%tool[-1+%tools++] = "0 0 PrintGun";
			%tool[-1+%tools++] = "0 0 fistItem";

			//Spawn one of the following at respective day:
			%tool[-1+%tools++] = "45 1 vrpItem";
			%tool[-1+%tools++] = "35 1 macheteItem";
			%tool[-1+%tools++] = "25 1 fryingpanItem";
			%tool[-1+%tools++] = "15 1 baseballBatItem";

			%found_only_one = 0;
			for(%i = 0; %i < %tools; %i++)
			{
				%day = getWord(%tool[%i], 0);
				%only_one = getWord(%tool[%i], 1);
				%item = getWord(%tool[%i], 2);

				if($EOTW::Day <= %day)
					continue;

				if(%only_one)
				{
					if(%found_only_one)
						continue;

					%found_only_one = 1;
				}

				talk("Give player: "@ %item);

				if(!isObject(%item))
					continue;

				%item = %item.getID();
				%pl.EOTWaddItem(%item);
			}
		}
	}
	
	function Player::RemoveInvul(%pl)
	{
		%pl.immunetosun = false;
		%pl.immuneToBurn = false;
		%pl.immuneToFire = false;
		%pl.client.centerPrint("\c6Your sun invulnerability wore off.",3);
	}

	function GameConnection::GetBlueprintCount(%client)
	{
		for (%i=0;%i<ItemDataGroup.getCount();%i++)
		{
			%item = ItemDataGroup.getObject(%i);
			%name = getSubStr(%item.uiName,striPos(%item.uiName,"]") + 2,100);
			%type2 = $EOTW::ItemTier[%name];
			if (%item.getClassName() $= "ItemData" && $EOTW::BluePrintInv[%client.bl_id,%name] && %type2 !$= "")
			{	
				%blueprints++;
			}
		}
		
		return %blueprints;
	}

	//TODO: I recall an issue where not all deaths show up...
	function GameConnection::onDeath(%cl, %a, %b, %damageType, %d)
	{
		Parent::onDeath(%cl, %a, %b, %damageType, %d);
		if(%damageType == $DamageType::BurnedToDeath)
			messageClient(%cl, '', "\c0" @ %cl.name @ " burned to death!");
		else if(%damageType == $DamageType::EOTWFireball)
			messageClient(%cl, '', "\c0" @ %cl.name @ " was hit by a meteor!");
		else if(%damageType == $DamageType::Sun)
			messageClient(%cl, '', "\c0" @ %cl.name @ " got fried by the Sun!");
		else if(%damageType == $DamageType::Drowning)
			messageClient(%cl, '', "\c0" @ %cl.name @ " drowned!");
		else
			messageClient(%cl, '', "\c0" @ %cl.name @ " died of unknown causes! ("@ %damageType @")");
		$EOTW::HasDied[%cl.bl_id] = 1;
		
		//echo("Damage Type:" @ %damageType);
	}
	
	function servercmdCreateMinigame(%cl, %a, %b, %c, %d, %e, %f, %g, %h, %i, %j)
	{ if(!isEventPending($EnvMasterLoop) || %cl.isSuperAdmin) Parent::servercmdCreateMinigame(%cl, %a, %b, %c, %d, %e, %f, %g, %h, %i, %j); }

	function Armor::damage(%this, %obj, %sourceObj, %position, %damage, %damageType)
	{
		//if ((isObject(%sourceObj.client.player) && %sourceObj.client.player.getClassName() $= "Player") && %obj.getClassName() $= "Player" && (!%sourceObj.client.pvp || !%obj.client.pvp)) return;
		if (isObject(%sourceObj) && %sourceObj.getClassName() $= "Player" && %sourceObj.currentPowerup $= "Power")
		{
			%damage *= %sourceObj.damageMult;
			%obj.playAudio(1,EOTW_CritSound);
		}
		
		if (isObject(%sourceObj.sourceObject) && %sourceObj.sourceObject.getClassName() $= "AIPlayer" && %sourceObj.getClassName() $= "Projectile")
			%damage *= 0.5;
		
		if (%obj.currentPowerup $= "Power")
			%damage *= 0.5;
		
		%objName = %obj.getClassName();
		parent::damage(%this, %obj, %sourceObj, %position, %damage, %damageType);
		
		if (isObject(%obj) && %obj.getClassName() $= "Player" && %obj.getDamagePercent() < 1.0)
			EOTW_UpdatePlayerStats(%obj.client);
		
		if (isObject(%sourceObj) && %objName $= "AIPlayer" && %obj.getDatablock().getName() $= "BehemothHoleBot")
		{
			if (%obj.getState() !$= "Dead")
				$EOTW::BossText = "<br>BEHEMOTH:" SPC ((100 - mFloor(%obj.getDamagePercent() * 100)) @ "%");
			else
				$EOTW::BossText = "";
		}
		if (isObject(%sourceObj) && %objName $= "AIPlayer" && %obj.getDatablock().getName() $= "SturdiumBeastHoleBot")
		{
			if (%obj.getState() !$= "Dead")
				$EOTW::BossText = "<br>THE BEAST:" SPC ((100 - mFloor(%obj.getDamagePercent() * 100)) @ "%");
			else
			{
				if ($EOTW::SpawnSturdium < 6)
				{
					$EOTW::SpawnSturdium++;
					messageAll('',"\c3The world has been blessed with a special material! \c7(LVL " @ $EOTW::SpawnSturdium @ "/6)");
					$EOTW::BossText = "";
				}
			
			}
		}
		
		//echo(%objName SPC %sourceObj.getClassName() SPC %obj.getState());=
		
		if (isObject(%sourceObj) && %objName $= "AIPlayer" && (%sourceObj.getClassName() $= "Player" || %sourceObj.getClassName() $= "GameConnection" || %sourceObj.getClassName() $= "Projectile") && %obj.getState() $= "Dead")
		{
			if (%sourceObj.getClassName() !$= "GameConnection")
				%client = %sourceObj.client;
			else
				%client = %sourceObj;
			
			if (isObject(%client))
			{
				%client.AwardAchievement("First Blood");
				%client.AddAchievementProgress("Hunter",1);
				%client.AddAchievementProgress("Cleansing",1);
				%client.AddAchievementProgress("Thunderstorm",1);
				%client.AddAchievementProgress("Doomguy",1);
					
				%db = %obj.getDatablock();
				
				if (%db.killAchievement !$= "")
					%client.AddAchievementProgress(%db.killAchievement,1);
				
				if (%db.wolframAmount > 0)
				{
					if(getRandom(0,3) == 0)
					{
						%wolframAmount = getRandom(1,%db.wolframAmount / 3);
						%wolframMessage = "\c6+" @ %wolframAmount @ " Wolfram | ";
						giveMaterial(%client,"Wolfram",%wolframAmount); //$EOTW::Material[%client.bl_id,"Wolfram"] += 100;
					}
					if (getRandom(1,1000) <= %db.materialChance)
					{
						%amt = (getRandom(getWord(%db.materialAmount,0),getWord(%db.materialAmount,1)) * 3);
						%matDropMessage = "\c6+" @ %amt SPC %db.materialDrop;
						//%client.chatMessage("\c4+" @ %amt SPC %db.materialDrop @ "!");
						giveMaterial(%client,%db.materialDrop,%amt); //$EOTW::Material[%client.bl_id,%db.materialDrop] += %amt;
					}
					
					if (%wolframMessage !$= "" || %matDropMessage !$= "")
						%client.chatMessage(%wolframMessage @ %matDropMessage);
				}
				if (%db.dropList !$= "")
				{
					for (%i = 0; getRecord(%db.dropList,%i) !$= ""; %i++)
					{
						%dropTable = getRecord(%db.dropList,%i);
						%item = getField(%dropTable,0);
						%minDrop = getWord(getField(%dropTable,1),0) + 0;
						%maxDrop = getWord(getField(%dropTable,1),1) + 0;
						%chance = getField(%dropTable,2);
						
						if (getRandom(0,1000) < %chance)
						{
							%totalDrop = (%minDrop > %maxDrop) ? %minDrop : getRandom(%minDrop,%maxDrop);
							while (%totalDrop > 0)
							{
								%itemDrop = getRandom(1,%totalDrop);
								%totalDrop -= %itemDrop;
								
								%worldItem = new Item()
								{
									datablock = EOTW_ItemDrop;
									static    = "0";
									position  = vectorAdd(%position,"0 0 1");
									rotation = EulerToAxis(getRandom(0,359) SPC getRandom(0,359) SPC getRandom(0,359)); //Todo: Get this to work.
									craftedItem = true;
									
									item = %item;
									itemAmt = %itemDrop;
								};
								%worldItem.setVelocity(getRandom(-3,3) SPC getRandom(-3,3) SPC getRandom(2,7));
								%worldItem.schedulePop();
							}
						}
					}
				}
				
				if (getRandom(1, 1000) <= %db.itemChance)
				{
					if (%db.itemDrop $= "HELD_ITEM")
						%toDrop = %obj.getMountedImage(0).item;
					else
						%toDrop = %db.itemDrop;
						
					%item = new Item()
					{
						datablock = %toDrop;
						static    = "0";
						position  = vectorAdd(%position,"0 0 1");
						craftedItem = false;
					};
					%item.setVelocity("0 0 5");
					%client.chatMessage("\c5" @ %db.itemFlavor);
					%item.schedulePop();
				}
				if (getRandom(1,1000) <= %db.blueprintChance)
				{
					if (true || getRandom(1,$EOTW::BlueprintCount + 1) > %client.GetBlueprintCount())
					{
						%item = new Item()
						{
							datablock = BlueprintItem;
							static    = "0";
							position  = vectorAdd(%position,"0 0 1");
							rotation = EulerToAxis(getRandom(0,359) SPC getRandom(0,359) SPC getRandom(0,359)); //Todo: Get this to work.
						};
						%item.setVelocity("0 0 " @ getRandom(2,7));
						%item.schedulePop();
					}
					
				}
				if (%db.score > 0)
				{
					%client.incScore(%db.score);
				}
			}
		}	
	}
	
	function EOTW_UpdatePlayerStats(%cl)
	{
		if (!isObject(%cl))
			return;
			
		if ($EOTW::IsDay) %timeText = "Day";
		else %timeText = "Night";
		
		if (isObject(%cl.player))
		{
			//if (!(isObject(%cl.player.getMountedImage(0)) && striPos("DeckOutImage ChipImage CardsOutImage",%cl.player.getMountedImage(0).getName()) != -1))
				%cl.bottomPrint("<just:center>\c3" @ %timeText @ "\c6: " @ $EOTW::Day @ "  \c3Time\c6: " @ $LastEOTWTime @ "     " @ %cl.player.getHealthText() @ $EOTW::BossText,30);
		}
		else if (!isObject(%cl.player))
			%cl.bottomPrint("<just:center>\c3" @ %timeText @ "\c6: " @ $EOTW::Day @ "  \c3Time\c6: " @ $LastEOTWTime @ $EOTW::BossText,30);
	}
	
	function ServerLoadSaveFile_End()
	{
		Parent::ServerLoadSaveFile_End();
		messageAll('MsgAdminForce', "\c3Environment Master\c6: Brick loading has finished! \c2Material and enemy generation has begun.");
		//echo("File saving finished. Starting material + enemy spawn loop.");


		//TODO: override here
		return;

		if(isObject(Gatherables))
			Gatherables.deleteAll();
		
		cancel($GatherableLoop);
		$GatherableLoop = schedule(60 * 1000, 0, "GatherableLoop");

		cancel($EnemySpawnLoop);
		EnemySpawnLoop();

		cancel($EOTW::TipLoop);
		doTipLoop();
		
		doPowerPreload();
	}
	
	function giveMaterial(%client,%mat,%amt)
	{
		$EOTW::Material[%client.bl_id,%mat] += %amt;
		%lwrMat = strLwr(%mat);
		
		EOTW_UpdatePlayerStats(%cl);
			
		if (%lwrMat $= "")
			%client.AddAchievementProgress("",%amt);
		else if (%lwrMat $= "wood")
		{
			%client.AddAchievementProgress("Woodcutter",%amt);
			%client.incScore(mFloor(%amt / 750));
		}
		else if (%lwrMat $= "stone")
		{
			%client.AddAchievementProgress("Miner",%amt);
			%client.incScore(mFloor(%amt / 25));
		}
		else if (%lwrMat $= "glass")
		{
			%client.AddAchievementProgress("Glass Blower",%amt);
			%client.incScore(mFloor(%amt / 2));
		}
		else if (%lwrMat $= "grass" || %lwrMat $= "vine")
		{
			%client.incScore(mFloor(%amt / 3));
		}
		else if (%lwrMat $= "metal")
		{
			%client.AddAchievementProgress("Metallurgist",%amt);
			%client.incScore(mFloor(%amt / 14));
		}
		else if (%lwrMat $= "copper" || %lwrMat $= "lead" || %lwrMat $= "wolfram")
		{
			%client.AddAchievementProgress("Metal Collector",%amt);
			if (%lwrMat $= "copper")
				%client.incScore(mFloor(%amt * 2));
			else if (%lwrMat $= "lead")
				%client.incScore(mFloor(%amt / 2));
				
		}
		else if (%lwrMat $= "sturdium")
		{
			%client.AddAchievementProgress("Blue Mist",%amt);
			%client.incScore(mFloor(%amt / 3));
		}
	}
	
	function getProperMatName(%mat)
	{
		%lwrMat = strLwr(%mat);
		
		if (%lwrMat $= "wood")
			return "Wood";
		else if (%lwrMat $= "stone")
			return "Stone";
		else if (%lwrMat $= "glass")
			return "Glass";
		else if (%lwrMat $= "grass")
			return "Grass";
		else if (%lwrMat $= "vine" || %lwrMat $= "vines" || %lwrMat $= "weed")
			return "Vine";
		else if (%lwrMat $= "metal")
			return "Metal";
		else if (%lwrMat $= "copper")
			return "Copper";
		else if (%lwrMat $= "lead")
			return "Lead";
		else if (%lwrMat $= "wolfram")
			return "Wolfram";
		else if (%lwrMat $= strLwr($SturdiumName) || %lwrMat $= "???" || %lwrMat $= "Sturdium")
		{
			if ($EOTW::Day >= 53)
				return $SturdiumName;
			else
				return "???";
		}
		else
			return "Unknown";
	}
	
	function serverCmdLight(%cl)
	{
		parent::serverCmdLight(%cl);
		if (isObject(%cl.player.light))
		{
			//if (%cl.player.SuperLightEnergy > 0)
			//{
			//	%cl.player.light.setDatablock(SuperBrightLight);
			//	%cl.player.EOTW_SuperLightDrain();
			//}
			//else
			if(isObject(BrightAmbientLight))
				%cl.player.light.setDatablock(BrightAmbientLight); //Because bright light is cooler B) 		%cl.player.dump(); %cl.selectedLight
		}
	}
	
	function serverCmdSL(%cl)
	{
			if (%cl.player.SuperLightEnergy > 0)
			{
				if (!isObject(%cl.player.light))
					parent::serverCmdLight(%cl);
					
				if (%cl.player.light.getDatablock().getName() $= "SuperBrightLight") return;
				
				%cl.player.FlashlightRangeModifier = 2;
				%cl.player.light.setDatablock(SuperBrightLight);
				%cl.player.EOTW_SuperLightDrain();
			}
			else
				%cl.centerPrint("\c0Whoops!<br>\c6You do not have any Super Light charge!",3);
	}
	
	function rpgPickaxeImage::onMount(%this, %obj, %slot)
	{
		parent::onMount(%this, %obj, %slot);
		if (!%obj.client.EOTW_PickaxeWarning)
		{
			%obj.client.chatMessage("\c6The \c3Hand Drill \c6passively increases your gathering speed by 25 percent.");
			%obj.client.chatMessage("\c6To use it, just pickup a gatherable as you usually would.");
			%obj.client.EOTW_PickaxeWarning = true;
		}
	}
	
	function Player::EOTW_SuperLightDrain(%this)
	{
		if (isObject(%this.light))
		{
			%this.SuperLightEnergy--;
			if (%this.SuperLightEnergy < 1)
			{
				%this.light.setDatablock(BrightAmbientLight);
				%this.FlashlightRangeModifier = 0;
				%this.client.play2d(errorSound);
				%this.client.chatMessage("\c6Your \c4Super Light\c6 ran out of power.");
			}
			else
				%this.schedule(500,EOTW_SuperLightDrain);
		}
	}
	
	function GameConnection::OnAchievementGet(%client,%title)
	{
		parent::OnAchievementGet(%client,%title);
		
		switch$(%title)
		{
			case "Self Help":
				%client.Fastmapler_RewardCredits(1);
				
			case "Woodcutter":
				%client.Fastmapler_RewardCredits(1);
			case "Miner":
				%client.Fastmapler_RewardCredits(1);
			case "Glass Blower":
				%client.Fastmapler_RewardCredits(2);
			case "Metallurgist":
				%client.Fastmapler_RewardCredits(2);
			case "Metal Collector":
				%client.Fastmapler_RewardCredits(3);		
			case "Botanist":
				%client.Fastmapler_RewardCredits(2);
			case "Ew...":
				%client.Fastmapler_RewardCredits(4);
			case "Blue Mist":
				%client.Fastmapler_RewardCredits(5);
				
			case "First Blood":
				%client.Fastmapler_RewardCredits(1);
			case "Hunter":
				%client.Fastmapler_RewardCredits(2);
			case "Cleansing":
				%client.Fastmapler_RewardCredits(4);
			case "Thunderstorm":
				%client.Fastmapler_RewardCredits(6);
			case "Doomguy":
				%client.Fastmapler_RewardCredits(10);
				
			case "Zombie Hunter":
				%client.Fastmapler_RewardCredits(4);
			case "Bone Picker":
				%client.Fastmapler_RewardCredits(5);
			case "Janitor":
				%client.Fastmapler_RewardCredits(6);
			case "Rais Begone":
				%client.Fastmapler_RewardCredits(6);
				
			case "Time for an Upgrade":
				%client.Fastmapler_RewardCredits(2);
			case "Maxed Out!":
				%client.Fastmapler_RewardCredits(6);
			case "Anti-Pomeg":
				%client.Fastmapler_RewardCredits(2);
			case "Superpower":
				%client.Fastmapler_RewardCredits(6);
			case "Faster is Better":
				%client.Fastmapler_RewardCredits(2);
			case "Shields up!":
				%client.Fastmapler_RewardCredits(2);
				
			case "Runepower":
				%client.Fastmapler_RewardCredits(1);
			case "99Magic":
				%client.Fastmapler_RewardCredits(4);
				
			case "Treasure Hunter":
				%client.Fastmapler_RewardCredits(3);
				
			case "The End.":
				%client.Fastmapler_RewardCredits(15);
		}
	}
};
activatePackage("EOTW");


function EnvMasterLoop(%time, %day, %color, %colInc, %colTarg, %size)
{
	cancel($EnvMasterLoop);
	if($Overloaded)
	{
		$EnvMasterLoop = schedule((100 * ($SampleRate + 1)), 0, "EnvMasterLoop", %time, %day, %color, %colInc, %colTarg, %size);
		return;
	}
	%time += $TimeSync;
	$TimeSync = 0;
	if(%time >= 330 && !$EOTW::WarnSunRise && $EOTW::Day >= 9)
	{
		messageAll('MsgAdminForce', "\c3Environment Master\c6: The sun is about to rise. Please take cover.");
		$EOTW::WarnSunRise = true;
	}

	if (%time <= 0)
	{
		if (%day > 1)
		{
			%day--;
			%time = 360;
			
			%size = %day * 0.1;
			if(%size > 5)
				%size = 5;
		}
		else
		{
			%day = 1;
			%time = 0;
			
			%size = %day * 0.1;
			if(%size > 5)
				%size = 5;
				
			$EOTW::Timescale = 1;
			
			$EOTW::Age++;
			
			if(isObject(Chests))
				Chests.deleteAll();
				
			if(isObject(Gatherables))
				Gatherables.deleteAll();
				
			ServerCmdKillBots(EnvMaster);
			
			%color = "1 1 1";
			%colInc = "0.00 -0.02 -0.02";
			%colTarg = "1 0 0";
			servercmdEnvGui_SetVar(EnvMaster, "SunFlareColor", %color);
		}
	}
	else if(%time >= 360)
	{
		$EOTW::WarnSunRise = false;
		$EOTW::CurrentTime = "Day";
		%time = 0;
		%day++;
		if(isObject(Gatherables)) for(%i=0;%i<Gatherables.getCount();%i++)
			if((%brick = Gatherables.getObject(%i)).destroyOnDay <= %day) %brick.killBrick();
		%size = %day * 0.1;
		if(%size > 5)
			%size = 5;
		if(%day >= 46)
		{
			$EOTW::Timescale = -1000;
			$EOTW::BossText = "";
			messageAll('MsgAdminForce', "\c3Environment Master\c6: Congratulations, survivors! \c2Time is now reverting!");
		}
		if(%day >= 61)
		{
			%groups = MainBrickgroup.getCount();
			for(%i=0;%i<%groups;%i++)
			{
				%group = MainBrickgroup.getObject(%i);
				%bricks = %group.getCount();
				for(%j=0;%j<%bricks;%j++)
					%group.getObject(%j).dontRefund = 1;
			}

			$SturdiumName = getSturdiumName();
			//deleteVariables("$EOTW::*");
			Gatherables.delete();
			cancel($GatherableLoop);

			EOTW_SetColors();
			cancel($EnvMasterLoop);

			%groups = MainBrickgroup.getCount();
			for(%i=0;%i<%groups;%i++)
			{
				%group = MainBrickgroup.getObject(%i);
				%bricks = %group.getCount();
				for(%j=0;%j<%bricks;%j++)
				{
					%brick = %group.getObject(%j);
					if(%brick.material !$= "")
						%brick.killBrick();
				}
			}
			servercmdEnvMaster(EnvMaster);
			messageAll('MsgAdminForce', "\c3Environment Master\c6: Congratulations, survivors! \c2Starting a new round!");
			for(%i=0;%i<ClientGroup.getCount();%i++) 
			{
				if((%cl = ClientGroup.getObject(%i)).hasSpawnedOnce) %cl.instantRespawn();
				%cl.AwardAchievement("The End.");
			}
			
			 return;
		}
		switch(%day)
		{
			// case xx: %msg = " Environmental Warning: Hostile lifeforms have been spotted far away. Arrival time expected at day xx."; //Default enemy warning
			// case xx: %msg = " Environmental Warning: Hostile lifeform has arrived. Enemy is ___."; //Default enemy info.
			// case xx: %msg = " Environmental Warning: Hostile lifeform has arrived. Enemy is an aggressive crab. Minimal threat level, but harmful.";
			// case xx: %msg = " Environmental Warning: Hostile lifeform has arrived. Enemy is a heat resistant zombie. High threat level. Dubbed as 'Husk'.";
			// case xx: %msg = " Environmental Warning: Hostile lifeform has arrived. Enemy is a skeleton with access to primitive firearms. Dubbed as 'Revenant'.";
			// case xx: %msg = " Environmental Warning: Hostile lifeform has arrived. Enemy is a exotic force with unknown properties. Dubbed as 'Elemental'.";
			// case xx: %msg = " Environmental Warning: Hostile lifeform has arrived. Enemy is similar to a husk, but it is only made of muscle and sturdium. Dubbed as 'Golem'.";
			case 5: %msg = " Environment Notice: Grass seeds will no longer spawn for collection.";
			case 7: %msg = " Environment Notice: Grass will now die if directly exposed to the sun.";
			case 9: %msg = " Environmental Warning: Hostile lifeforms have been spotted far away. Arrival time is expected at day 11.";
			case 10: %msg = " Environmental Warning: It is no longer safe to scavenge during the day.";
			case 11: %msg = " Environmental Warning: Hostile lifeforms have arrived. Enemy is zombie-like, and will die in the sun.";
			case 12: %msg = " Environment Notice: Copper deposits have been found in the area. Analysis finds that copper has use in crafting.";
			case 14: %msg = " Environment Notice: Vines will now die if directly exposed to the sun.";
			case 15: %msg = " Environment Notice: Vine seeds and wood will no longer spawn for collection.";
			case 17: %msg = " Environment Notice: Lead has been found in the area. Analysis finds that lead can be used to craft items.";
			case 20: %msg = " Environmental Warning: Wood and dead plants will ignite if directly exposed to the sun.";//      <color:ff0000>praise";
			case 21: %msg = " Environmental Warning: A new species of hostile lifeforms have been spotted far away. Arrival time is expected at day 24.";
			case 24: %msg = " Environmental Warning: Hostile lifeforms have arrived. Enemy is a heat resistant zombie. High threat level.";
			case 25: %msg = " Environment Notice: Stone will no longer spawn for collection.";//      <color:ff0000>the king";
			case 26: %msg = " Environmental Warning: A species of boney hostile lifeforms have been spotted far away. Arrival time is expected at day 29.";
			case 29: %msg = " Environmental Warning: Hostile lifeform has arrived. Enemy is a skeleton with access to primitive projectiles.";
			case 30: %msg = " Environmental Warning: Meteors are raining from the sky; collisions may severely damage structures.";//      <color:ff0000>he shall rise";
			case 31: %msg = " Environmental Warning: Exotic lifeforms have been spotted far away. Arrival time estimated day 34.";
			case 34: %msg = " Environmental Warning: Exotic lifeforms have arrived. Enemy is a mystic force with unknown properties.";
			case 35: %msg = " Environment Notice: Glass will no longer spawn for collection.";      //<color:ff0000>burn grass and vines to survi- die.";
			case 36: %msg = " WARNING: A band of gun wielding bandits have entered the area! They only appear during the night.";
			case 37: %msg = " Environment Notice: Grass will now die even when indoors.";
			case 38: %msg = " Environmental Warning: Players can now catch fire if exposed to the sun, fire, or other burning players.";
			case 39: %msg = " Environmental Warning: Jelly-like monsters have been found.";
			case 40: %msg = " Environment Notice: The Unfleshed have moved away from the area.";//      <color:ff0000>they cower over him hahah";
			case 41: %msg = " Environmental Warning: Ignitable materials will now catch fire even when indoors.";
			case 42: %msg = " Environment Notice: Life, the Universe, and everything.";
			case 43: %msg = " Environment Warning: Time anomaly detected. Arrival on the sunrise of day 46.";
			case 44: %msg = " Environment Notice: The Husks have moved away from the area.";
			case 45: %msg = " Environment Notice: Metal will no longer spawn for collection.";//      <color:ff0000>he shall rule";
			case 46: %msg = " Environment Warning: Time anomaly arrived.";
			case 47: %msg = " Environment Notice: Vines will now die even when indoors.";
			case 48: %msg = " Environment Notice: Players no longer regenerate over time.";
			case 49: %msg = " Environment Notice: The Revenants have moved away from the area.";
			case 50: %msg = " Environmental Warning: The heat has reached critical levels; no known materials can withstand it.";//      <color:ff0000>he is behemoth the kin- <color:00ff00>stop acting so edgy lamo";
			case 53: %msg = " Environment Notice: A new material has been discovered. It has been given the temporary name of " @ $SturdiumName @ ".";
			case 54: %msg = " Environment Notice: The Elementals have stopped forming.";
			case 55: %msg = " Environment Notice: " @ $SturdiumName @ " will no longer spawn for collection.";
			case 56: %msg = " Environment Notice: The Bandits have left the area. No more monsters left to annoy us.";
			case 57: %msg = " Environment Notice: No material except for " @ $SturdiumName @ " can withstand the heat, even when indoors.";//      <color:0ff00>kill behemoth to win this bad solar apoc mod ok?";
			case 58: %msg = " Environment Notice: " @ $SturdiumName @ " will no longer withstand the heat.";
			//case 60: %msg = " Environ<color:ffffff>MEAW QQQQQQ:<color:840aff> Behemoth has awoken!       <color:00ff00>kill em dead!1!";
		}
		messageAll('', "\c3Environment Master\c6: The sun rises on Day " @ %day @ "." @ %msg);
		
		if($EOTW::Day > 7) backupEOTWPlayerData();
		
		%color = "1 1 1";
		%colInc = "0.00 -0.02 -0.02";
		%colTarg = "1 0 0";
		for (%i = 0; %i < %day; %i++)
		{
			%color = vectorAdd(%color, %colInc);
			if(vectorDist(%color, %colTarg) == 0)
			{
				switch$(%colTarg)
				{
					case "1 0 0":
						if(%colInc $= "0.1 0 0")
						{
							%colInc = "0 0.1 0";
							%colTarg = "1 1 0";
						}
						else
						{
							%colInc = "0 0 0";
							%colTarg = "0 0 0";
						}
					case "1 1 0":
						%colInc = "-0.1 0 0";
						%colTarg = "0 1 0";
					case "0 1 0":
						%colInc = "0 0 0.1";
						%colTarg = "0 1 1";
					case "0 1 1":
						%colInc = "0 -0.1 0";
						%colTarg = "0 0 1";
					case "0 0 1":
						%colInc = "0.1 0.1 0";
						%colTarg = "1 1 1";
					case "1 1 1":
						%colInc = "0 0 0";
						%colTarg = "0 0 0";
				}
			}
		}
		
		servercmdEnvGui_SetVar(EnvMaster, "SunFlareColor", %color);
	}
	$EOTW::Day = %day; $EOTW::Time = %time;
	if(%time >= 180 && $EOTW::CurrentTime !$= "Night")
	{
		$EOTW::CurrentTime = "Night";
		messageAll('', "\c3Environment Master\c6: The sun sets on Day " @ %day @ "."@(%day >= 10 ? " It is now safe to scavenge." : ""));
		servercmdEnvGui_SetVar(EnvMaster, "SunFlareColor", "0 0 0");
		for(%i=0;%i<EOTWEnemies.getCount();%i++)
			if (!EOTWEnemies.getObject(%i).immuneToNight)
				EOTWEnemies.getObject(%i).kill();
				
		for (%i=0;%i<EOTWEnemies.getCount();%i++)
		{
			if (EOTWEnemies.getObject(%i).getDatablock().getName() $= "SturdiumBeastHoleBot")
			{
				%beastSpawned = true;
				break;
			}
		}
		
		if ($EOTW::Day >= 40 && !%beastSpawned && $EOTW::TimeScale > 0)
		{
			%initPos = "";
			
			%victim = ClientGroup.getObject(getRandom(0,ClientGroup.getCount() - 1)).player;
			if (isObject(%victim)) %initPos = (getWord(%victim.getPosition(),0) + getRandom(-128, 128)) TAB (getWord(%victim.getPosition(),1) + getRandom(-128, 128));
			
			%trans = EOTW_getEnemyRaycastSpawnLocation(%initPos);
			%trans = %trans SPC eulerToAxis("0 0" SPC getRandom(0,359));
				//if (getWord(%trans,0) > 64 && getWord(%trans,0) < 1660 && getWord(%trans,1) > 64 && getWord(%trans,1) < 1660)
			setWord(%trans,2,"128");
			spawnNewZombie(%trans,"SturdiumBeastHoleBot");
					
			if (isObject(%victim)) messageAll('',"\c5A beast spawned near " @ %victim.client.name @ "! Kill it immediately for special loot!");
			else messageAll('',"\c5A beast spawned somewhere on the map! Kill it immediately for special loot!");
		}
		
		if (getRandom(0,5) == 1 && $EOTW::Day >= 15) schedule(getRandom(20000,30000),0,"EnemySpawnSwarm");
	}
	if(%time > 180)
	{
		$EOTW::IsDay = 0; %flare = 0;
		%realA = 1 - (mAbs(%time - 90) / 120);
		%realB = 1 - (mAbs(%time - 450) / 120);
		%realFlare = getMax(%realA, %realB);
		if(%realFlare < 0) 
			%realFlare = 0;
	}
	else
	{
		$EOTW::IsDay = 1;
		%flare = 1 - (mAbs(%time - 90) / 120);
		%realFlare = %flare;
		if(%day >= 20 && false)
		{
			for(%i=0;%i<mCeil((getBrickCount() - 72960) / 2000);%i++)
			{
				%groups = MainBrickgroup.getCount();
				for(%j=0;%j<%groups;%j++)
					%bricks[%j] = MainBrickgroup.getObject(%j).getCount() + %bricks[%j - 1];
				%highest = %bricks[%groups - 1];
				%select = getRandom(1, %highest);
				while(%bricks[%sel] < %select) %sel++;
				while(MainBrickgroup.getObject(%sel).getCount() == 0 && %sel < %groups) %sel++;
				if(%sel < %groups)
				{
					%group = MainBrickgroup.getObject(%sel);
					%bricks = %group.getCount();
					if(%bricks != 0)
					{
						%brick = %group.getObject(getRandom(0, %bricks - 1));
						if(%brick.isIgnitable)
						{
							if(%day >= 41) %brick.burnLoop(getSimTime());
							else
							{
								%pi = 3.14159265; %val = (%time / 180) * %pi;
								%ang = ($EnvGuiServer::SunAzimuth / 180) * %pi;
								%dir = vectorScale(mSin(%ang) * mCos(%val) SPC mCos(%ang) * mCos(%val) SPC mSin(%val), 512);
								%ray = containerRaycast(vectorAdd(%pos = %brick.getPosition(), %dir), %pos, $Typemasks::fxBrickAlwaysObjectType | $Typemasks::StaticShapeObjectType);
								if(!isObject(%hit = firstWord(%ray)) || %hit == %brick)
									%brick.burnLoop(getSimTime());
							}
						}
					}
				}
			}
		}
		if(%day >= 30)
		{
			%pi = 3.14159265; %val = (%time / 180) * %pi;
			%ang = ($EnvGuiServer::SunAzimuth / 180) * %pi;
			%dir = vectorScale(mSin(%ang) * mCos(%val) SPC mCos(%ang) * mCos(%val) SPC mSin(%val), 512);
			%hit = getRandom(-1000, 1000) SPC getRandom(-1000, 1000) SPC 0;
			%proj = new Projectile()
			{
				datablock = EOTWFireballProjectile;
				initialPosition = vectorAdd(%hit, vectorScale(%dir, 1));
				initialVelocity = vectorScale(%dir, -100);
			};
		}
		if(%day >= 50)
		{
			if(mAbs(%time * 10 - mFloor(%time * 10)) <= 0.05)
			{
				for(%i=0;%i<mCeil((getBrickCount() - 72960)/(1000/(%day-49)));%i++)
				{
					%groups = MainBrickgroup.getCount();
					for(%j=0;%j<%groups;%j++)
						%bricks[%j] = MainBrickgroup.getObject(%j).getCount() + %bricks[%j - 1];
					%highest = %bricks[%groups - 1];
					%select = getRandom(1, %highest);
					while(%bricks[%sel] < %select) %sel++;
					while(MainBrickgroup.getObject(%sel).getCount() == 0 && %sel < %groups) %sel++;
					%group = MainBrickgroup.getObject(%sel);
					%bricks = %group.getCount();
					if(%bricks != 0)
					{
						%brick = %group.getObject(getRandom(0, %bricks - 1));
						if(%brick.isBreakable || %day >= 58)
						{
							if(%day >= 57 && %brick.isBreakable) %brick.killBrick();
							else
							{
								%pi = 3.14159265; %val = (%time / 180) * %pi;
								%ang = ($EnvGuiServer::SunAzimuth / 180) * %pi;
								%dir = vectorScale(mSin(%ang) * mCos(%val) SPC mCos(%ang) * mCos(%val) SPC mSin(%val), 512);
								%ray = containerRaycast(vectorAdd(%pos = %brick.getPosition(), %dir), %pos, $Typemasks::fxBrickAlwaysObjectType | $Typemasks::StaticShapeObjectType);
								if(!isObject(%hit = firstWord(%ray)) || %hit == %brick)
									%brick.killBrick();
							}
						}
					}
				}
			}
		}
	}
	%pi = 3.14159265;
	%val = (%time / 180) * %pi;
	%ang = ($EnvGuiServer::SunAzimuth / 180) * %pi;
	%dir = vectorScale(mSin(%ang) * mCos(%val) SPC mCos(%ang) * mCos(%val) SPC mSin(%val), 512);
	%pos[-1+%posCount++] = "0 0 0";
	
	for(%i=0;%i<ClientGroup.getCount();%i++)
		if(isObject(%pl = ClientGroup.getObject(%i).player))
			%pos[-1+%posCount++] = %pl.getPosition();
			
	for(%i=0;%i<%posCount;%i++)
	{
		initContainerRadiusSearch(%pos[%i], 240, $Typemasks::PlayerObjectType | $TypeMasks::VehicleObjectType);
		while(isObject(%obj = containerSearchNext()))
		{
			%isVehicle = striPos(%obj.getClassName(),"Vehicle") > -1;
			if(%isVehicle || (%obj.getState() !$= "DEAD" && !%hasHarmed[%obj]))
			{
				%hasHarmed[%obj] = 1;
				if (%isVehicle) %hit = containerRaycast(vectorAdd(%pos = %obj.getPosition(), %dir), %pos, $Typemasks::fxBrickObjectType | $Typemasks::StaticShapeObjectType);
				else %hit = containerRaycast(vectorAdd(%pos = %obj.getHackPosition(), %dir), %pos, $Typemasks::fxBrickObjectType | $Typemasks::StaticShapeObjectType);
				if(!isObject(%hit) && %size >= 1 && %flare != 0 && $EOTW::Timescale > 0)
				{
					if(%obj.getDamagePercent() >= 1)
						%obj.damage(0, 0, 10000, ''); //$DamageType::Sun
					else
					{
						%damage = %obj.getDamageLevel() + ((%size / 4) * ($SampleRate + 1) * 5);
						if(%damage >= 100 && !%obj.immuneToSun)
							%obj.damage(0, 0, 10000, ''); //$DamageType::Sun
						else
						{
							if(!%obj.immuneToSun)
							{
								if (%isVehicle) %obj.setDamageLevel(%damage);
								else %obj.addHealth(-1 * ((%size / 4) * ($SampleRate + 1) * 5));
								%obj.setDamageFlash(0.25);
							}
							if(%day >= 38 && !%obj.immuneToFire)
							{
								%obj.burningTime += (%day / 50) * ($SampleRate + 1) * (%obj.tripleFire ? 3 : 1);
								if(!isEventPending(%obj.burnLoop))
									%obj.burnLoop(getSimTime());
							}
						}
						if(isObject(%cl = %obj.client) && !%obj.immuneToSun)
							%cl.centerPrint("<color:FF8000>Get into a shaded area!<br>" @ %obj.getHealthText(), 2);
						if(%obj.immuneToSun && %obj.deimmuneTime > 0)
						{
							%obj.deimmuneTime -= (100 * ($SampleRate + 1));
							if(%obj.deimmuneTime <= 0)
							{
								%obj.deimmuneTime = 0;
							}
							%obj.immuneToSun = 0;
							%obj.immuneToFire = 0;
							%obj.immuneToBurn = 0;
							%obj.immuneToMeteors = 0;
						}
					}
				}
				else if(%obj.getDamageLevel() > 0 && $EOTW::Day < 48)
					%obj.setDamageLevel(%obj.getDamageLevel() - (0.01 * ($SampleRate + 1)));
			}
		}
	}
	%timeOfDay = %time + 90;
	if(%timeOfDay > 360)
		%timeOfDay -= 360;
	%hours = mFloor(%timeOfDay / 15);
	%mins = mFloor((%timeOfDay - (%hours * 15)) * 4);
	if (!$Pref::ETOW::MilitaryTime)
	{
		if(%hours >= 12)
		{
			%hours -= 12;
			%suffix = "PM";
		}
		else
			%suffix = "AM";
		if(%hours == 0)
			%hours += 12;
	}
	if(strLen(%mins) == 1)
		%mins = "0" @ %mins;
	if(%hours @ ":" @ %mins !$= $LastEOTWTime && !(%mins % 10))
	{
		$LastEOTWTime = %hours @ ":" @ %mins;
		for(%k = 0;%k < ClientGroup.getCount(); %k++)
		{
			%cl = ClientGroup.getObject(%k);
			if(isObject(%pl = %cl.player))
			{
				if((isObject(%image = %pl.getMountedImage(0)) && %image.getName() $= "BrickImage") || isEventPending(%pl.collectLoop))
				{
					%cl.showMaterials(); 
					%dontPrint = 1; 
				}
				else %health = %pl.getHealthText();
			}
			else %health = "<just:center><color:FFFFFF>Health: <color:FF0000>YOU'RE DEAD";
			//if(!%dontPrint) %cl.bottomPrint("<just:center>\c3Time\c6: " @ %hours @ ":" @ %mins @ " " @ %suffix @ "     " @ %health, 30);
			if (!%dontPrint) EOTW_UpdatePlayerStats(%cl);
		}
	}
	if(%time >= 330 || %time <= 210)
	{
		if(%time >= 330)
			%colTime = %time - 360;
		else
			%colTime = %time;
		%colVal = 1 - mAbs((%colTime - 90) / 120);
	}
	else
		%colVal = 0;
	if(%time >= 345 || %time <= 15)
	{
		%fogTime = -(mAbs(%time - 180) - 180);
		%fogVal = 1 - (%fogTime / 15);
	}
	else if(%time >= 165 && %time <= 195)
		%fogVal = 1 - mAbs((%time - 180) / 15);
	else
		%fogVal = 0;
	if(%fogVal >= 0)
		servercmdEnvGui_SetVar(EnvMaster, "FogColor", vectorScale(vectorAdd(vectorScale(%color, %fogVal), vectorScale(%color, %colVal)), 0.8));
	%sizeVal = %colVal * mSqrt(%flare);
	%realSizeVal = %colVal * mSqrt(%realFlare);
	%sunCol = vectorScale(%color, (%realSizeVal + %sizeVal) * 0.5);
	%ambCol = vectorScale(%color, %realSizeVal * 0.7);
	%shadCol = vectorScale(%color, %realSizeVal * 0.4);
	servercmdEnvGui_SetVar(EnvMaster, "VignetteColor", vectorScale(%color, %colVal) SPC ((%size * %colVal) / 5));
	servercmdEnvGui_SetVar(EnvMaster, "SkyColor", vectorScale(%color, %colVal));
	servercmdEnvGui_SetVar(EnvMaster, "DirectLightColor", %sunCol);
	servercmdEnvGui_SetVar(EnvMaster, "AmbientLightColor", %ambCol);
	servercmdEnvGui_SetVar(EnvMaster, "ShadowColor", %shadCol);
	servercmdEnvGui_SetVar(EnvMaster, "SunElevation", %time);
	servercmdEnvGui_SetVar(EnvMaster, "SunFlareSize", (%val = (mSqrt(%flare) * %size)) < 0.1 ? 0.1 : %val);
	
	if(%day <= 10) $EnvMasterLoop = schedule((100 * ($SampleRate + 1)), 0, "EnvMasterLoop", %time += (($EOTW::TimeScale * 0.12) * ($SampleRate + 1)), %day, %color, %colInc, %colTarg, %size);
	else if(%day <= 20) $EnvMasterLoop = schedule((100 * ($SampleRate + 1)), 0, "EnvMasterLoop", %time += (($EOTW::TimeScale * 0.10) * ($SampleRate + 1)), %day, %color, %colInc, %colTarg, %size);
	else if(%day <= 30) $EnvMasterLoop = schedule((100 * ($SampleRate + 1)), 0, "EnvMasterLoop", %time += (($EOTW::TimeScale * 0.08) * ($SampleRate + 1)), %day, %color, %colInc, %colTarg, %size);
	else if(%day <= 40) $EnvMasterLoop = schedule((100 * ($SampleRate + 1)), 0, "EnvMasterLoop", %time += (($EOTW::TimeScale * 0.06) * ($SampleRate + 1)), %day, %color, %colInc, %colTarg, %size);
	else if(%day <= 50) $EnvMasterLoop = schedule((100 * ($SampleRate + 1)), 0, "EnvMasterLoop", %time += (($EOTW::TimeScale * 0.04) * ($SampleRate + 1)), %day, %color, %colInc, %colTarg, %size);
	else $EnvMasterLoop = schedule((100 * ($SampleRate + 1)), 0, "EnvMasterLoop", %time += (($EOTW::TimeScale * 0.02) * ($SampleRate + 1)), %day, %color, %colInc, %colTarg, %size);
}

function servercmdEnvMaster(%cl)
{
	if(isObject(%cl) && !%cl.isSuperAdmin)
		return;
	if(!isEventPending($EnvMasterLoop))
	{
		if(!isObject(EnvMaster))
			new ScriptObject(EnvMaster) { isAdmin = 1; isSuperAdmin = 1; environMaster = 1; };
		
		//EnvMasterLoop(360, 8, "1 1 1", "0.00 -0.02 -0.02", "1 0 0", 0.8);
		//servercmdEnvGui_SetVar(EnvMaster, "SkyIdx", 13); //11
		for (%i = 0; %i < $EnvGUIServer::SkyCount; %i++)
		{
			if ($EnvGUIServer::Sky[%i] $= "Add-Ons/Sky_ROBLOX/Alien Red/AlienRed.dml")
			{
				servercmdEnvGui_SetVar(EnvMaster, "SkyIdx", %i);
				break;
			}
		}
		EnvMasterLoop(360, 0, "1 1 1", "0.00 -0.02 -0.02", "1 0 0", 0);
		//servercmdEnvGui_SetVar(EnvMaster, "GroundIdx", 6);
		servercmdEnvGui_SetVar(EnvMaster, "SunAzimuth", 75);
		schedule(96, 0, "servercmdEnvGui_SetVar", EnvMaster, "SunAzimuth", 75);
		servercmdEnvGui_SetVar(EnvMaster, "SunFlareTopIdx", 2);
		servercmdEnvGui_SetVar(EnvMaster, "SunFlareBottomIdx", 4);
		servercmdEnvGui_SetVar(EnvMaster, "SunFlareSize", 10);
		servercmdEnvGui_SetVar(EnvMaster, "WaterColor", "0.0 0.5 1.0 0.5");
		servercmdEnvGui_SetVar(EnvMaster, "UnderwaterColor", "0.0 0.5 1.0 0.5");
		servercmdEnvGui_SetVar(EnvMaster, "GroundColor", "0.0 0.0 0.0 0.0"); //0.1 0.3 0.2 1.0
		if(!$EOTW_ClearedEvents) clearIllegalEvents();
		cancel($GatherableLoop);
		cancel($EnemySpawnLoop);
		
		EOTW_AddAchievements();
		
		EOTW_PlayerSearchLoop();
	}
}

function clearIllegalEvents()
{
	unregisterOutputEvent("fxDtsBrick", "setItem");		//Bypasses blacklist
	unregisterOutputEvent("fxDtsBrick", "spawnExplosion");	//Assholes try to lag the server up.
	unregisterOutputEvent("fxDtsBrick", "spawnItem");	//Allows players to bypass the crafting process + Bypasses Blacklist
	unregisterOutputEvent("fxDtsBrick", "spawnProjectile");	//People shoot projectiles at others.
	
	unregisterOutputEvent("Player", "addHealth");		//This is a survival-based gamemode. No healing.
	unregisterOutputEvent("Player", "changeDatablock");	//Incredibly abusable; people give themselves jets.
	unregisterOutputEvent("Player", "clearTools");		//People *will* use this to clear others' tools.
	unregisterOutputEvent("Player", "instantRespawn");	//Death traps and the like. People are assholes.
	unregisterOutputEvent("Player", "kill");		//Death traps and the like. People are assholes.
	unregisterOutputEvent("Player", "setHealth");		//This is a survival-based gamemode. No healing.
	unregisterOutputEvent("Player", "spawnExplosion");	//Assholes try to lag the server up + Landmines/Turrets
	unregisterOutputEvent("Player", "spawnProjectile");	//People shoot projectiles at others + Turrets
	unregisterOutputEvent("Player", "setPlayerScale");	//Trap players by increasing their size.
	unregisterOutputEvent("Player", "setMaxHealth");	//Allows to give players inf. health
	unregisterOutputEvent("Player", "addMaxHealth");
	unregisterOutputEvent("Player", "setInvulnerbilityTime");
	unregisterOutputEvent("Player", "setFInvulnerbilityTime");
	unregisterOutputEvent("Player", "setInvulnerbility");
	unregisterOutputEvent("Player", "saveHealth");
	unregisterOutputEvent("Player", "loadHealth");
	unregisterOutputEvent("Player", "addVelocity");
	unregisterOutputEvent("Player", "setVelocity");
	
	unregisterOutputEvent("MiniGame", "CenterPrintAll");		//Spammable.
	unregisterOutputEvent("MiniGame", "BottomPrintAll");		//Spammable.
	unregisterOutputEvent("MiniGame", "ChatMsgAll");		//Spammable.
	unregisterOutputEvent("MiniGame", "Reset");		//Just No.
	unregisterOutputEvent("MiniGame", "RespawnAll");		//NO
	
	unregisterOutputEvent("bot", "setMaxHealth");
	unregisterOutputEvent("bot", "addMaxHealth");
	unregisterOutputEvent("bot", "setInvulnerbilityTime");
	unregisterOutputEvent("bot", "setFInvulnerbilityTime");
	unregisterOutputEvent("bot", "setInvulnerbility");
	
	unregisterOutputEvent("vehicle", "setMaxHealth");
	unregisterOutputEvent("vehicle", "addMaxHealth");
	unregisterOutputEvent("vehicle", "setInvulnerbilityTime");
	unregisterOutputEvent("vehicle", "setFInvulnerbilityTime");
	unregisterOutputEvent("vehicle", "setInvulnerbility");
}

//function Player::getHealthText(%pl)
//{
//	%bars = mCeil((100 - %pl.getDamageLevel()) / 5);
//	for(%i=0;%i<%bars;%i++)
//		%bar0 = %bar0@"|";
//	for(%i=%bars;%i<20;%i++)
//		%bar1 = %bar1@"|";
//	return "<color:FFFFFF>Health: <color:00FF00>" @ %bar0 @ "<color:FF0000>" @ %bar1;
//}

function Player::getHealthText(%pl)
{
	%pl_total = %pl.getDatablock().maxDamage;
	%pl_health = %pl_total-%pl.getDamageLevel();
	%bars = mCeil((%pl_total - (%pl_total - %pl_health)) / 5);

	for(%i=0;%i<%bars;%i++)
		%bar0 = %bar0@"|";

	for(%i=%bars;%i<mCeil(%pl_total / 5);%i++)
		%bar1 = %bar1@"|";

	return "<color:FFFFFF>Health: <color:00FF00>" @ %bar0 @ "<color:FF0000>" @ %bar1;
}

//SkyIdx 9
//WaterIdx 4
//GroundIdx 6
//SunAzimuth
//SunElevation
//DirectLightColor
//AmbientLightColor
//ShadowColor
//SunFlareTopIdx 2
//SunFlareBottomIdx 4
//SunFlareColor R G B (A = 1)
//SunFlareSize: Size (0 to 5)
//SkyColor R G B (A = 1)
//WaterColor R G B A
//UnderwaterColor R G B A
//WaterScrollX
//WaterScrollY
//GroundColor R G B (A = 1)
//VignetteColor

exec("./OLD-CreateBrick.cs");
exec("./OLD-Fire.cs");
exec("./OLD-Fireball.cs");
exec("./OLD-Plants.cs");
exec("./OLD-Materials.cs");
exec("./OLD-Math.cs");
exec("./OLD-Extras.cs");
exec("./OLD-Zombies.cs");
exec("./OLD-Blob.cs");

exec("./OLD-Emote_InvExpand.cs");
exec("./OLD-Emote_PotionEffects.cs");
exec("./OLD-Item_InvExpander.cs");
exec("./OLD-Item_Potions.cs");
exec("./OLD-Weapon_Boss.cs");
exec("./OLD-Support_Power.cs");
exec("./OLD-Inventory.cs");
exec("./OLD-Magic.cs");
exec("./OLD-TradePost.cs");
exec("./OLD-BrickControlsMenu.cs");

schedule(0, 0, "servercmdEnvMaster");
schedule(0, 0, "updateItemNames");

new SimSet(ItemDataGroup);
for (%i=0;%i<datablockGroup.getCount();%i++)
{
	%item = datablockGroup.getObject(%i);
	%name = getSubStr(%item.uiName,striPos(%item.uiName,"]") + 2,100);
	%type2 = $EOTW::ItemTier[%name];
	
	if (%item.getClassName() $= "ItemData")
	{	
		ItemDataGroup.add(%item);
		$EOTW::ItemCount++;
		if(%type2 !$= "") $EOTW::BlueprintCount++;
	}
}

//TODO: re-enable this
function EOTW_AddAchievements()
{
	if(!isFunction("AddServerAchievement"))
		return;

	AddServerAchievement("Self Help","Say '/help'.","NONE",0);
	AddServerAchievement("Woodcutter","Pickup 20k Wood.","DisplayPoints DisplayPercent",20000);
	AddServerAchievement("Miner","Pickup 3.5k Stone.","DisplayPoints DisplayPercent",3500);
	AddServerAchievement("Glass Blower","Pickup 200 Glass.","DisplayPoints DisplayPercent",200);
	AddServerAchievement("Metallurgist","Pickup 2k Metal.","DisplayPoints DisplayPercent",2000);
	AddServerAchievement("Metal Collector","Pickup 700 in Lead, Copper, and Wolfram.","DisplayPoints DisplayPercent",700);
	AddServerAchievement("Botanist","Grow a total of 1.2k studs in plants.","DisplayPoints DisplayPercent",1200);
	AddServerAchievement("Blue Mist","Pickup 250 Sturdium.","HideTitle HideDisc DisplayPoints DisplayPercent",250);
	AddServerAchievement("First Blood","Kill an enemy.","NONE",0);
	AddServerAchievement("Hunter","Kill 10 enemies.","DisplayPoints DisplayPercent",10);
	AddServerAchievement("Cleansing","Kill 40 enemies.","DisplayPoints DisplayPercent",40);
	AddServerAchievement("Thunderstorm","Kill 90 enemies.","DisplayPoints DisplayPercent",90);
	AddServerAchievement("Doomguy","Kill 180 enemies.","HideTitle DisplayPoints DisplayPercent",180);
	AddServerAchievement("Zombie Hunter","Kill 25 Unfleshed and Husks.","DisplayPoints DisplayPercent",25);
	AddServerAchievement("Bone Picker","Kill 20 Revenants.","DisplayPoints DisplayPercent",20);
	AddServerAchievement("Janitor","Kill 20 Fire Elementals.","DisplayPoints DisplayPercent",20);
	AddServerAchievement("Rais Begone","Kill 20 Bandits.","DisplayPoints DisplayPercent",20);
	//AddServerAchievement("Runepower","Charge a blank rune.","NONE",0);
	//AddServerAchievement("99Magic","Achieve all of the avaliable spells.","NONE",0);
	//AddServerAchievement("Crafty","Craft an inventory item.","NONE",0);
	AddServerAchievement("Time for an Upgrade","Use a backpack expander.","NONE",0);
	AddServerAchievement("Maxed Out!","Max out your inventory space.","HideTitle",0);
	AddServerAchievement("Anti-Pomeg","Increase your max health.","NONE",0);
	AddServerAchievement("Superpower","Max out your max health.","NONE",0);
	AddServerAchievement("Faster is Better","Craft a Hand Drill.","NONE",0);
	//AddServerAchievement("Shields up!","Craft a power shield.","NONE",0);
	AddServerAchievement("Treasure Hunter","Find and open up a treasure chest.","NONE",0);
	//AddServerAchievement("The End.","Experience day 61.","HideDisc",0);
}

//Hot Water
//schedule(100, 0, registerLoadingScreen, "https://i.imgur.com/RSh0ALZ.png", "png");

//Solar Apoc
//schedule(100, 0, registerLoadingScreen, "https://i.imgur.com/31jI0z3.png", "png");

if (!$EOTW::ReloadedPlayerData)
{
	$EOTW::ReloadedPlayerData = true;
	reloadEOTWPlayerData();
}

