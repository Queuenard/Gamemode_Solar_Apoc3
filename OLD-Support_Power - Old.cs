datablock StaticShapeData(EOTW_BrickTextEmptyShape)
{
        shapefile = "base/data/shapes/empty.dts";
};


function fxDtsBrick::SetBrickText(%this,%name,%color) //For displaying power n' stuff. Uses code from Event_BrickText
{
		%distance = 32;
		
        if(isObject(%this.textShape))
        {
                %this.textShape.setShapeName(%name);
                if (%color !$= "NONE") %this.textShape.setShapeNameColor(%color);
                %this.textShape.setShapeNameDistance(%distance);
                %this.bricktext = %name;
        }
        else
        {
                %this.textShape = new StaticShape()
                {
                        datablock = EOTW_BrickTextEmptyShape;
                        position = vectorAdd(%this.getPosition(),"0 0" SPC %this.getDatablock().brickSizeZ / 9 + "0.3"); //0.166
                        scale = "0.1 0.1 0.1";
                };
                %this.textShape.setShapeName(%name);
                if (%color !$= "NONE") %this.textShape.setShapeNameColor(%color);
                %this.textShape.setShapeNameDistance(%distance);
                %this.bricktext = %name;
        }
}

package EOTW_BrickText
{
        function fxDtsBrick::onDeath(%this)
        {
            if(isObject(%this.textShape))
                %this.textShape.delete();

            Parent::onDeath(%this);        
        }

        

        function fxDtsBrick::onRemove(%this)
        {
            if(isObject(%this.textShape))
                %this.textShape.delete();

            Parent::onRemove(%this);
        }
};
ActivatePackage(EOTW_BrickText);

function fxDtsBrick::UpdatePowerText(%this, %color)
{
	%this.SetBrickText("(" @ %this.getDatablock().uiName @ ") Power: " @ (%this.EOTW_Energy * 1) @ "/" @ (%this.EOTW_MaxEnergy * 1),%color);
}

//Solar Panel

datablock fxDTSBrickData(brickEOTWSolarPanelData)
{
	brickFile = "./Solar Panel.blb";
	category = "Solar Apoc";
	subCategory = "Power - Supplier";
	uiName = "Solar Panel";
	restrictMat = "Copper";
	isPowerSupplier = true;
	maxEnergy = 125;
	loopFunc = "EOTW_SolarPanelLoop";
	iconName = "Add-Ons/Gamemode_Solar_Apoc_Expanded/uiIcons/SolarPanel";
};

function fxDtsBrick::EOTW_SolarPanelLoop(%this)
{
	if (%this.EOTW_Energy $= "" || %this.EOTW_Energy < 0)
		%this.EOTW_Energy = 0;
		
	if ($EOTW::Time < 180)
	{
		%pi = 3.14159265;
		%val = ($EOTW::Time / 180) * %pi;
		%ang = ($EnvGuiServer::SunAzimuth / 180) * %pi;
		%dir = vectorScale(mSin(%ang) * mCos(%val) SPC mCos(%ang) * mCos(%val) SPC mSin(%val), 512);
		%ray = containerRaycast(vectorAdd(%pos = %this.getPosition(), %dir), %pos, $Typemasks::fxBrickObjectType | $Typemasks::StaticShapeObjectType);
		if(!isObject(%hit = firstWord(%ray)) || %hit == %this)
		{
			%this.UpdatePowerText("0 1 0");
			%this.charging = true;
			%hasPower = true;
			%this.EnergyInterval++;
			
			if (%this.EnergyInterval > 0 && %this.EOTW_Energy < %this.EOTW_MaxEnergy)
			{
				%this.EnergyInterval = 0;
				%this.EOTW_Energy++;
			}
		}
	}
	
	if (!%hasPower)
	{
		%this.charging = false;
		%this.UpdatePowerText("1 0 0");
		%this.EnergyInterval = 0;
	}
	
	%this.schedule(1000,"EOTW_SolarPanelLoop");
}

//Generator

datablock fxDTSBrickData(brickEOTWGeneratorData)
{
	brickFile = "./Generator.blb";
	category = "Solar Apoc";
	subCategory = "Power - Supplier";
	uiName = "Generator";
	restrictMat = "Lead";
	isPowerSupplier = true;
	maxEnergy = 65;
	loopFunc = "EOTW_GeneratorLoop";
	iconName = "Add-Ons/Gamemode_Solar_Apoc_Expanded/uiIcons/Generator";
};

function fxDtsBrick::EOTW_GeneratorLoop(%this)
{
	if (%this.EOTW_Energy $= "" || %this.EOTW_Energy < 0)
		%this.EOTW_Energy = 0;
		
	if (%this.EOTW_Energy < %this.EOTW_MaxEnergy)
		%this.EOTW_Energy++;
	
	%this.UpdatePowerText("1 1 1");
	
	%this.schedule(2250,"EOTW_GeneratorLoop");
}

//Generator

datablock fxDTSBrickData(brickEOTWBioreactorData)
{
	brickFile = "./Bioreactor.blb";
	category = "Solar Apoc";
	subCategory = "Power - Supplier";
	uiName = "Bioreactor";
	restrictMat = "Metal";
	isPowerSupplier = true;
	maxEnergy = 200;
	iconName = "Add-Ons/Gamemode_Solar_Apoc_Expanded/uiIcons/Bioreactor";
};

function brickEOTWBioreactorData::onPlant(%this,%b,%c,%d)
{
	parent::onPlant(%this,%b,%c,%d);
	
	if (%b.EOTW_Energy $= "" || %b.EOTW_Energy < 0)
		%b.EOTW_Energy = 0;
	
	%b.schedule(10,UpdatePowerText,"1 1 1");
}

function serverCmdAddPower(%cl,%amt,%mat)
{
	if (!isObject(%pl = %cl.player)) return;
	
	if (%amt $= "" || %mat $= "")
	{
		messageClient(%cl, '', "Usage: /addpower <amt> <leather/grass/vine>");
		return;
	}
	
	%mat2 = getProperMatName(%mat);
	%amt2 = mFloor(%amt);
	
	%eye = %pl.getEyePoint();
	%dir = %pl.getEyeVector();
	%for = %pl.getForwardVector();
	%face = getWords(vectorScale(getWords(%for, 0, 1), vectorLen(getWords(%dir, 0, 1))), 0, 1) SPC getWord(%dir, 2);
	%mask = $Typemasks::fxBrickAlwaysObjectType | $Typemasks::TerrainObjectType;
	%ray = containerRaycast(%eye, vectorAdd(%eye, vectorScale(%face, 5)), %mask, %pl);
	if(isObject(%hit = firstWord(%ray)) && %hit.getClassName() $= "fxDtsBrick" && %hit.getDatablock().getName() $= "brickEOTWBioreactorData")
	{
		if (%mat2 !$= "Leather" && %mat2 !$= "Grass" && %mat2 !$= "Vine")
		{
			%cl.centerPrint("\c0Whoops!<br>\c6The biorector only supports Leather, Grass, and Vine.",3);
			return;
		}
		else if (%amt2 < 1)
		{
			%cl.centerPrint("\c0Whoops!<br>\c6 You need to insert atleast 1 material.",3);
			return;
		}
		else if ($EOTW::Material[%cl.bl_id, %mat2] < %amt2)
		{
			%cl.centerPrint("\c0Whoops!<br>\c6 You can't add than more of what you have.<br>\c7(You have " @ ($EOTW::Material[%cl.bl_id, %mat2] + 0) @ " of the material you selected.)",3);
			return;
		}
		if (%mat2 $= "Grass") %mult = 3;
		else if (%mat2 $= "Leather") %mult = 2;
		else %mult = 1;
		
		%power = %amt2 * %mult;
		%overPower = ((%hit.EOTW_MaxEnergy - %hit.EOTW_Energy) - %power);
		
		if (%overPower < 0) %amt2 -= mCeil((-1 * %overPower) / %mult);
		
		%hit.EOTW_Energy += %power;
		if (%hit.EOTW_Energy > %hit.EOTW_MaxEnergy) %hit.EOTW_Energy = %hit.EOTW_MaxEnergy;
		
		$EOTW::Material[%cl.bl_id, %mat2] -= %amt2;
		
		messageClient(%cl, '', "\c6You inserted\c4 " @ %amt2 @ " \c2" @ %mat2 @ " \c6to the\c5 Bioreactor\c6.");
		
		%hit.UpdatePowerText("1 1 1");
	}
	else
	{
		%cl.centerPrint("\c0Whoops!<br>\c6You must stand close to a biorector and look at it to use this command.",3);
		return;
	}
}

//Crank-Powered Charger

datablock fxDTSBrickData(brickEOTWCrankData)
{
	brickFile = "./Battery.blb";
	category = "Solar Apoc";
	subCategory = "Power - Supplier";
	uiName = "Crank-Powered Charger";
	restrictMat = "Metal";
	isPowerSupplier = true;
	maxEnergy = 15;
	iconName = "Add-Ons/Gamemode_Solar_Apoc_Expanded/uiIcons/CrankMachine";
};

function brickEOTWCrankData::onPlant(%this,%b,%c,%d)
{
	parent::onPlant(%this,%b,%c,%d);
	
	if (%b.EOTW_Energy $= "" || %b.EOTW_Energy < 0)
		%b.EOTW_Energy = 0;
	
	%b.schedule(10,UpdatePowerText,"1 1 1");
	
	%b.energyInterval = 0;
}

//Battery

datablock fxDTSBrickData(brickEOTWBatteryData)
{
	brickFile = "./Battery.blb";
	category = "Solar Apoc";
	subCategory = "Power - Storage";
	uiName = "Power Cell";
	restrictMat = "Copper";
	maxEnergy = 250;
	isPowerStorage = true;
	loopFunc = "EOTW_BatteryLoop";
	iconName = "Add-Ons/Gamemode_Solar_Apoc_Expanded/uiIcons/PowerCell";
};

function fxDtsBrick::EOTW_BatteryLoop(%this)
{
	if (%this.EOTW_Energy $= "" || %this.EOTW_Energy < 0)
		%this.EOTW_Energy = 0;
		
	if (isObject(%this.getGroup().client))
	{
		initContainerRadiusSearch(%this.getPosition(), 32, $TypeMasks::FxBrickAlwaysObjectType);
		while(isObject(%obj = containerSearchNext()) && %this.EOTW_Energy < %this.EOTW_MaxEnergy)
		{
			if ((%this.getGroup().client == %obj.getGroup().client || %this.getGroup().client.getBL_IDTrustLevel(%obj.getGroup().bl_id) > 0) && (%obj.getDatablock().isPowerSupplier) && %obj.EOTW_Energy > 0)
			{
				%obj.EOTW_Energy--;
				%this.EOTW_Energy++;
				
				%obj.UpdatePowerText("NONE");
			}
		}
	}
	
	
	%this.UpdatePowerText("1 1 1");
	
	%this.schedule(2000,"EOTW_BatteryLoop");
}

//Ion Battery

datablock fxDTSBrickData(brickEOTWIonBatteryData)
{
	brickFile = "./IonBattery.blb";
	category = "Solar Apoc";
	subCategory = "Power - Storage";
	uiName = "Ion Battery";
	restrictMat = "Lead";
	maxEnergy = 750;
	isPowerStorage = true;
	loopFunc = "EOTW_IonBatteryLoop";
	iconName = "Add-Ons/Gamemode_Solar_Apoc_Expanded/uiIcons/IonBattery";
};

function fxDtsBrick::EOTW_IonBatteryLoop(%this)
{
	if (%this.EOTW_Energy $= "" || %this.EOTW_Energy < 0)
		%this.EOTW_Energy = 0;
		
	if (isObject(%this.getGroup().client))
	{
		initContainerRadiusSearch(%this.getPosition(), 32, $TypeMasks::FxBrickAlwaysObjectType);
		while(isObject(%obj = containerSearchNext()) && %this.EOTW_Energy < %this.EOTW_MaxEnergy)
		{
			if ((%this.getGroup().client == %obj.getGroup().client || %this.getGroup().client.getBL_IDTrustLevel(%obj.getGroup().bl_id) > 0) && (%obj.getDatablock().isPowerSupplier) && %obj.EOTW_Energy > 2)
			{
				%obj.EOTW_Energy -= 3;
				%this.EOTW_Energy += 3;
				
				%obj.UpdatePowerText("NONE");
			}
		}
	}
	
	
	%this.UpdatePowerText("1 1 1");
	
	%this.schedule(2000,"EOTW_IonBatteryLoop");
}

//Ore Grower

datablock fxDTSBrickData(brickEOTWOreGrowerData)
{
	brickFile = "./Ore Grower.blb";
	category = "Solar Apoc";
	subCategory = "Machinery";
	uiName = "Ore Grower";
	uncolorable = true;
	loopFunc = "EOTW_OreGrowerLoop";
	iconName = "Add-Ons/Gamemode_Solar_Apoc_Expanded/uiIcons/OreGrower";
};

function fxDtsBrick::EOTW_OreGrowerLoop(%this)
{
	if (isObject(%this.getGroup().client))
	{
		initContainerRadiusSearch(%this.getPosition(), 32, $TypeMasks::FxBrickAlwaysObjectType);
		while(isObject(%obj = containerSearchNext()))
		{
			if ((%this.getGroup().client == %obj.getGroup().client || %this.getGroup().client.getBL_IDTrustLevel(%obj.getGroup().bl_id) > 0) && %obj.getDatablock().isPowerStorage && %obj.EOTW_Energy > 0)
			{
				%obj.EOTW_Energy--;
				%obj.UpdatePowerText("NONE");
				
				%this.SetBrickText("(Ore Grower) Running","0 1 0");
				%foundPower = true;
				
				%this.EnergyInterval++;
				
				if (%this.EnergyInterval >= 12)
				{
					%data = EOTW_CreateBrick(%this.getGroup(), "brick1x1fdata", vectorAdd(%this.getPosition(), (getRandom(-4, 3) / 2) SPC (getRandom(-4, 3) / 2)  SPC "0.2"), %this.getColorID());
					
					%brick = getField(%data, 0);
					
					if(getField(%data, 1))
						%brick.delete();
						
					if (isObject(%brick))
					{
						%brick.material = %this.material;
						%brick.fromOreGrower = true;
						%brick.dontRefund = true;
					}
					%this.EnergyInterval = 0;
				}
			}
		}
	}
	
	
	if (!%foundPower)
	{
		
		%this.SetBrickText("(Ore Grower) No Power","1 0 0");
		%this.EnergyInterval = 0;
	}
	
	%this.schedule(2000,"EOTW_OreGrowerLoop");
}

//Super Light Charger

datablock fxDTSBrickData(brickEOTWSLCData)
{
	brickFile = "./Super Light Charger.blb";
	category = "Solar Apoc";
	subCategory = "Machinery";
	uiName = "Super Light Charger";
	restrictMat = "Lead";
	loopFunc = "EOTW_SLCLoop";
	iconName = "Add-Ons/Gamemode_Solar_Apoc_Expanded/uiIcons/SLCCharger";
};

function fxDtsBrick::EOTW_SLCLoop(%this)
{
	if (isObject(%this.getGroup().client))
	{
		initContainerRadiusSearch(%this.getPosition(), 32, $TypeMasks::FxBrickAlwaysObjectType);
		while(isObject(%obj = containerSearchNext()))
		{
			if ((%this.getGroup().client == %obj.getGroup().client || %this.getGroup().client.getBL_IDTrustLevel(%obj.getGroup().bl_id) > 0) && %obj.getDatablock().isPowerStorage && %obj.EOTW_Energy > 0)
			{
				%obj.UpdatePowerText("NONE");
				
				%this.SetBrickText("(S. L. C.) Running - Stand on to Charge.","0 1 0");
				%foundPower = true;
				
				if (%this.EnergyInterval > 1)
				{
					%eye = %this.GetPosition();
					%dir = "0 0 1";
					%for = "0 1 0";
					%face = getWords(vectorScale(getWords(%for, 0, 1), vectorLen(getWords(%dir, 0, 1))), 0, 1) SPC getWord(%dir, 2);
					%mask = $Typemasks::fxBrickAlwaysObjectType | $Typemasks::TerrainObjectType | $Typemasks::PlayerObjectType;
					%ray = containerRaycast(%eye, vectorAdd(%eye, vectorScale(%face, 2)), %mask, %this);
					
					if (isObject(%hit = firstWord(%ray)) && %hit.getClassName() $= "Player" && !isObject(%hit.light))
					{
						if (%hit.SuperLightEnergy < 40)
						{
							%hit.SuperLightEnergy += 4;
							
							if (%hit.SuperLightEnergy > 40)
								%hit.SuperLightEnergy = 40;
								
							%this.EnergyInterval = 0;
						}
						
						%hit.client.centerPrint("\c6Super Light Charge: \c4" @ %hit.SuperLightEnergy @ "/40<br>Type /sl to turn on your super light.",4);
					}
					else if (isObject(%hit.light))
						%hit.client.centerPrint("\c6Turn off your light before charging.",4);
				}
				else
				{
					%obj.EOTW_Energy--;
					%obj.UpdatePowerText("NONE");
					%this.EnergyInterval++;
				}
			}
		}
	}
	
	
	if (!%foundPower)
	{
		
		%this.SetBrickText("(S. L. C.) No Power","1 0 0");
		%this.EnergyInterval = 0;
	}
	
	%this.schedule(500,"EOTW_SLCLoop");
}

//Monster Repellant

datablock fxDTSBrickData(brickEOTWRepellantData)
{
	brickFile = "./IonBattery.blb";
	category = "Solar Apoc";
	subCategory = "Machinery";
	uiName = "Monster Repellant";
	restrictMat = "Wolfram";
	loopFunc = "EOTW_RepellantLoop";
	iconName = "Add-Ons/Gamemode_Solar_Apoc_Expanded/uiIcons/MonsterRepel";
};

function fxDtsBrick::EOTW_RepellantLoop(%this)
{
	if (isObject(%this.getGroup().client))
	{
		initContainerRadiusSearch(%this.getPosition(), 32, $TypeMasks::FxBrickAlwaysObjectType);
		while(isObject(%obj = containerSearchNext()))
		{
			if ((%this.getGroup().client == %obj.getGroup().client || %this.getGroup().client.getBL_IDTrustLevel(%obj.getGroup().bl_id) > 0) && %obj.getDatablock().isPowerStorage && %obj.EOTW_Energy > 0)
			{
				%obj.EOTW_Energy--;
				%obj.UpdatePowerText("NONE");
				
				%this.SetBrickText("(Repellant) Running","0 1 0");
				%foundPower = true;
				break;
			}
		}
	}
	
	
	if (!%foundPower)
	{
		
		%this.SetBrickText("(Repellant) No Power","1 0 0");
		%this.EnergyInterval = 0;
	}
	else
	{
		initContainerRadiusSearch(%this.getPosition(), 32, $TypeMasks::PlayerObjectType);
		while(isObject(%pl = containerSearchNext()))
		{
			%pl.lastRepellant = getSimTime();
		}
	}
	
	%this.schedule(1000,"EOTW_RepellantLoop");
}

//Heal Pulser

datablock fxDTSBrickData(brickEOTWHealPulserData)
{
	brickFile = "./IonBattery.blb";
	category = "Solar Apoc";
	subCategory = "Machinery";
	uiName = "Heal Pulser";
	restrictMat = "Sturdium";
	loopFunc = "EOTW_HealPulserLoop";
	iconName = "Add-Ons/Gamemode_Solar_Apoc_Expanded/uiIcons/HealPulser";
};

function fxDtsBrick::EOTW_HealPulserLoop(%this)
{
	if (isObject(%this.getGroup().client))
	{
		initContainerRadiusSearch(%this.getPosition(), 32, $TypeMasks::FxBrickAlwaysObjectType);
	
		%amt = -1;
		
		while(isObject(%obj = containerSearchNext()))
		{
			%bricks[%amt++] = %obj;
		}
		
		for (%i=0;%i<%amt;%i++)
		{
			%brick = %bricks[%i];
			
			if (%this.getGroup().bl_id == %brick.getGroup().bl_id && %brick.getDatablock().isPowerStorage && %brick.EOTW_Energy > 0)
			{
				%brick.EOTW_Energy--;
				%brick.UpdatePowerText("NONE");
				
				%this.SetBrickText("(Heal Pulser) Running","0 1 0");
				%foundPower = true;
				
				%this.EnergyInterval++;
				
				if (%this.EnergyInterval > 4)
				{
					initContainerRadiusSearch(%this.getPosition(), 32, $TypeMasks::PlayerObjectType);
					while(isObject(%pl = containerSearchNext()))
					{
						%pl.addHealth(2);
					}
					%this.EnergyInterval = 0;
				}
			}
		}
	}
	
	
	if (!%foundPower)
	{
		
		%this.SetBrickText("(Heal Pulser) No Power","1 0 0");
		%this.EnergyInterval = 0;
	}
	
	%this.schedule(1000,"EOTW_HealPulserLoop");
}

//Monster Repellant

datablock fxDTSBrickData(brickEOTWAltarData)
{
	brickFile = "./IonBattery.blb";
	category = "Solar Apoc";
	subCategory = "Machinery";
	uiName = "Altar";
	restrictMat = "Metal";
	loopFunc = "EOTW_AltarLoop";
	iconName = "Add-Ons/Gamemode_Solar_Apoc_Expanded/uiIcons/Altar";
};

function fxDtsBrick::EOTW_AltarLoop(%this)
{
	if (isObject(%this.getGroup().client))
	{
		initContainerRadiusSearch(%this.getPosition(), 32, $TypeMasks::FxBrickAlwaysObjectType);
		while(isObject(%obj = containerSearchNext()))
		{
			if ((%this.getGroup().client == %obj.getGroup().client || %this.getGroup().client.getBL_IDTrustLevel(%obj.getGroup().bl_id) > 0) && %obj.getDatablock().isPowerStorage && %obj.EOTW_Energy > 0)
			{
				%obj.EOTW_Energy--;
				%obj.UpdatePowerText("NONE");
				
				%this.SetBrickText("(Altar) Running","0 1 0");
				%this.hasPower = true;
				%foundPower = true;
				break;
			}
		}
	}
	
	if (!%foundPower)
	{
		%this.hasPower = false;
		%this.SetBrickText("(Altar) No Power","1 0 0");
	}
	
	%this.schedule(1000,"EOTW_AltarLoop");
}