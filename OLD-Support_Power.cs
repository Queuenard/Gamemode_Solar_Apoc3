datablock StaticShapeData(EOTW_BrickTextEmptyShape)
{
        shapefile = "base/data/shapes/empty.dts";
};


function fxDtsBrick::SetBrickText(%this,%name,%color) //For displaying power n' stuff. Uses code from Event_BrickText
{
	%this.lastBrickText = %name;
	if (%color !$= "NONE")
		%this.lastBrickColor = %color;
	
	return;
	
	%distance = 32;
		
	if(isObject(%this.textShape))
	{
			%this.textShape.setShapeName(%name);
			if (%color !$= "NONE") %this.textShape.setShapeNameColor(%color);
			%this.textShape.setShapeNameDistance(%distance);
			%this.bricktext = %name;
	}
	else
	{
			%this.textShape = new StaticShape()
			{
					datablock = EOTW_BrickTextEmptyShape;
					position = vectorAdd(%this.getPosition(),"0 0" SPC %this.getDatablock().brickSizeZ / 9 + "0.3"); //0.166
					scale = "0.1 0.1 0.1";
			};
			%this.textShape.setShapeName(%name);
			if (%color !$= "NONE") %this.textShape.setShapeNameColor(%color);
			%this.textShape.setShapeNameDistance(%distance);
			%this.bricktext = %name;
	}
}

package EOTW_BrickText
{
        function fxDtsBrick::onDeath(%this)
        {
            if(isObject(%this.textShape))
                %this.textShape.delete();

            Parent::onDeath(%this);        
        }

        

        function fxDtsBrick::onRemove(%this)
        {
            if(isObject(%this.textShape))
                %this.textShape.delete();

            Parent::onRemove(%this);
        }
		
		function fxDtsBrick::onAdd(%this, %obj)
		{
			Parent::onAdd(%this, %obj);
		}
		
		// function fxDTSBrick::onPlant(%data,%obj)
		// {
			// // %db = %data.getDatablock();
			// // if (%db.loopFunc !$= "" && !%data.startedMachine)
			// // {
				// // %data.schedule(getRandom(500,999), "StartMachineLoop");
			// // }
			// %obj.StartMachineLoop();
			
			// return Parent::onPlant(%data,%obj);
		// }
		
		// function fxDTSBrick::onLoadPlant(%data, %obj)
		// {
			// // %db = %data.getDatablock();
			// // if (%db.loopFunc !$= "" && !%data.startedMachine)
			// // {
				// // %data.schedule(getRandom(500,999), "StartMachineLoop");
			// // }
			// %obj.StartMachineLoop();
			
			// return Parent::onLoadPlant(%data, %obj);
		// }
};
ActivatePackage(EOTW_BrickText);

function fxDTSBrick::StartMachineLoop(%this)
{
	%db = %this.getDatablock();
	if (%db.loopFunc !$= "" && !%this.startedMachine)
	{
		%this.startedMachine = true;
		%this.EOTW_MaxEnergy = %db.MaxEnergy;
		
		// if (%db.loopFunc $= "EOTW_SolarPanelLoop") %this.EOTW_SolarPanelLoop();
		// else if (%db.loopFunc $= "EOTW_GeneratorLoop") %this.EOTW_GeneratorLoop();
		// else if (%db.loopFunc $= "EOTW_BatteryLoop") %this.EOTW_BatteryLoop();
		// else if (%db.loopFunc $= "EOTW_IonBatteryLoop") %this.EOTW_IonBatteryLoop();
		// else if (%db.loopFunc $= "EOTW_OreGrowerLoop") %this.EOTW_OreGrowerLoop();
		// else if (%db.loopFunc $= "EOTW_SLCLoop") %this.EOTW_SLCLoop();
		// else if (%db.loopFunc $= "EOTW_RepellantLoop") %this.EOTW_RepellantLoop();
		// else if (%db.loopFunc $= "EOTW_HealPulserLoop") %this.EOTW_HealPulserLoop();
		// else if (%db.loopFunc $= "EOTW_AltarLoop") %this.EOTW_AltarLoop();
	}
}

function fxDtsBrick::UpdatePowerText(%this, %color)
{
	%this.SetBrickText("(" @ %this.getDatablock().uiName @ ") Power: " @ (%this.EOTW_Energy * 1) @ "/" @ (%this.EOTW_MaxEnergy * 1),%color);
}

//Solar Panel

datablock fxDTSBrickData(brickEOTWSolarPanelData)
{
	brickFile = "./bricks/Solar Panel.blb";
	category = "Solar Apoc";
	subCategory = "Power - Supplier";
	uiName = "Solar Panel";
	restrictMat = "Copper";
	isPowerSupplier = true;
	powerCooldown = 1000;
	maxEnergy = 125;
	loopFunc = "EOTW_SolarPanelLoop";
	iconName = "Add-Ons/Gamemode_Solar_Apoc3/uiIcons/SolarPanel";
};

function fxDtsBrick::EOTW_SolarPanelLoop(%this)
{
	if (%this.EOTW_Energy $= "" || %this.EOTW_Energy < 0)
		%this.EOTW_Energy = 0;
		
	// if ($EOTW::Time < 180)
	// {
		// %pi = 3.14159265;
		// %val = ($EOTW::Time / 180) * %pi;
		// %ang = ($EnvGuiServer::SunAzimuth / 180) * %pi;
		// %dir = vectorScale(mSin(%ang) * mCos(%val) SPC mCos(%ang) * mCos(%val) SPC mSin(%val), 512);
		// %ray = containerRaycast(vectorAdd(%pos = %this.getPosition(), %dir), %pos, $Typemasks::fxBrickObjectType | $Typemasks::StaticShapeObjectType);
		// if(!isObject(%hit = firstWord(%ray)) || %hit == %this)
		// {
			// %this.UpdatePowerText("0 1 0");
			// %this.charging = true;
			// %hasPower = true;
			// %this.EnergyInterval++;
			
			// if (%this.EnergyInterval > 0 && %this.EOTW_Energy < %this.EOTW_MaxEnergy)
			// {
				// %this.EnergyInterval = 0;
				// %this.EOTW_Energy++;
			// }
		// }
	// }
	
	if ($EOTW::Time < 180)
	{
		if (%this.EOTW_Energy < %this.EOTW_MaxEnergy)
		{
			%this.EOTW_Energy++;
		}
	}
	
	if (!%hasPower)
	{
		%this.charging = false;
		%this.UpdatePowerText("1 0 0");
		%this.EnergyInterval = 0;
	}
}

//Generator

datablock fxDTSBrickData(brickEOTWGeneratorData)
{
	brickFile = "./bricks/Generator.blb";
	category = "Solar Apoc";
	subCategory = "Power - Supplier";
	uiName = "Generator";
	restrictMat = "Lead";
	isPowerSupplier = true;
	powerCooldown = 2250;
	maxEnergy = 65;
	loopFunc = "EOTW_GeneratorLoop";
	iconName = "Add-Ons/Gamemode_Solar_Apoc3/uiIcons/Generator";
};

function fxDtsBrick::EOTW_GeneratorLoop(%this)
{
	if (%this.EOTW_Energy $= "" || %this.EOTW_Energy < 0)
		%this.EOTW_Energy = 0;
		
	if (%this.EOTW_Energy < %this.EOTW_MaxEnergy)
		%this.EOTW_Energy += 2;
	
	%this.UpdatePowerText("1 1 1");
}

//Sturdy Generator

datablock fxDTSBrickData(brickEOTWSturdyGeneratorData)
{
	brickFile = "./bricks/Generator.blb";
	category = "Solar Apoc";
	subCategory = "Power - Supplier";
	uiName = "Generator";
	restrictMat = "Sturdium";
	isPowerSupplier = true;
	powerCooldown = 333;
	maxEnergy = 200;
	loopFunc = "EOTW_SturdyGeneratorLoop";
	iconName = "Add-Ons/Gamemode_Solar_Apoc3/uiIcons/SturdyGen";
};

function fxDtsBrick::EOTW_SturdyGeneratorLoop(%this)
{
	if (%this.EOTW_Energy $= "" || %this.EOTW_Energy < 0)
		%this.EOTW_Energy = 0;
		
	if (%this.EOTW_Energy < %this.EOTW_MaxEnergy)
		%this.EOTW_Energy += 2;
	
	%this.UpdatePowerText("1 1 1");
}


//Generator

datablock fxDTSBrickData(brickEOTWBioreactorData)
{
	brickFile = "./bricks/Bioreactor.blb";
	category = "Solar Apoc";
	subCategory = "Power - Supplier";
	uiName = "Bioreactor";
	restrictMat = "Metal";
	isPowerSupplier = true;
	maxEnergy = 200;
	iconName = "Add-Ons/Gamemode_Solar_Apoc3/uiIcons/Bioreactor";
};

function brickEOTWBioreactorData::onPlant(%this,%b,%c,%d)
{
	parent::onPlant(%this,%b,%c,%d);
	
	%b.EOTW_MaxEnergy = %b.getDatablock().MaxEnergy;
	
	if (%b.EOTW_Energy $= "" || %b.EOTW_Energy < 0)
		%b.EOTW_Energy = 0;
	
	%b.schedule(10,UpdatePowerText,"1 1 1");
}

function brickEOTWBioreactorData::onLoadPlant(%this,%b,%c,%d)
{
	parent::onLoadPlant(%this,%b,%c,%d);
	
	if (%b.EOTW_Energy $= "" || %b.EOTW_Energy < 0)
		%b.EOTW_Energy = 0;
	
	%b.schedule(10,UpdatePowerText,"1 1 1");
}

function serverCmdAddPower(%cl,%amt,%mat)
{
	if (!isObject(%pl = %cl.player)) return;
	
	if (%amt $= "" || %mat $= "")
	{
		messageClient(%cl, '', "Usage: /addpower <amt> <grass/vine>");
		return;
	}
	
	%mat2 = getProperMatName(%mat);
	%amt2 = mFloor(%amt);
	
	%eye = %pl.getEyePoint();
	%dir = %pl.getEyeVector();
	%for = %pl.getForwardVector();
	%face = getWords(vectorScale(getWords(%for, 0, 1), vectorLen(getWords(%dir, 0, 1))), 0, 1) SPC getWord(%dir, 2);
	%mask = $Typemasks::fxBrickAlwaysObjectType | $Typemasks::TerrainObjectType;
	%ray = containerRaycast(%eye, vectorAdd(%eye, vectorScale(%face, 5)), %mask, %pl);
	if(isObject(%hit = firstWord(%ray)) && %hit.getClassName() $= "fxDtsBrick" && %hit.getDatablock().getName() $= "brickEOTWBioreactorData")
	{
		if (%mat2 !$= "Grass" && %mat2 !$= "Vine")
		{
			%cl.centerPrint("\c0Whoops!<br>\c6The biorector only supports Grass and Vine.",3);
			return;
		}
		else if (%amt2 < 1)
		{
			%cl.centerPrint("\c0Whoops!<br>\c6 You need to insert atleast 1 material.",3);
			return;
		}
		else if ($EOTW::Material[%cl.bl_id, %mat2] < %amt2)
		{
			%cl.centerPrint("\c0Whoops!<br>\c6 You can't add than more of what you have.<br>\c7(You have " @ ($EOTW::Material[%cl.bl_id, %mat2] + 0) @ " of the material you selected.)",3);
			return;
		}
		if (%mat2 $= "Grass") %mult = 3;
		else %mult = 1;
		
		%power = %amt2 * %mult;
		%overPower = ((%hit.EOTW_MaxEnergy - %hit.EOTW_Energy) - %power);
		
		if (%overPower < 0) %amt2 -= mCeil((-1 * %overPower) / %mult);
		
		%hit.EOTW_Energy += %power;
		if (%hit.EOTW_Energy > %hit.EOTW_MaxEnergy) %hit.EOTW_Energy = %hit.EOTW_MaxEnergy;
		
		$EOTW::Material[%cl.bl_id, %mat2] -= %amt2;
		
		messageClient(%cl, '', "\c6You inserted\c4 " @ %amt2 @ " \c2" @ %mat2 @ " \c6to the\c5 Bioreactor\c6.");
		
		%hit.UpdatePowerText("1 1 1");
	}
	else
	{
		%cl.centerPrint("\c0Whoops!<br>\c6You must stand close to a biorector and look at it to use this command.",3);
		return;
	}
}

//Crank-Powered Charger

datablock fxDTSBrickData(brickEOTWCrankData)
{
	brickFile = "./bricks/Battery.blb";
	category = "Solar Apoc";
	subCategory = "Power - Supplier";
	uiName = "Crank-Powered Charger";
	restrictMat = "Metal";
	isPowerSupplier = true;
	maxEnergy = 15;
	iconName = "Add-Ons/Gamemode_Solar_Apoc3/uiIcons/CrankMachine";
};

function brickEOTWCrankData::onPlant(%this,%b,%c,%d)
{
	%b.EOTW_MaxEnergy = %b.getDatablock().MaxEnergy;
	
	if (%b.EOTW_Energy $= "" || %b.EOTW_Energy < 0)
		%b.EOTW_Energy = 0;
	
	%b.schedule(10,UpdatePowerText,"1 1 1");
	
	%b.energyInterval = 0;
	
	
	parent::onPlant(%this,%b,%c,%d);
}

//Battery

datablock fxDTSBrickData(brickEOTWBatteryData)
{
	brickFile = "./bricks/Battery.blb";
	category = "Solar Apoc";
	subCategory = "Power - Storage";
	uiName = "Power Cell";
	restrictMat = "Copper";
	maxEnergy = 250;
	isPowerStorage = true;
	powerCooldown = 2000;
	loopFunc = "EOTW_BatteryLoop";
	iconName = "Add-Ons/Gamemode_Solar_Apoc3/uiIcons/PowerCell";
};

function fxDtsBrick::EOTW_BatteryLoop(%this)
{
	if (%this.EOTW_Energy $= "" || %this.EOTW_Energy < 0)
		%this.EOTW_Energy = 0;
		
	if (isObject(%this.getGroup().client) && isObject(%this.EOTW_PowerSources) && %this.EOTW_Energy < %this.EOTW_MaxEnergy)
	{
		for (%i = 0; %i < %this.EOTW_PowerSources.getCount(); %i++)
		{
			if (!isObject(%obj = %this.EOTW_PowerSources.getObject(%i)))
				break;
				
			if (%obj.EOTW_Energy <= 0) continue;
			
			if ((%this.getGroup().client == %obj.getGroup().client || %this.getGroup().client.getBL_IDTrustLevel(%obj.getGroup().bl_id) > 0) && (%obj.getDatablock().isPowerSupplier) && %obj.EOTW_Energy > 0)
			{
				%obj.EOTW_Energy--;
				%this.EOTW_Energy++;
				
				if (%this.EOTW_Energy > %this.EOTW_MaxEnergy)
				{
					%this.EOTW_Energy = %this.EOTW_MaxEnergy;
					%obj.UpdatePowerText("NONE");
					break;
				}
				
				%obj.UpdatePowerText("NONE");
			}
		}
	}
	
	%this.UpdatePowerText("1 1 1");
}

function brickEOTWBatteryData::onPlant(%this,%obj)
{
	%obj.EOTW_SetupPowerGroup();
	
	parent::onPlant(%this,%obj);
}

//Ion Battery

datablock fxDTSBrickData(brickEOTWIonBatteryData)
{
	brickFile = "./bricks/IonBattery.blb";
	category = "Solar Apoc";
	subCategory = "Power - Storage";
	uiName = "Ion Battery";
	restrictMat = "Lead";
	maxEnergy = 750;
	isPowerStorage = true;
	powerCooldown = 2000;
	loopFunc = "EOTW_IonBatteryLoop";
	iconName = "Add-Ons/Gamemode_Solar_Apoc3/uiIcons/IonBattery";
};

function fxDtsBrick::EOTW_IonBatteryLoop(%this)
{
	if (%this.EOTW_Energy $= "" || %this.EOTW_Energy < 0)
		%this.EOTW_Energy = 0;
		
	if (isObject(%this.EOTW_PowerSources) && %this.EOTW_Energy < %this.EOTW_MaxEnergy)
	{
		for (%i = 0; %i < %this.EOTW_PowerSources.getCount(); %i++)
		{
			if (!isObject(%obj = %this.EOTW_PowerSources.getObject(%i)))
				break;
				
			if (%obj.EOTW_Energy <= 0) continue;
			
			if ((%this.getGroup().client == %obj.getGroup().client || %this.getGroup().client.getBL_IDTrustLevel(%obj.getGroup().bl_id) > 0) && (%obj.getDatablock().isPowerSupplier) && %obj.EOTW_Energy > 2)
			{
				%obj.EOTW_Energy -= 3;
				%this.EOTW_Energy += 3;
				
				if (%this.EOTW_Energy > %this.EOTW_MaxEnergy)
				{
					%this.EOTW_Energy = %this.EOTW_MaxEnergy;
					%obj.UpdatePowerText("NONE");
					break;
				}
				
				%obj.UpdatePowerText("NONE");
			}
		}
	}
	
	%this.UpdatePowerText("1 1 1");
}

//Ion Battery

datablock fxDTSBrickData(brickEOTWSturdyBatteryData)
{
	brickFile = "./bricks/IonBattery.blb";
	category = "Solar Apoc";
	subCategory = "Power - Storage";
	uiName = "Sturdy Battery";
	restrictMat = "Sturdium";
	maxEnergy = 5000;
	isPowerStorage = true;
	powerCooldown = 250;
	loopFunc = "EOTW_SturdyBatteryLoop";
	iconName = "Add-Ons/Gamemode_Solar_Apoc3/uiIcons/SturdyBat";
};

function fxDtsBrick::EOTW_SturdyBatteryLoop(%this)
{
	if (%this.EOTW_Energy $= "" || %this.EOTW_Energy < 0)
		%this.EOTW_Energy = 0;
		
	if (isObject(%this.EOTW_PowerSources) && %this.EOTW_Energy < %this.EOTW_MaxEnergy)
	{
		for (%i = 0; %i < %this.EOTW_PowerSources.getCount(); %i++)
		{
			if (!isObject(%obj = %this.EOTW_PowerSources.getObject(%i)))
				break;
				
			if (%obj.EOTW_Energy <= 0) continue;
			
			if ((%this.getGroup().client == %obj.getGroup().client || %this.getGroup().client.getBL_IDTrustLevel(%obj.getGroup().bl_id) > 0) && (%obj.getDatablock().isPowerSupplier) && %obj.EOTW_Energy > 2)
			{
				%obj.EOTW_Energy -= 2;
				%this.EOTW_Energy += 2;
				
				if (%this.EOTW_Energy > %this.EOTW_MaxEnergy)
				{
					%this.EOTW_Energy = %this.EOTW_MaxEnergy;
					%obj.UpdatePowerText("NONE");
					break;
				}
				
				%obj.UpdatePowerText("NONE");
			}
		}
	}
	
	%this.UpdatePowerText("1 1 1");
}

//Ore Grower

datablock fxDTSBrickData(brickEOTWOreGrowerData)
{
	brickFile = "./bricks/Ore Grower.blb";
	category = "Solar Apoc";
	subCategory = "Machinery";
	uiName = "Ore Grower";
	uncolorable = true;
	isPowerConsumer = true;
	powerCooldown = 1000;
	loopFunc = "EOTW_OreGrowerLoop";
	iconName = "Add-Ons/Gamemode_Solar_Apoc3/uiIcons/OreGrower";
};

function fxDtsBrick::EOTW_OreGrowerLoop(%this)
{
	if (isObject(%this.getGroup().client) && isObject(%this.EOTW_PowerSources))
	{
		for (%i = 0; %i < %this.EOTW_PowerSources.getCount(); %i++)
		{
			if (!isObject(%obj = %this.EOTW_PowerSources.getObject(%i)))
				break;
			
			//talk((%this.getGroup().client == %obj.getGroup().client) SPC (%this.getGroup().client.getBL_IDTrustLevel(%obj.getGroup().bl_id) > 0) SPC (%obj.getDatablock().isPowerStorage && %obj.EOTW_Energy > 0));
			if ((%this.getGroup().client == %obj.getGroup().client || %this.getGroup().client.getBL_IDTrustLevel(%obj.getGroup().bl_id) > 0) && %obj.getDatablock().isPowerStorage && %obj.EOTW_Energy > 0)
			{
				%obj.EOTW_Energy--;
				%obj.UpdatePowerText("NONE");
				
				%this.SetBrickText("(Ore Grower) Running","0 1 0");
				%this.EOTW_EnergyRunning = true;
				%foundPower = true;
				
				%this.EnergyInterval++;
				
				if (%this.EnergyInterval > 4)
				{
					%data = EOTW_CreateBrick(%this.getGroup(), "brick1x1fdata", vectorAdd(%this.getPosition(), (getRandom(-4, 3) / 2) SPC (getRandom(-4, 3) / 2)  SPC "0.2"), %this.getColorID());
					
					%brick = getField(%data, 0);
					
					if(getField(%data, 1))
						%brick.delete();
						
					if (isObject(%brick))
					{
						%brick.material = %this.material;
						%brick.fromOreGrower = true;
						%brick.dontRefund = true;
					}
					%this.EnergyInterval = 0;
					
					break;
				}
			}
		}
	}
	
	
	if (!%foundPower)
	{
		
		%this.SetBrickText("(Ore Grower) No Power","1 0 0");
		%this.EOTW_EnergyRunning = false;
		%this.EnergyInterval = 0;
	}
}

//Super Light Charger

datablock fxDTSBrickData(brickEOTWSLCData)
{
	brickFile = "./bricks/Super Light Charger.blb";
	category = "Solar Apoc";
	subCategory = "Machinery";
	uiName = "Super Light Charger";
	restrictMat = "Lead";
	isPowerConsumer = true;
	powerCooldown = 500;
	loopFunc = "EOTW_SLCLoop";
	iconName = "Add-Ons/Gamemode_Solar_Apoc3/uiIcons/SLCCharger";
};

function fxDtsBrick::EOTW_SLCLoop(%this)
{
	if (isObject(%this.getGroup().client) && isObject(%this.EOTW_PowerSources))
	{
		for (%i = 0; %i < %this.EOTW_PowerSources.getCount(); %i++)
		{
			if (!isObject(%obj = %this.EOTW_PowerSources.getObject(%i)))
				break;
				
			if ((%this.getGroup().client == %obj.getGroup().client || %this.getGroup().client.getBL_IDTrustLevel(%obj.getGroup().bl_id) > 0) && %obj.getDatablock().isPowerStorage && %obj.EOTW_Energy > 0)
			{
				%obj.UpdatePowerText("NONE");
				
				%this.SetBrickText("(S. L. C.) Running - Stand on to Charge.","0 1 0");
				%this.EOTW_EnergyRunning = true;
				%foundPower = true;
				
				%maxCharge = 300;
				
				if (%this.EnergyInterval > 1)
				{
					%eye = %this.GetPosition();
					%dir = "0 0 1";
					%for = "0 1 0";
					%face = getWords(vectorScale(getWords(%for, 0, 1), vectorLen(getWords(%dir, 0, 1))), 0, 1) SPC getWord(%dir, 2);
					%mask = $Typemasks::fxBrickAlwaysObjectType | $Typemasks::TerrainObjectType | $Typemasks::PlayerObjectType;
					%ray = containerRaycast(%eye, vectorAdd(%eye, vectorScale(%face, 2)), %mask, %this);
					
					if (isObject(%hit = firstWord(%ray)) && %hit.getClassName() $= "Player" && !isObject(%hit.light))
					{
						if (%hit.SuperLightEnergy < %maxCharge)
						{
							%hit.SuperLightEnergy += mFloor(%maxCharge / 10);
							
							if (%hit.SuperLightEnergy > %maxCharge)
								%hit.SuperLightEnergy = %maxCharge;
								
							%this.EnergyInterval = 0;
						}
						
						%hit.client.centerPrint("\c6Super Light Charge: \c4" @ %hit.SuperLightEnergy @ "/" @ %maxCharge @ "<br>Type /sl to turn on your super light.",4);
					}
					else if (isObject(%hit.light))
						%hit.client.centerPrint("\c6Turn off your light before charging.",4);
				}
				else
				{
					%obj.EOTW_Energy--;
					%obj.UpdatePowerText("NONE");
					%this.EnergyInterval++;
				}
			}
		}
	}
	
	
	if (!%foundPower)
	{
		
		%this.SetBrickText("(S. L. C.) No Power","1 0 0");
		%this.EOTW_EnergyRunning = false;
		%this.EnergyInterval = 0;
	}
}

//Monster Repellant

datablock fxDTSBrickData(brickEOTWRepellantData)
{
	brickFile = "./bricks/IonBattery.blb";
	category = "Solar Apoc";
	subCategory = "Machinery";
	uiName = "Monster Repellant";
	restrictMat = "Wolfram";
	isPowerConsumer = true;
	powerCooldown = 1000;
	loopFunc = "EOTW_RepellantLoop";
	iconName = "Add-Ons/Gamemode_Solar_Apoc3/uiIcons/MonsterRepel";
};

function fxDtsBrick::EOTW_RepellantLoop(%this)
{
	if (isObject(%this.getGroup().client) && isObject(%this.EOTW_PowerSources))
	{
		for (%i = 0; %i < %this.EOTW_PowerSources.getCount(); %i++)
		{
			if (!isObject(%obj = %this.EOTW_PowerSources.getObject(%i)))
				break;
				
			if ((%this.getGroup().client == %obj.getGroup().client || %this.getGroup().client.getBL_IDTrustLevel(%obj.getGroup().bl_id) > 0) && %obj.getDatablock().isPowerStorage && %obj.EOTW_Energy > 0)
			{
				%obj.EOTW_Energy--;
				%obj.UpdatePowerText("NONE");
				
				%this.SetBrickText("(Repellant) Running","0 1 0");
				%this.EOTW_EnergyRunning = true;
				%foundPower = true;
				break;
			}
		}
	}
	
	
	if (!%foundPower)
	{
		
		%this.SetBrickText("(Repellant) No Power","1 0 0");
		%this.EOTW_EnergyRunning = false;
		%this.EnergyInterval = 0;
	}
	else
	{
		initContainerRadiusSearch(%this.getPosition(), 32, $TypeMasks::PlayerObjectType);
		while(isObject(%pl = containerSearchNext()))
		{
			%pl.lastRepellant = getSimTime();
		}
	}
}

//Heal Pulser

datablock fxDTSBrickData(brickEOTWHealPulserData)
{
	brickFile = "./bricks/IonBattery.blb";
	category = "Solar Apoc";
	subCategory = "Machinery";
	uiName = "Heal Pulser";
	restrictMat = "Sturdium";
	isPowerConsumer = true;
	powerCooldown = 1000;
	loopFunc = "EOTW_HealPulserLoop";
	iconName = "Add-Ons/Gamemode_Solar_Apoc3/uiIcons/HealPulser";
};

function fxDtsBrick::EOTW_HealPulserLoop(%this)
{
	if (isObject(%this.getGroup().client) && isObject(%this.EOTW_PowerSources))
	{
		for (%i = 0; %i < %this.EOTW_PowerSources.getCount(); %i++)
		{
			if (!isObject(%obj = %this.EOTW_PowerSources.getObject(%i)))
				break;
				
			if (%this.getGroup().bl_id == %obj.getGroup().bl_id && %obj.getDatablock().isPowerStorage && %obj.EOTW_Energy > 0)
			{
				%obj.EOTW_Energy--;
				%obj.UpdatePowerText("NONE");
				
				%this.SetBrickText("(Heal Pulser) Running","0 1 0");
				%this.EOTW_EnergyRunning = true;
				%foundPower = true;
				
				%this.EnergyInterval++;
				
				if (%this.EnergyInterval > 4)
				{
					initContainerRadiusSearch(%this.getPosition(), 32, $TypeMasks::PlayerObjectType);
					while(isObject(%pl = containerSearchNext()))
					{
						%pl.addHealth(2);
					}
					%this.EnergyInterval = 0;
				}
			}
		}
	}
	
	if (!%foundPower)
	{
		
		%this.SetBrickText("(Heal Pulser) No Power","1 0 0");
		%this.EOTW_EnergyRunning = false;
		%this.EnergyInterval = 0;
	}
}

//Monster Repellant

datablock fxDTSBrickData(brickEOTWAltarData)
{
	brickFile = "./bricks/IonBattery.blb";
	category = "Solar Apoc";
	subCategory = "Machinery";
	uiName = "Altar";
	restrictMat = "Metal";
	isPowerConsumer = true;
	powerCooldown = 1000;
	loopFunc = "EOTW_AltarLoop";
	iconName = "Add-Ons/Gamemode_Solar_Apoc3/uiIcons/Altar";
};

function fxDtsBrick::EOTW_AltarLoop(%this)
{
	if (isObject(%this.getGroup().client) && isObject(%this.EOTW_PowerSources))
	{
		for (%i = 0; %i < %this.EOTW_PowerSources.getCount(); %i++)
		{
			if (!isObject(%obj = %this.EOTW_PowerSources.getObject(%i)))
				break;
				
			if ((%this.getGroup().client == %obj.getGroup().client || %this.getGroup().client.getBL_IDTrustLevel(%obj.getGroup().bl_id) > 0) && %obj.getDatablock().isPowerStorage && %obj.EOTW_Energy > 0)
			{
				%obj.EOTW_Energy--;
				%obj.UpdatePowerText("NONE");
				
				%this.SetBrickText("(Altar) Running","0 1 0");
				%this.EOTW_EnergyRunning = true;
				%this.hasPower = true;
				%foundPower = true;
				break;
			}
		}
	}
	
	if (!%foundPower)
	{
		%this.hasPower = false;
		%this.SetBrickText("(Altar) No Power","1 0 0");
		%this.EOTW_EnergyRunning = false;
	}
}

// function EOTWPowerLoop()
// {
	// cancel($EOTW::PowerLoop);
	// %count = PowerGroup.getCount();
	
	// for (%i = 0; %i < %count; %i++)
	// {
		// if (!isObject(%brick = PowerGroup.getObject(%i))) continue;
		
		// %data = %brick.getDatablock();
		
		// if ((getSimTime() - %brick.lastPowerUpdate) >= %data.loopFuncDelay)
		// {
			// %brick.lastPowerUpdate = getSimTime();
			
			// if (%data.loopFunc $= "EOTW_SolarPanelLoop") %brick.EOTW_SolarPanelLoop();
			// else if (%data.loopFunc $= "EOTW_GeneratorLoop") %brick.EOTW_GeneratorLoop();
			// else if (%data.loopFunc $= "EOTW_BatteryLoop") %brick.EOTW_BatteryLoop();
			// else if (%data.loopFunc $= "EOTW_IonBatteryLoop") %brick.EOTW_IonBatteryLoop();
			// else if (%data.loopFunc $= "EOTW_OreGrowerLoop") %brick.EOTW_OreGrowerLoop();
			// else if (%data.loopFunc $= "EOTW_SLCLoop") %brick.EOTW_SLCLoop();
			// else if (%data.loopFunc $= "EOTW_RepellantLoop") %brick.EOTW_RepellantLoop();
			// else if (%data.loopFunc $= "EOTW_HealPulserLoop") %brick.EOTW_HealPulserLoop();
			// else if (%data.loopFunc $= "EOTW_AltarLoop") %brick.EOTW_AltarLoop();
		// }
	// }
	
	// $EOTW::PowerLoop = schedule(500,ClientGroup,"EOTWPowerLoop");
// }

// if (!isObject(PowerGroup))
// {
	// new SimSet(PowerGroup);
	// EOTWPowerLoop();
// }

//Optimization (Hopefully)

$EOTW::PowerTickRate = 1;
$EOTW::PowerTickLoop = 25;

	if (!isObject(EOTW_PowerStorage))
		new SimSet(EOTW_PowerStorage);
		
	if (!isObject(EOTW_PowerSupplier))
		new SimSet(EOTW_PowerSupplier);
		
	if (!isObject(EOTW_PowerConsumer))
		new SimSet(EOTW_PowerConsumer);	
		
function EOTW_SetupPower()
{
	$EOTW::PowerTickRate = 100;
	$EOTW::PowerTickLoop = 50;
	$EOTW::MaxPowerTicks = 200;
	$EOTW::PowerTickChunk["Storage"] = 100;
	$EOTW::PowerTickChunk["Supplier"] = 100;
	$EOTW::PowerTickChunk["Consumer"] = 100;
	
	if (!isObject(EOTW_PowerStorage))
		new SimSet(EOTW_PowerStorage);
		
	if (!isObject(EOTW_PowerSupplier))
		new SimSet(EOTW_PowerSupplier);
		
	if (!isObject(EOTW_PowerConsumer))
		new SimSet(EOTW_PowerConsumer);	
	
	// if (!isObject($EOTW::StorageLoop))
		// EOTW_PowerLoop("Storage");
		
	// if (!isObject($EOTW::SupplierLoop))
		// EOTW_PowerLoop("Supplier");
		
	// if (!isObject($EOTW::ConsumerLoop))
		// EOTW_PowerLoop("Consumer");
}
schedule(1,0,"EOTW_SetupPower");

function EOTW_PowerLoop(%type,%iteration,%set)
{
	%iteration = %iteration + 0;
	if (%type $= "Storage")
	{
		if (isObject(%set))
			cancel(%set.schedLoop);
		else
			cancel($EOTW::StorageLoop);
		
		for (%i = 0; %i < $EOTW::PowerTickLoop; %i++)
		{
			if (isObject(%set))
			{
				if (%iteration < %set.getCount())
				{
					%obj = %set.getObject(%iteration);
					
					if (isObject(%obj.getGroup().client) && %obj.getGroup().client.powerTicks < $EOTW::MaxPowerTicks && (%call = %obj.getDatablock().loopFunc) !$= "" && (getSimTime() - %obj.lastPowerTime) > %obj.getDatablock().powerCooldown)
					{
						%obj.getGroup().client.powerTicks++;
						%obj.lastPowerTime = getSimTime();
						%obj.doCall(%call);
					}
						
					%iteration++;
				}
				else
				{
					%iteration = 0;
					break;
				}
			}
			else
			{
				if (%iteration < EOTW_PowerStorage.getCount())
				{
					%obj = EOTW_PowerStorage.getObject(%iteration);
					
					if (isObject(%obj.getGroup().client) && %obj.getGroup().client.powerTicks < $EOTW::MaxPowerTicks && (%call = %obj.getDatablock().loopFunc) !$= "" && (getSimTime() - %obj.lastPowerTime) > %obj.getDatablock().powerCooldown)
					{
						%obj.getGroup().client.powerTicks++;
						%obj.lastPowerTime = getSimTime();
						%obj.doCall(%call);
					}
						
					%iteration++;
				}
				else
				{
					%iteration = 0;
					break;
				}
			}
		}
		
		if (isObject(%set) && %iteration > 0)
			%modifier = 10;
		else
			%modifier = 1;
			
		
		if (isObject(%set))
			%set.schedLoop = schedule($EOTW::PowerTickRate * %modifier,0,"EOTW_PowerLoop", %type, %iteration, %set);
		else
			$EOTW::StorageLoop = schedule($EOTW::PowerTickRate * %modifier,0,"EOTW_PowerLoop", %type, %iteration, %set);
			
		return;
	}
	else if (%type $= "Supplier")
	{
		if (isObject(%set))
			cancel(%set.schedLoop);
		else
			cancel($EOTW::SupplierLoop);
		
		
		
		for (%i = 0; %i < $EOTW::PowerTickLoop; %i++)
		{
			if (isObject(%set))
			{
				if (%iteration < %set.getCount())
				{
					%obj = %set.getObject(%iteration);
					
					if (isObject(%obj.getGroup().client) && %obj.getGroup().client.powerTicks < $EOTW::MaxPowerTicks && (%call = %obj.getDatablock().loopFunc) !$= "" && (getSimTime() - %obj.lastPowerTime) > %obj.getDatablock().powerCooldown)
					{
						%obj.getGroup().client.powerTicks++;
						%obj.lastPowerTime = getSimTime();
						%obj.doCall(%call);
					}
						
					%iteration++;
				}
				else
				{
					%iteration = 0;
					break;
				}
			}
			else
			{
				if (%iteration < EOTW_PowerSupplier.getCount())
				{
					%obj = EOTW_PowerSupplier.getObject(%iteration);
					
					if (isObject(%obj.getGroup().client) && %obj.getGroup().client.powerTicks < $EOTW::MaxPowerTicks && (%call = %obj.getDatablock().loopFunc) !$= "" && (getSimTime() - %obj.lastPowerTime) > %obj.getDatablock().powerCooldown)
					{
						%obj.getGroup().client.powerTicks++;
						%obj.lastPowerTime = getSimTime();
						%obj.doCall(%call);
					}
						
					%iteration++;
				}
				else
				{
					%iteration = 0;
					break;
				}
			}
		}
		
		if (isObject(%set) && %iteration > 0)
			%modifier = 10;
		else
			%modifier = 1;
			
		if (isObject(%set))
			%set.schedLoop = schedule($EOTW::PowerTickRate * %modifier,0,"EOTW_PowerLoop", %type, %iteration, %set);
		else
			$EOTW::SupplierLoop = schedule($EOTW::PowerTickRate * %modifier,0,"EOTW_PowerLoop", %type, %iteration, %set);
			
		return;
	}
	else if (%type $= "Consumer")
	{
		if (isObject(%set))
			cancel(%set.schedLoop);
		else
			cancel($EOTW::ConsumerLoop);
		
		for (%i = 0; %i < $EOTW::PowerTickLoop; %i++)
		{
			if (isObject(%set))
			{
				if (%iteration < %set.getCount())
				{
					%obj = %set.getObject(%iteration);
					
					if (isObject(%obj.getGroup().client) && %obj.getGroup().client.powerTicks < $EOTW::MaxPowerTicks && (%call = %obj.getDatablock().loopFunc) !$= "" && (getSimTime() - %obj.lastPowerTime) > %obj.getDatablock().powerCooldown)
					{
						%obj.getGroup().client.powerTicks++;
						%obj.lastPowerTime = getSimTime();
						%obj.doCall(%call);
					}
						
					%iteration++;
				}
				else
				{
					%iteration = 0;
					break;
				}
			}
			else
			{
				if (%iteration < EOTW_PowerConsumer.getCount())
				{
					%obj = EOTW_PowerConsumer.getObject(%iteration);
					
					if (isObject(%obj.getGroup().client) && %obj.getGroup().client.powerTicks < $EOTW::MaxPowerTicks && (%call = %obj.getDatablock().loopFunc) !$= "" && (getSimTime() - %obj.lastPowerTime) > %obj.getDatablock().powerCooldown)
					{
						%obj.getGroup().client.powerTicks++;
						%obj.lastPowerTime = getSimTime();
						%obj.doCall(%call);
					}
						
					%iteration++;
				}
				else
				{
					%iteration = 0;
					break;
				}
			}
			
			
		}
		
		if (isObject(%set) && %iteration > 0)
			%modifier = 10;
		else
			%modifier = 1;
			
		if (isObject(%set))
			%set.schedLoop = schedule($EOTW::PowerTickRate * %modifier,0,"EOTW_PowerLoop", %type, %iteration, %set);
		else
			$EOTW::ConsumerLoop = schedule($EOTW::PowerTickRate * %modifier,0,"EOTW_PowerLoop", %type, %iteration, %set);
			
		return;
	}
}

function doPowerPreload()
{
	if (EOTW_PowerPreload.getCount() > 0)
	{
		EOTW_PowerPreload.getObject(0).EOTW_SetupPowerGroup();
		EOTW_PowerPreload.remove(EOTW_PowerPreload.getObject(0));
		$test = schedule(1,0,"doPowerPreload");
	}
}
package EOTW_PowerFix
{
	function fxDTSBrick::onPlant(%data,%obj)
	{
		%toReturn = Parent::onPlant(%data,%obj);
		
		%obj.EOTW_SetupPowerGroup();
		
		return %toReturn;
	}

	function fxDTSBrick::onLoadPlant(%data, %obj)
	{
		%toReturn = Parent::onLoadPlant(%data, %obj);
		
		if (%data.getDatablock().isPowerStorage || %data.getDatablock().isPowerSupplier || %data.getDatablock().isPowerConsumer)
		{
			if (!isObject(EOTW_PowerPreload))
				$EOTW::PowerPreload = new SimSet(EOTW_PowerPreload);
			
			EOTW_PowerPreload.add(%obj);
		}
		
		return %toReturn;
	}
			
	function fxDTSBrick::EOTW_SetupPowerGroup(%this)
	{
		if (%this.getDatablock().isPowerStorage)
		{
			%this.lastPowerTime = getSimTime();
			
			%this.EOTW_MaxEnergy = %this.getDatablock().MaxEnergy;
			
			EOTW_PowerStorage.add(%this);
			
			if (!isObject(%this.getGroup().PowerSet))
			{
				%this.getGroup().PowerSet = new SimSet();
			}
			
			if (!isObject(%this.getGroup().PowerSet.Storage))
			{
				%this.getGroup().PowerSet.Storage = new SimSet();
				EOTW_PowerLoop("Storage", 0, %this.getGroup().PowerSet.Storage);
			}
			
			%this.getGroup().PowerSet.Storage.add(%this);
			
			initContainerRadiusSearch(%this.getPosition(), 32, $TypeMasks::FxBrickAlwaysObjectType);
			
			while(isObject(%target = containerSearchNext()))
			{
				if (%target.getDatablock().isPowerSupplier)
					%this.EOTW_addPowerSource(%target);
				else if (%target.getDatablock().isPowerConsumer)
					%target.EOTW_addPowerSource(%this);
			}
		}
		else if (%this.getDatablock().isPowerSupplier)
		{
			%this.lastPowerTime = getSimTime();
			
			%this.EOTW_MaxEnergy = %this.getDatablock().MaxEnergy;
			
			EOTW_PowerSupplier.add(%this);
			
			if (!isObject(%this.getGroup().PowerSet))
			{
				%this.getGroup().PowerSet = new SimSet();
			}
			
			if (!isObject(%this.getGroup().PowerSet.Supplier))
			{
				%this.getGroup().PowerSet.Supplier = new SimSet();
				EOTW_PowerLoop("Supplier", 0, %this.getGroup().PowerSet.Supplier);
			}
			
			%this.getGroup().PowerSet.Supplier.add(%this);
			
			initContainerRadiusSearch(%this.getPosition(), 32, $TypeMasks::FxBrickAlwaysObjectType);
			
			while(isObject(%target = containerSearchNext()))
			{
				if (%target.getDatablock().isPowerStorage)
					%target.EOTW_addPowerSource(%this);
			}
		}
		else if (%this.getDatablock().isPowerConsumer)
		{
			%this.lastPowerTime = getSimTime();
			
			%this.EOTW_MaxEnergy = %this.getDatablock().MaxEnergy;
			
			EOTW_PowerConsumer.add(%this);
			
			if (!isObject(%this.getGroup().PowerSet))
			{
				%this.getGroup().PowerSet = new SimSet();
			}
			
			if (!isObject(%this.getGroup().PowerSet.Consumer))
			{
				%this.getGroup().PowerSet.Consumer = new SimSet();
				EOTW_PowerLoop("Consumer", 0, %this.getGroup().PowerSet.Consumer);
			}
			
			%this.getGroup().PowerSet.Consumer.add(%this);
			
			initContainerRadiusSearch(%this.getPosition(), 32, $TypeMasks::FxBrickAlwaysObjectType);
			
			while(isObject(%target = containerSearchNext()))
			{
				if (%target.getDatablock().isPowerStorage)
					%this.EOTW_addPowerSource(%target);
			}
		}
	}

	function fxDtsBrick::EOTW_addPowerSource(%this, %source)
	{
		if (!isObject(%this.EOTW_PowerSources))
			%this.EOTW_PowerSources = new SimSet();
			
		%this.EOTW_PowerSources.add(%source);
	}
};
activatePackage("EOTW_PowerFix");

function EOTW_RunningPowerReset()
{
	cancel($EOTW::PowerReset);
	
	for (%i = 0; %i < ClientGroup.getCount(); %i++)
		ClientGroup.getObject(%i).powerTicks = 0;
		
	$EOTW::PowerReset = schedule(1000,0,"EOTW_RunningPowerReset");
}
schedule(1, 0, "EOTW_RunningPowerReset");


//Extraction Wand

datablock ParticleData (ExtractionWandExplosionParticle)
{
	dragCoefficient = 10;
	gravityCoefficient = 0;
	inheritedVelFactor = 0.2;
	constantAcceleration = 0;
	lifetimeMS = 500;
	lifetimeVarianceMS = 300;
	spinSpeed = 0;
	spinRandomMin = -150.0;
	spinRandomMax = 150;
	textureName = "base/data/particles/dot";

	colors[0] = "0.7 0.7 0.0 0.9";
	colors[1] = "0.7 0.0 0.0 0.9";
	colors[1] = "0.7 0.0 0.0 0.0";

	sizes[0] = 0;
	sizes[1] = 0.7;
	sizes[2] = 0.2;

	times[0] = 0;
	times[1] = 0.2;
	times[2] = 1;
};

datablock ParticleEmitterData(ExtractionWandExplosionEmitter)
{
	lifetimeMS = 50;
	ejectionPeriodMS = 1;
	periodVarianceMS = 0;
	ejectionVelocity = 15;
	velocityVariance = 4;
	ejectionOffset = 0.25;
	thetaMin = 0;
	thetaMax = 120;
	phiReferenceVel = 0;
	phiVariance = 360;
	overrideAdvance = 0;
	particles = "ExtractionWandExplosionParticle";
};

datablock ExplosionData(ExtractionWandExplosion)
{
	lifetimeMS = 500;
	emitter[0] = ExtractionWandExplosionEmitter;
	soundProfile = WandHitSound;
	faceViewer = 1;
	explosionScale = "1 1 1";
	shakeCamera = 1;
	camShakeFreq = "2.0 2.0 2.0";
	camShakeAmp = "1.0 1.0 1.0";
	camShakeDuration = 0.5;
	camShakeRadius = 10;
	lightStartRadius = 3;
	lightEndRadius = 1;
	lightStartColor = "00.0 0.6 0.9";
	lightEndColor = "0 0 0";
};

datablock ProjectileData(ExtractionWandProjectile)
{
	directDamage = 0;
	radiusDamage = 0;
	damageRadius = 0.5;
	Explosion = ExtractionWandExplosion;
	muzzleVelocity = 50;
	velInheritFactor = 1;
	armingDelay = 0;
	lifetime = 0;
	fadeDelay = 70;
	bounceElasticity = 0;
	bounceFriction = 0;
	isBallistic = 0;
	gravityMod = 0;
	explodeOnDeath = 1;
	hasLight = 0;
	lightRadius = 3;
	lightColor = "0 0 0.5";
};

datablock ItemData(ExtractionWandItem)
{
	category = "Weapon";
	className = "Weapon";
	shapeFile = "base/data/shapes/wand.dts";
	mass = 1;
	density = 0.2;
	elasticity = 0.2;
	friction = 0.6;
	emap = 1;
	uiName = "Extraction Wand";
	iconName = "base/client/ui/itemIcons/wand";
	doColorShift = true;
	colorShiftColor = "0.860 0.860 0.10 0.750";
	image = ExtractionWandImage;
	canDrop = 1;
};

datablock ShapeBaseImageData(ExtractionWandImage)
{
	shapeFile = "base/data/shapes/wand.dts";
	emap = 0;
	mountPoint = 0;
	eyeOffset = "";
	correctMuzzleVector = 0;
	className = "WeaponImage";
	Item = WandItem;
	ammo = " ";
	Projectile = wandProjectile;
	projectileType = Projectile;
	melee = 1;
	doRetraction = 0;
	armReady = 1;
	showBricks = 1;
	doColorShift = true;
	colorShiftColor = "0.860 0.860 0.10 0.750";

	stateName[0]						= "Activate";
	stateTimeoutValue[0]				= 0.15;
	stateTransitionOnTimeout[0]			= "Ready";
	
	stateName[1]						= "Ready";
	stateTransitionOnTriggerDown[1]		= "Fire";
	stateAllowImageChange[1]			= true;
	stateEmitter[1]						= WandEmitterA;
	stateEmitterTime[1]					= 0.15;
	stateEmitterNode[1]					= "muzzlePoint";

	stateName[2]						= "Fire";
	stateTransitionOnTimeout[2]			= "Ready";
	stateAllowImageChange[2]			= true;
	stateScript[2]						= "onFire";
	stateTimeoutValue[2]				= 0.65;
	stateEmitter[2]						= WandEmitterB;
	stateEmitterTime[2]					= 0.4;
	stateEmitterNode[2]					= "blah";
};

function ExtractionWandImage::onFire(%this, %player, %slot)
{
	if (!isObject(%client = %player.client))
		return;

	%player.playthread(2, shiftUp);
	
	%start = %player.getEyePoint();

	%muzzleVec = %player.getMuzzleVector(%slot);
	%muzzleVecZ = getWord(%muzzleVec, 2);

	if (%muzzleVecZ < -0.9)
		%range = 5.5;
	else
		%range = 5;


	%vec = VectorScale(%muzzleVec, %range * getWord(%player.getScale(), 2) );
	%end = VectorAdd(%start, %vec);

	%mask = $TypeMasks::StaticObjectType | $TypeMasks::FxBrickAlwaysObjectType | $TypeMasks::PlayerObjectType | $TypeMasks::VehicleObjectType | $TypeMasks::StaticShapeObjectType;
	
	if (%player.isMounted())
		%exempt = %player.getObjectMount();
	else
		%exempt = %player;


	%raycast = containerRayCast(%start, %end, %mask, %exempt);

	if (!%raycast)
		return;


	%hitObj = getWord(%raycast, 0);
	%hitPos = getWords(%raycast, 1, 3);
	%hitNormal = getWords(%raycast, 4, 6);
	%projectilePos = VectorSub(%hitPos, VectorScale(%player.getEyeVector(), 0.25));

	%p = new Projectile()
	{
		dataBlock = ExtractionWandProjectile;

		initialVelocity = %hitNormal;
		initialPosition = %projectilePos;

		sourceObject = %player;
		sourceSlot = %slot;

		client = %player.client;
	};
	%p.setScale(%player.getScale());
	MissionCleanup.add(%p);
	
	initContainerRadiusSearch(%hitPos, 5, $TypeMasks::FxBrickAlwaysObjectType);
	
	while(isObject(%brick = containerSearchNext()))
	{
		if (%brick.fromOreGrower && %brick.getGroup().bl_id == %client.bl_id && %brick.material !$= "")
		{
			%amt = 1;
			if (%brick.material $= "Glass") %amt = 1;
			else if (%brick.material $= "Metal") %amt = 2;
			else if (%brick.material $= "Stone") %amt = 3;
			else if (%brick.material $= "Wood") %amt = 10;
			else if (%brick.material $= "Wolfram") %amt = 2;
			else if (%brick.material $= "Copper") %amt = 1;
			else if (%brick.material $= "Lead") %amt = 1;
			else if (%brick.material $= "Sturdium") %amt = 1;
			
			giveMaterial(%client, %brick.material, %amt);
			
			%brick.delete();
		}
	}
}
