datablock ItemData(BehemothWepItem)
{
	category = "Weapon";  // Mission editor category
	className = "Weapon"; // For inventory system

	 // Basic Item Properties
	shapeFile = "./models/ISBackPackItem.dts";
	rotate = false;
	mass = 1;
	density = 0.2;
	elasticity = 0.2;
	friction = 0.6;
	emap = true;

	//gui stuff
	uiName = "Test";
	iconName = "./images/BackpackIcon";
	doColorShift = false;

	 // Dynamic properties defined by the scripts
	image = BehemothWepImage;
	canDrop = true;
};

//TODO: does this work?
datablock ShapeBaseImageData(BehemothWepImage)
{
   // Basic Item properties
   shapeFile = "base/data/shapes/empty.dts";
   emap = true;

   // Specify mount point & offset for 3rd person, and eye offset
   // for first person rendering.
   mountPoint = 0;
   offset = "0.09 -0.07 -0.2";
   eyeOffset = 0; //"0.7 1.2 -0.5";
   rotation = eulerToMatrix( "0 0 0" );

   className = "WeaponImage";
   item = BehemothWepItem;

   //raise your arm up or not
   armReady = true;

   doColorShift = false;

   // Initial start up state
   
    stateName[0]                     = "Activate";
	stateTimeoutValue[0]             = 0.5;
	stateTransitionOnTimeout[0]      = "Ready";
	
	stateName[1]                     = "Ready";
	stateTransitionOnTriggerDown[1]  = "Fire";
	stateAllowImageChange[1]         = true;

	stateName[2]                     = "Fire";
	stateTransitionOnTimeout[2]      = "Ready";
	stateAllowImageChange[2]         = true;
	stateScript[2]                   = "onFire";
	stateTimeoutValue[2]		     = 1.0;
};

function BehemothWepImage::onFire(%this,%obj,%slot)
{
	return;
	if (getRandom(0,1) == 0)
	{
		%amt = getRandom(getRandom(2,3));
		for (%i=0;%i<%amt;%i++)
			spawnNewZombie(vectorAdd(%obj.getTransform(),(getRandom(-10,10) SPC getRandom(-10,10) SPC 8)),ElementalHoleBot);
	}
	else
	{
		%amt = getRandom(getRandom(3,4));
		for (%i=0;%i<%amt;%i++)
			spawnNewZombie(vectorAdd(%obj.getTransform(),(getRandom(-10,10) SPC getRandom(-10,10) SPC 8)),HuskHoleBot);
	}
}

function BehemothWepImage::SpawnHorde(%this,%obj,%slot)
{
	return;
	if (getRandom(0,1) == 0)
	{
		%amt = getRandom(getRandom(2,3));
		for (%i=0;%i<%amt;%i++)
			spawnNewZombie(vectorAdd(%obj.getTransform(),(getRandom(-10,10) SPC getRandom(-10,10) SPC 8)),ElementalHoleBot);
	}
	else
	{
		%amt = getRandom(getRandom(3,4));
		for (%i=0;%i<%amt;%i++)
			spawnNewZombie(vectorAdd(%obj.getTransform(),(getRandom(-10,10) SPC getRandom(-10,10) SPC 8)),HuskHoleBot);
	}
}

//TODO: does this work?
datablock ItemData(SturdiumBeastWepItem)
{
	category = "Weapon";  // Mission editor category
	className = "Weapon"; // For inventory system

	 // Basic Item Properties
	shapeFile = "./models/ISBackPackItem.dts";
	rotate = false;
	mass = 1;
	density = 0.2;
	elasticity = 0.2;
	friction = 0.6;
	emap = true;

	//gui stuff
	uiName = "Test II";
	iconName = "./images/BackpackIcon";
	doColorShift = false;

	 // Dynamic properties defined by the scripts
	image = SturdiumBeastWepImage;
	canDrop = true;
};

datablock ShapeBaseImageData(SturdiumBeastWepImage)
{
   // Basic Item properties
   shapeFile = "base/data/shapes/empty.dts";
   emap = true;

   // Specify mount point & offset for 3rd person, and eye offset
   // for first person rendering.
   mountPoint = 0;
   offset = "0.09 -0.07 -0.2";
   eyeOffset = 0; //"0.7 1.2 -0.5";
   rotation = eulerToMatrix( "0 0 0" );

   className = "WeaponImage";
   item = SturdiumBeastWepItem;
   
   projectile = BeastMeleeProjectile;
   projectileType = projectile;

   //raise your arm up or not
   armReady = false;
   
   melee = true;

   doColorShift = false;

   // Initial start up state
   
    stateName[0]                     = "Activate";
	stateTimeoutValue[0]             = 0.5;
	stateTransitionOnTimeout[0]      = "Ready";
	
	stateName[1]                     = "Ready";
	stateTransitionOnTriggerDown[1]  = "Fire";
	stateAllowImageChange[1]         = true;

	stateName[2]                     = "Fire";
	stateTransitionOnTimeout[2]      = "Ready";
	stateAllowImageChange[2]         = true;
	stateScript[2]                   = "decideAttack";
	stateTimeoutValue[2]		     = 1.0;
};

function SturdiumBeastWepImage::decideAttack(%this,%obj,%slot) //Decide the attack based on players and previous attack.
{
	%count = 0;
	initContainerRadiusSearch(%pos = %obj.getPosition(), 25, $Typemasks::PlayerObjectType);
	while(isObject(%target = containerSearchNext()))
	{ // && %target.getClassName() $= "Player"
		if (%target != %obj)
		{
			%count++;
		}
	}
	if (%count > 1 && %obj.lastBossAttack $= "Punch") %this.onFireC(%obj,%slot);
	else if (%count > 1 && %obj.lastBossAttack !$= "Fissure") %this.onFireB(%obj,%slot);
	else %this.onFireA(%obj,%slot);
}

function SturdiumBeastWepImage::onFireA(%this,%obj,%slot) //Basic Punch
{
	%obj.setVelocity(VectorAdd(%obj.getVelocity(),VectorScale(%obj.getEyeVector(),"14")));
	
	%obj.playThread(2, shiftUp);
	
	%obj.lastBossAttack = "Punch";
	
	%projectile = BeastMeleeProjectile;
	%spread = 0.0;
	%shellcount = 1;
	
	for(%i=1;%i<=%shellcount;%i++)
	{
		%obj.playThread(2, shiftDown);
		%vector = %obj.getEyeVector();
		%objectVelocity = %obj.getVelocity();
		%vector1 = VectorScale(%vector, %projectile.muzzleVelocity);
		%vector2 = VectorScale(%objectVelocity, %projectile.velInheritFactor);
		%velocity = VectorAdd(%vector1,%vector2);
		%x = (getRandom() - 0.5) * 10 * 3.1415926 * %spread;
		%y = (getRandom() - 0.5) * 10 * 3.1415926 * %spread;
		%z = (getRandom() - 0.5) * 10 * 3.1415926 * %spread;
		%mat = MatrixCreateFromEuler(%x @ " " @ %y @ " " @ %z);
		%velocity = MatrixMulVector(%mat, %velocity);
		%velocity = setWord(%velocity,2,0);
		
		if (!%obj.isCrouched()) %initPos = vectorSub(%obj.getEyePoint(),"0 0 2");
		else %initPos = %obj.getEyePoint();
		
		%p = new Projectile()
		{
			dataBlock = %projectile;
			initialVelocity = %velocity;
			initialPosition = %initPos;
			sourceObject = %obj;
			sourceSlot = %slot;
			client = %obj.client;
		};
		MissionCleanup.add(%p);
	}
	
	//parent::onFire(%this,%obj,%slot);
}

function SturdiumBeastWepImage::onFireB(%this,%obj,%slot,%amt) //Fissure Attack
{
	%amt = %amt + 0;
	
	if ((%obj.usingFissureAttack && %amt == 0) || %obj.getState() $= "DEAD") return;
	
	%obj.usingFissureAttack = true;
	%obj.lastBossAttack = "Fissure";
	
	%obj.setMaxForwardSpeed(0);
	%obj.setMaxSideSpeed(0);
	%obj.setMaxBackwardSpeed(0);
	%obj.setMaxCrouchForwardSpeed(0);
	%obj.setMaxCrouchSideSpeed(0);
	%obj.setMaxCrouchBackwardSpeed(0);
	
	%projectile = BeastFissureProjectile;
	%spread = 0.1;
	%shellcount = 25;
	for(%i=1;%i<=%shellcount;%i++)
	{
		if (!%obj.isGroundedSport() || %amt == 0) break;
		%obj.playThread(2, shiftDown);
		%vector = %obj.getEyeVector();
		%objectVelocity = %obj.getVelocity();
		%vector1 = VectorScale(%vector, %projectile.muzzleVelocity);
		%vector2 = VectorScale(%objectVelocity, %projectile.velInheritFactor);
		%velocity = VectorAdd(%vector1,%vector2);
		%x = (getRandom() - 0.5) * 10 * 3.1415926 * %spread;
		%y = (getRandom() - 0.5) * 10 * 3.1415926 * %spread;
		%z = (getRandom() - 0.5) * 10 * 3.1415926 * %spread;
		%mat = MatrixCreateFromEuler(%x @ " " @ %y @ " " @ %z);
		%velocity = MatrixMulVector(%mat, %velocity);
		%velocity = setWord(%velocity,2,0);
		
		if (!%obj.isCrouched()) %initPos = vectorSub(%obj.getEyePoint(),"0 0 2");
		else %initPos = %obj.getEyePoint();
		
		%p = new Projectile()
		{
			dataBlock = %projectile;
			initialVelocity = %velocity;
			initialPosition = %initPos;
			sourceObject = %obj;
			sourceSlot = %slot;
			client = %obj.client;
		};
		MissionCleanup.add(%p);
	}
	if (%amt < 3) %this.schedule(1000,"onFireB",%obj,%slot,%amt + 1);
	else
	{
		%obj.setMaxForwardSpeed(%obj.getDataBlock().MaxForwardSpeed);
		%obj.setMaxSideSpeed(%obj.getDataBlock().MaxSideSpeed);
		%obj.setMaxBackwardSpeed(%obj.getDataBlock().MaxBackwardSpeed);
		%obj.setMaxCrouchForwardSpeed(%obj.getDataBlock().MaxForwardCrouchSpeed);
		%obj.setMaxCrouchSideSpeed(%obj.getDataBlock().MaxSideCrouchSpeed);
		%obj.setMaxCrouchBackwardSpeed(%obj.getDataBlock().MaxBackwardCrouchSpeed);
		%obj.usingFissureAttack = false;
	}
}

function SturdiumBeastWepImage::onFireC(%this,%obj,%slot) //AoE Smash
{
	%obj.playThread(2, shiftUp);
	%obj.lastBossAttack = "Smash";
	initContainerRadiusSearch(%pos = %obj.getPosition(), 10, $Typemasks::PlayerObjectType);
	while(isObject(%target = containerSearchNext()))
	{
		if (%target != %obj)
		{
			%target.setVelocity(getRandom(-15,15) SPC getRandom(-15,15) SPC 20);
			%target.damage(%obj,%obj,20,%obj.getPosition(),$DamageType::BeastAoE);
		}
	}
}

datablock ParticleData(beastFistParticle)
{
   dragCoefficient      = 2;
   gravityCoefficient   = 1.0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = 0.0;
   spinRandomMin = -90;
   spinRandomMax = 90;
   lifetimeMS           = 500;
   lifetimeVarianceMS   = 300;
   textureName          = "base/data/particles/star1";
   colors[0]     = "0.7 0.0 0.0 0.7";
   colors[1]     = "0.9 0.9 0.9 1.0";
   colors[2]     = "0.9 0.9 0.9 0.0";
   sizes[0]      = 0.25;
   sizes[1]      = 0.75;
   sizes[2]      = 0.25;
   times[0]		 = 0.0;
   times[1]		 = 0.5;
   times[2]		 = 1.0;
};

datablock ParticleEmitterData(beastFistEmitter)
{
   lifetimeMS		= 100;
   ejectionPeriodMS = 10;
   periodVarianceMS = 0;
   ejectionVelocity = 8;
   velocityVariance = 1.0;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 60;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvance = false;
   particles = "beastFistParticle";

   uiName = "";
};

AddDamageType("BeastFist",   'Fist Smash %1',    '%2 Fist Smash %1',0.75,1);
datablock ProjectileData(BeastMeleeProjectile)
{
   directDamage        = 40;
   directDamageType  = $DamageType::BeastFist;
   radiusDamageType  = $DamageType::BeastFist;
   particleEmitter     = beastFistEmitter;

   muzzleVelocity      = 30;
   velInheritFactor    = 1;
   
   
   
   impactImpulse       = 1500;
   verticalImpulse     = 1000; 

   armingDelay         = 0;
   lifetime            = 250;
   fadeDelay           = 70;
   bounceElasticity    = 0;
   bounceFriction      = 0;
   isBallistic         = false;
   gravityMod = 0.0;

   hasLight    = false;
   lightRadius = 3.0;
   lightColor  = "0 0 0.5";

   uiName = "";
};

datablock ParticleData(beastFissureParticle)
{
   dragCoefficient      = 0.2;
   gravityCoefficient   = -0.2;
   inheritedVelFactor   = 0.2;
   constantAcceleration = 0.0;
   spinRandomMin = -20;
   spinRandomMax = 20;
   lifetimeMS           = 500;
   lifetimeVarianceMS   = 300;
   textureName          = "base/data/particles/star1";
   colors[0]     = "0.9 0.9 0.9 0.0";
   colors[1]     = "0.9 0.9 0.9 1.0";
   colors[2]     = "0.9 0.9 0.9 0.0";
   sizes[0]      = 0.25;
   sizes[1]      = 0.75;
   sizes[2]      = 0.25;
   times[0]		 = 0.0;
   times[1]		 = 0.5;
   times[2]		 = 1.0;
};

datablock ParticleEmitterData(beastFissureEmitter)
{
   lifetimeMS		= 2000;
   ejectionPeriodMS = 10;
   periodVarianceMS = 0;
   ejectionVelocity = 1;
   velocityVariance = 0.0;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 60;
   phiReferenceVel  = 0;
   phiVariance      = 0;
   overrideAdvance = false;
   particles = "beastFissureParticle";

   uiName = "";
};

AddDamageType("BeastFissure",   'Fissure %1',    '%2 Fissure %1',0.75,1);
datablock ProjectileData(BeastFissureProjectile)
{
	projectileShapeName = "Add-Ons/Weapon_Gun/bullet.dts";
   directDamage        = 10;
   directDamageType  = $DamageType::BeastFissure;
   radiusDamageType  = $DamageType::BeastFissure;
   particleEmitter     = beastFissureEmitter;

   muzzleVelocity      = 30;
   velInheritFactor    = 0;
   
   impactImpulse       = 1000;
   verticalImpulse     = 500; 

   armingDelay         = 2000;
   lifetime            = 2000;
   fadeDelay           = 70;
   bounceElasticity    = 0.75;
   bounceFriction      = 0;
   isBallistic         = true;
   gravityMod = 1.0;

   hasLight    = false;
   lightRadius = 3.0;
   lightColor  = "0 0 0.5";

   uiName = "";
};

AddDamageType("BeastAoE",   'Ground Pound %1',    '%2 Ground Pound %1',0.75,1);
