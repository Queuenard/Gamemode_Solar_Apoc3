//Spellbook item/image

datablock ItemData(EOTWSpellbookItem)
{
	category = "Weapon";  // Mission editor category
	className = "Weapon"; // For inventory system

	 // Basic Item Properties
	shapeFile = "./models/Spellbook.dts";
	rotate = false;
	mass = 1;
	density = 0.2;
	elasticity = 0.2;
	friction = 0.6;
	emap = true;

	//gui stuff
	uiName = "Magic Spellbook";
	iconName = "./images/BackpackIcon";
	doColorShift = true;
	colorShiftColor = "0.500 0.250 0.000 1.000";

	 // Dynamic properties defined by the scripts
	image = EOTWSpellbookImage;
	canDrop = true;
};

datablock ShapeBaseImageData(EOTWSpellbookImage)
{
   // Basic Item properties
   shapeFile = "./models/Spellbook.dts";
   emap = true;

   // Specify mount point & offset for 3rd person, and eye offset
   // for first person rendering.
   mountPoint = 0;
   offset = "0 0 0";
   eyeOffset = 0; //"0.7 1.2 -0.5";
   rotation = eulerToMatrix( "0 0 0" );

   className = "WeaponImage";
   item = EOTWSpellbookItem;

   //raise your arm up or not
   armReady = true;

   doColorShift = true;
   colorShiftColor = EOTWSpellbookItem.colorShiftColor;

   // Initial start up state
   
    stateName[0]                     = "Activate";
	stateTimeoutValue[0]             = 0.1;
	stateTransitionOnTimeout[0]      = "Ready";
	
	stateName[1]                     = "Ready";
	stateTransitionOnTriggerDown[1]  = "Fire";
	stateAllowImageChange[1]         = true;

	stateName[2]                     = "Fire";
	stateTransitionOnTimeout[2]      = "Ready";
	stateAllowImageChange[2]         = true;
	stateScript[2]                   = "onFire";
	stateTimeoutValue[2]		     = 1.0;
};

function EOTWSpellbookImage::onFire(%this,%obj,%slot)
{
	%cl = %obj.client;
	%currSlot = %obj.currTool;
	
	if (%cl.currentSpell !$= "")
	{
		%obj.EOTW_castSpell(%cl.currentSpell);
	}
	else
	{
		%cl.centerPrint("You do not have a spell equipped!",3);
	}
}

function Player::EOTW_CastSpell(%obj,%name)
{
	if (%name $= "fireball")
	{
		%obj.lastFireTime = getSimTime();
		  
		%obj.setVelocity(VectorAdd(%obj.getVelocity(),VectorScale(%obj.getEyeVector(),"-3")));
		%obj.playThread(2, shiftAway);

		%projectile = gunProjectile;
		%spread = 0.0015;
		%shellcount = 3;
		%slot = 0;
		for(%shell=0; %shell<%shellcount; %shell++)
		{
			%vector = %obj.getMuzzleVector(%slot);
			%objectVelocity = %obj.getVelocity();
			%vector1 = VectorScale(%vector, %projectile.muzzleVelocity);
			%vector2 = VectorScale(%objectVelocity, %projectile.velInheritFactor);
			%velocity = VectorAdd(%vector1,%vector2);
			%x = (getRandom() - 0.5) * 10 * 3.1415926 * %spread;
			%y = (getRandom() - 0.5) * 10 * 3.1415926 * %spread;
			%z = (getRandom() - 0.5) * 10 * 3.1415926 * %spread;
			%mat = MatrixCreateFromEuler(%x @ " " @ %y @ " " @ %z);
			%velocity = MatrixMulVector(%mat, %velocity);

			%p = new (Projectile)()
			{
				dataBlock = %projectile;
				initialVelocity = %velocity;
				initialPosition = %obj.getMuzzlePoint(%slot);
				sourceObject = %obj;
				sourceSlot = %slot;
				client = %obj.client;
			};
			MissionCleanup.add(%p);
		}
	}
}

package EOTW_Magic
{
	function Armor::onTrigger(%data, %this, %trig, %tog)
	{
		if(isObject(%cl = %this.client))
		{
			if (%this.MagicSpellAlt !$= "" && %trig == 4 && %tog && isObject(%this.getMountedImage(0)) && %this.getMountedImage(0).getName() $= "EOTWSpellbookImage")
			{
			
			}
		}
		Parent::onTrigger(%data, %this, %trig, %tog);
	}
	
};
activatePackage("EOTW_Magic");
