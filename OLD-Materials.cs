function EOTW_SetColors()
{
	$EOTW::Colors::Dead		= getClosestColor("0.60 0.60 0.30 1.00");   //153 153 077
	$EOTW::Colors::Glass	= getClosestColor("1.00 1.00 1.00 0.50");   //128 128 128
	$EOTW::Colors::Grass	= getClosestColor("0.05 0.75 0.15 1.00");   //013 191 038
	$EOTW::Colors::Metal	= getClosestColor("0.35 0.35 0.40 1.00");   //089 089 102
	$EOTW::Colors::Copper	= getClosestColor("1.00 0.50 0.00 1.00");	//256 128 000
	$EOTW::Colors::Lead		= getClosestColor("0.00 0.00 0.00 1.00");   //000 000 000
	$EOTW::Colors::Stone	= getClosestColor("0.75 0.75 0.80 1.00");   //191 191 204
	$EOTW::Colors::Sturdium	= getClosestColor("0.20 0.20 0.25 1.00");   //051 051 064
	$EOTW::Colors::Vine		= getClosestColor("0.05 0.35 0.15 1.00");   //013 089 038
	$EOTW::Colors::Wood		= getClosestColor("0.50 0.35 0.15 1.00");   //128 089 038
	$EOTW::Colors::Wolfram	= getClosestColor("0.55 0.88 0.59 1.00");	//140 226 152 //8CE298
	
	//Raw Colors. Is there a way to get RGB from color index?
	
	$EOTW::RawColors::Dead		= "0.60 0.60 0.30 1.00";   //153 153 077
	$EOTW::RawColors::Glass		= "1.00 1.00 1.00 0.50";   //128 128 128
	$EOTW::RawColors::Grass		= "0.05 0.75 0.15 1.00";   //013 191 038
	$EOTW::RawColors::Metal		= "0.35 0.35 0.40 1.00";   //089 089 102
	$EOTW::RawColors::Copper	= "1.00 0.50 0.00 1.00";	//256 128 000
	$EOTW::RawColors::Lead		= "0.00 0.00 0.00 1.00";   //000 000 000
	$EOTW::RawColors::Stone		= "0.75 0.75 0.80 1.00";   //191 191 204
	$EOTW::RawColors::Sturdium	= "0.20 0.20 0.25 1.00";   //051 051 064
	$EOTW::RawColors::Vine		= "0.05 0.35 0.15 1.00";   //013 089 038
	$EOTW::RawColors::Wood		= "0.50 0.35 0.15 1.00";   //128 089 038
	$EOTW::RawColors::Wolfram	= "0.55 0.88 0.59 1.00";	//140 226 152 //8CE298
}
EOTW_SetColors();

package EOTW_Materials
{
	function Armor::onTrigger(%data, %this, %trig, %tog)
	{
		if(isEventPending($EnvMasterLoop) && isObject(%cl = %this.client))
		{
			//echo("%trig: " SPC %trig SPC "%tog: " SPC %tog);
			
			if(%trig == 0 && !(isObject(%this.getMountedImage(0)) && %tog))
			{
				if(%tog)
				{
					%eye = %this.getEyePoint();
					%dir = %this.getEyeVector();
					%for = %this.getForwardVector();
					%face = getWords(vectorScale(getWords(%for, 0, 1), vectorLen(getWords(%dir, 0, 1))), 0, 1) SPC getWord(%dir, 2);
					%mask = $Typemasks::fxBrickAlwaysObjectType | $Typemasks::TerrainObjectType;
					%ray = containerRaycast(%eye, vectorAdd(%eye, vectorScale(%face, 5)), %mask, %this);
					if(isObject(%hit = firstWord(%ray)) && %hit.getClassName() $= "fxDtsBrick")
					{
						if (%hit.isCollectable)
						{
							if(%hit.beingCollected)
								%cl.centerPrint("<color:FFFFFF>Someone is already collecting that material brick!", 3);
							else
							{
								%timePercent = %this.pickupSpeed;
								for (%i=0;%i<%cl.player.getDatablock().maxtools;%i++)
								{
									if (isObject(%cl.player.tool[%i]) && %cl.player.tool[%i].getName() $= "handDrill2Item")
									{ %timePercent -= 0.35; break; }
									if (isObject(%cl.player.tool[%i]) && %cl.player.tool[%i].getName() $= "handDrillItem")
									{ %timePercent -= 0.25; break; }
								}
								initContainerRadiusSearch(%this.getPosition(), 32, $TypeMasks::fxBrickAlwaysObjectType);
								while(isObject(%objj = containerSearchNext()))
									if (%objj.getDatablock().getName() $= "brickEOTWAltarData" && %objj.hasPower)
									{ %timePercent -= 0.20; break; }
								%hit.beingCollected = 1;
								%hit.cancelCollecting = %hit.schedule(48, "cancelCollecting");
								%this.collectLoop(%hit, getSimTime(), %timePercent);
								%cl.material = %hit.material;
							}
						}
						else if (%hit.getDatablock().getName() $= "EOTWChestData" && !%hit.chestOpening)
						{
							%hit.setDatablock("EOTWChestOpenData");
							schedule(1000,%hit,"SpawnChestLoot",%hit,%cl);
							%hit.chestOpening = true;
							
							%cl.AwardAchievement("Treasure Hunter");
							
							%cl.incScore(10);
						}
						else if (%hit.getDatablock().loopFunc !$= "" && %hit.getDatablock().getName() !$= "brickEOTWCrankData")
						{
							if (%hit.lastBrickText !$= "")
								%cl.centerPrint("<color:" @ RGBToHex(%hit.lastBrickColor) @ ">" @ %hit.lastBrickText, 2);
							else if (%hit.getDatablock().maxEnergy > 0)
								%cl.centerPrint("<color:ffffff>" @ %hit.getDatablock().uiName @ " (" @ (%hit.EOTW_Energy + 0) @ "/" @ %hit.getDatablock().maxEnergy @ " Power)", 2);
							else
								%cl.centerPrint("<color:ffffff>" @ %hit.getDatablock().uiName, 2);
						}
						else if (%hit.getDatablock().getName() $= "brickEOTWCrankData" && %hit.EOTW_Energy < %hit.EOTW_MaxEnergy && (getSimTime() - %hit.lastUse) > getRandom(75,150))
						{
							if (%hit.energyInterval < 10) %dbName = "Synth_0" @ %hit.energyInterval @ "_Sound";
							else %dbName = "Synth_" @ %hit.energyInterval @ "_Sound";
							
							%hit.playSound(%dbName);
							%hit.energyInterval++;
							%hit.lastUse = getSimTime();
							if (%hit.energyInterval > 10)
							{
								%hit.EOTW_Energy++;
								%hit.energyInterval = 0;
								
								%hit.UpdatePowerText("1 1 1");
							}
							
							%cl.centerPrint("<color:ffffff>" @ %hit.getDatablock().uiName @ " (" @ (%hit.EOTW_Energy + 0) @ "/" @ %hit.getDatablock().maxEnergy @ " Power)", 2);
						}
					}
				}
				else if(isEventPending(%this.collectLoop))
					cancel(%this.collectLoop);
			}
			else if(%trig == 4 && ((isObject(%image = %this.getMountedImage(0)) && %image.getName() $= "BrickImage") || isEventPending(%this.collectLoop)))
			{
				if(%tog)
				{	
					if (!%cl.player.isCrouched())
					{
						switch$(%cl.material)
						{
							case "Dead_Plant": %cl.material = "Glass";
							case "Glass": %cl.material = "Grass";
							case "Grass": %cl.material = "Metal";
							case "Metal": %cl.material = "Stone";
							case "Stone": %cl.material = "Sturdium";
							case "Sturdium": %cl.material = "Vine";
							case "Vine": %cl.material = "Wood";
							case "Wood": %cl.material = "Copper";
							case "Copper": %cl.material = "Lead";
							case "Lead": %cl.material = "Wolfram";
							case "Wolfram": %cl.material = "Glass";
						}
					}
					else
					{
						switch$(%cl.material)
						{
							case "Dead_Plant": %cl.material = "Glass";
							case "Glass": %cl.material = "Wolfram";
							case "Grass": %cl.material = "Glass";
							case "Metal": %cl.material = "Grass";
							case "Stone": %cl.material = "Metal";
							case "Sturdium": %cl.material = "Stone";
							case "Vine": %cl.material = "Sturdium";
							case "Wood": %cl.material = "Vine";
							case "Copper": %cl.material = "Wood";
							case "Lead": %cl.material = "Copper";
							case "Wolfram": %cl.material = "Lead";
						}
					}

				}

				%cl.showMaterials();
			}
		}
		Parent::onTrigger(%data, %this, %trig, %tog);
	}
	
	function ScrollInventory(%a, %b, %c)
	{
		//echo(%a SPC %b SPC %c);
		parent::ScrollInventory(%a, %b, %c);
		//%cl.showMaterials();
	}
};
activatePackage("EOTW_Materials");

function ServerCmdMats(%cl) { ServerCmdMat(%cl); }
function ServerCmdMat(%cl)
{
	%cl.chatMessage("\c6Materials:");
	%cl.chatMessage("<color:808080>Glass\c6: " @ $EOTW::Material[%cl.bl_id,"Glass"] + 0);
	%cl.chatMessage("<color:0EBF26>Grass\c6: " @ $EOTW::Material[%cl.bl_id,"Grass"] + 0);
	%cl.chatMessage("<color:595966>Metal\c6: " @ $EOTW::Material[%cl.bl_id,"Metal"] + 0);
	%cl.chatMessage("<color:BFBFCC>Stone\c6: " @ $EOTW::Material[%cl.bl_id,"Stone"] + 0);
	%cl.chatMessage(($EOTW::Day >= 53 ? "<color:333340>"@$SturdiumName : "<color:FFFFFF>???")@"\c6: " @ $EOTW::Material[%cl.bl_id,"Sturdium"] + 0);
	%cl.chatMessage("<color:0E5926>Vine\c6: " @ $EOTW::Material[%cl.bl_id,"Vine"] + 0);
	%cl.chatMessage("<color:805926>Wood\c6: " @ $EOTW::Material[%cl.bl_id,"Wood"] + 0);
	%cl.chatMessage("<color:FF8800>Copper\c6: " @ $EOTW::Material[%cl.bl_id,"Copper"] + 0);
	%cl.chatMessage("<color:111111>Lead\c6: " @ $EOTW::Material[%cl.bl_id,"Lead"] + 0);
	%cl.chatMessage("<color:8CE298>Wolfram\c6: " @ $EOTW::Material[%cl.bl_id,"Wolfram"] + 0);
}
function fxDtsBrick::cancelCollecting(%brick)
{ %brick.beingCollected = 0; }

function GameConnection::showMaterials(%cl)
{
	if(%cl.material $= "")
		%cl.material = "Wood";
	%amt = $EOTW::Material[%cl.bl_id, %cl.material] + 0;
	%sturdium = ($EOTW::Day >= 53 ? "Sturdium" : "???");
	
	%text = "<just:center>";
	switch$(%cl.material)
	{
		case "Dead_Plant":	%text = %text @ ("<color:99994D>Dead Plant\c6: STOP HACKING MY GAME.        \c7Next material: MISSINGNO   (Jet to cycle)");
		case "Glass":	%text = %text @ ("<color:808080>Glass\c6: " @ %amt @ "        \c7Next material: Grass   (Jet to cycle)");
		case "Grass":	%text = %text @ ("<color:0EBF26>Grass\c6: " @ %amt @ "        \c7Next material: Metal   (Jet to cycle)");
		case "Metal":	%text = %text @ ("<color:595966>Metal\c6: " @ %amt @ "        \c7Next material: Stone   (Jet to cycle)");
		case "Stone":	%text = %text @ ("<color:BFBFCC>Stone\c6: " @ %amt @ "        \c7Next material: "@ %sturdium @"   (Jet to cycle)");
		case "Sturdium":%text = %text @ (($EOTW::Day >= 53 ? "<color:333340>"@$SturdiumName : "<color:FFFFFF>???")@"\c6: "@%amt@"        \c7Next material: Vine   (Jet to cycle)");
		case "Vine":	%text = %text @ ("<color:0E5926>Vine\c6: " @ %amt @ "        \c7Next material: Wood   (Jet to cycle)");
		case "Wood":	%text = %text @ ("<color:805926>Wood\c6: " @ %amt @ "        \c7Next material: Copper   (Jet to cycle)");
		case "Copper":	%text = %text @ ("<color:FF8800>Copper\c6: " @ %amt @ "        \c7Next material: Lead   (Jet to cycle)");
		case "Lead":	%text = %text @ ("<color:111111>Lead\c6: " @ %amt @ "        \c7Next material: Wolfram   (Jet to cycle)");
		case "Wolfram":	%text = %text @ ("<color:8CE298>Wolfram\c6: " @ %amt @ "        \c7Next material: Glass   (Jet to cycle)");
	}

	%db = %cl.inventory[%cl.currInvSlot];
	%volume = "  \c6(Brick Cost:" SPC (%db.brickSizeX * %db.brickSizeY * %db.brickSizeZ) @ ")";
	
	if ($EOTW::IsDay) %timeText = "Day";
	else %timeText = "Night";
		
	%text = %text @ %volume @ "<br>\c3" @ %timeText @ "\c6: " @ $EOTW::Day @ "  \c3Time\c6: " @ $LastEOTWTime @ "     " @ %cl.player.getHealthText();
	%cl.bottomPrint(%text,30);
}

function Player::collectLoop(%pl, %brick, %start, %percentTime)
{
	cancel(%pl.collectLoop);
	if(!isObject(%cl = %pl.client) || %pl.getState() $= "DEAD") return;
	if(!isObject(%brick) || %brick.isDead()) return;
	%eye = %pl.getEyePoint();
	%dir = %pl.getEyeVector();
	%for = %pl.getForwardVector();
	%face = getWords(vectorScale(getWords(%for, 0, 1), vectorLen(getWords(%dir, 0, 1))), 0, 1) SPC getWord(%dir, 2);
	%mask = $Typemasks::fxBrickAlwaysObjectType | $Typemasks::TerrainObjectType;
	%ray = containerRaycast(%eye, vectorAdd(%eye, vectorScale(%face, 5)), %mask, %this);
	if(isObject(%hit = firstWord(%ray)) && %hit == %brick)
	{
		cancel(%brick.cancelCollecting);
		if((%time=vectorDist(%start, getSimTime())) >= %brick.collectTime * %percentTime)
		{
			if(%brick.forceVolume !$= "")
				%volume = %brick.forceVolume;
			else
			{
				%data = %brick.getDatablock();
				%volume = %data.brickSizeX * %data.brickSizeY * %data.brickSizeZ;
			}
			//%old = $EOTW::Material[%cl.bl_id, %brick.material];
			//$EOTW::Material[%cl.bl_id, %brick.material] = %volume + %old;
			giveMaterial(%cl,%brick.material,%volume);
			if(%brick.material $= "Sturdium") %mat = ($EOTW::Day < 53 ? "???" : $SturdiumName); else %mat = %brick.material;
			if (%brick.pickupSize)
				%cl.centerPrint("<br><color:FFFFFF>Collected a large gatherable "@%mat@" brick!<br>100% complete.<br>+" @ %volume SPC %brick.material, 3);		
			else
				%cl.centerPrint("<br><color:FFFFFF>Collected a gatherable "@%mat@" brick.<br>100% complete.<br>+" @ %volume SPC %brick.material, 3);
			%brick.isCollectable = 0; %brick.killbrick(); %cl.showMaterials();
		}
		else
		{
			%brick.cancelCollecting = %brick.schedule(48, "cancelCollecting");
			%pl.collectLoop = %pl.schedule(16, "collectLoop", %brick, %start, %percentTime);
			if(%brick.material $= "Sturdium") %mat = ($EOTW::Day < 53 ? "???" : $SturdiumName); else %mat = %brick.material;
			%progress = mFloatLength(((%time/(%brick.collectTime*%percentTime))*100) ,0);
			if(%cl.gatherProgress != %progress)
			{
				if (%percentTime < 1)
					%pickMsg = "<br><color:0000AA>(Speed Boost -" SPC %percentTime * 100 @ "% Time)";
					
				if (%brick.pickupSize)
					%cl.centerPrint("<br><color:FFFFFF>Collecting a large gatherable "@%mat@" brick.<br>"@%cl.gatherProgress@"% complete." @ %pickMsg, 3);
				else
					%cl.centerPrint("<br><color:FFFFFF>Collecting a gatherable "@%mat@" brick.<br>"@%cl.gatherProgress@"% complete." @ %pickMsg, 3);
				%cl.gatherProgress = %progress;
			}
		}
	}
}

function GatherableLoop()
{
	cancel($GatherableLoop);
	if($EOTW::Day < 35) for(%i=0;%i<3;%i++) %mat[-1+%mats++] = "Glass";
	if($EOTW::Day < 5) for(%i=0;%i<4;%i++) %mat[-1+%mats++] = "Grass";
	if($EOTW::Day < 45) for(%i=0;%i<10;%i++) %mat[-1+%mats++] = "Metal";
	if($EOTW::Day < 25) for(%i=0;%i<14;%i++) %mat[-1+%mats++] = "Stone"; 
	if($EOTW::Day >= 40 && $EOTW::Day < 55) for(%i=0;%i<$EOTW::SpawnSturdium;%i++) %mat[-1+%mats++] = "Sturdium";
	if($EOTW::Day < 15) for(%i=0;%i<4;%i++) %mat[-1+%mats++] = "Vine";
	if($EOTW::Day < 15) for(%i=0;%i<20;%i++) %mat[-1+%mats++] = "Wood";
	if($EOTW::Day < 55 && $EOTW::Day >= 12) for(%i=0;%i<8;%i++) %mat[-1+%mats++] = "Copper";
	if($EOTW::Day < 55 && $EOTW::Day >= 17) for(%i=0;%i<6;%i++) %mat[-1+%mats++] = "Lead";
	
	if(!isObject(Gatherables) || Gatherables.getCount() < $Server::Pref::EOTWMaxGatherables)
		%mat = %mat[getRandom(0, 199)]; if(%mat !$= "") spawnGatherableRaycast(%mat);

	if (%mat $= "" && getRandom(0,100) == 1 && Chests.getCount() < 25)
	{
		spawnChestRaycast();
	}
	
	$GatherableLoop = schedule(48, 0, "GatherableLoop");
}

function spawnGatherable(%mat,%pos,%pickupSize)
{
	if (%pos $= "")
		%pos = (getRandom(-200, 199) / 2 + 0.25) SPC (getRandom(-200, 199) / 2 + 0.25) SPC 0.1;
		
	if (%pickupSize || getRandom(0,100) < 3)
	{
		%data = EOTW_CreateBrick(EnvMaster, brick1x1Data, %pos, $EOTW::Colors["::"@%mat]); %brick = getField(%data, 0);
		%pos = vectorAdd(%pos,"0 0 0.5");
		%pickupSize = 1;
	}
	else
	{
		%data = EOTW_CreateBrick(EnvMaster, brick1x1FData, %pos, $EOTW::Colors["::"@%mat]); %brick = getField(%data, 0);
	}
	if(getField(%data, 1)) { %brick.delete(); return; }
	%brick.material = %mat;
	%brick.pickupSize = %pickupSize;
	switch$(%mat)
	{
		case "Glass":
			%brick.collectTime = 8000;
			%brick.destroyOnDay = 35;
			%brick.isCollectable = 1;
			%brick.forceVolume = 20;
		case "Grass":
			%brick.collectTime = 500;
			%brick.destroyOnDay = 5;
			%brick.isCollectable = 1;
			%brick.forceVolume = 5;
		case "Metal":
			%brick.collectTime = 12000; //15000
			%brick.destroyOnDay = 45;
			%brick.isCollectable = 1;
			%brick.forceVolume = 100;
		case "Stone":
			%brick.collectTime = 4000; //5000
			%brick.destroyOnDay = 25;
			%brick.isCollectable = 1;
			%brick.forceVolume = 125;
		case "Sturdium":
			%brick.collectTime = 24000; //30000
			%brick.destroyOnDay = 55;
			%brick.isCollectable = 1;
			%brick.forceVolume = 50;
		case "Vine":
			%brick.collectTime = 1000;
			%brick.destroyOnDay = 15;
			%brick.isCollectable = 1;
			%brick.forceVolume = 5;
		case "Wood":
			%brick.collectTime = 2000;
			%brick.destroyOnDay = 15;
			%brick.isCollectable = 1;
			%brick.forceVolume = 1000;
		case "Copper":
			%brick.collectTime = 12500;
			%brick.destroyOnDay = 40;
			%brick.isCollectable = 1;
			%brick.forceVolume = 15;
		case "Lead":
			%brick.collectTime = 16500;
			%brick.destroyOnDay = 50;
			%brick.isCollectable = 1;
			%brick.forceVolume = 25;
	}
	
	if (%pickupSize == 1)
	{
		%brick.collectTime *= 2;
		%brick.forceVolume *= 3;
	}

	Gatherables.add(%brick);
}

//TODO: prevent messing with the raycast by height
function spawnGatherableRaycast(%mat)
{
	%pl = %cl.player; //Note to self - This works, and spawns a block where the player is looking.
						//What we need to do now is to set a specific point and raycast it onto a block. Need to figure out what %for is.
	%eye = (getRandom(0, 1664) / 2 + 0.25) SPC (getRandom(0, 1664) / 2 + 0.25) SPC 128;
	%dir = "0 0 -1";
	%for = "0 1 0";
	%face = getWords(vectorScale(getWords(%for, 0, 1), vectorLen(getWords(%dir, 0, 1))), 0, 1) SPC getWord(%dir, 2);
	%mask = $Typemasks::fxBrickAlwaysObjectType | $Typemasks::TerrainObjectType;
	%ray = containerRaycast(%eye, vectorAdd(%eye, vectorScale(%face, 256)), %mask, %this);
	//echo(%eye TAB %dir TAB %for TAB %face TAB %mask TAB %ray);
	%pos = getWord(%ray,1) SPC getWord(%ray,2) SPC (getWord(%ray,3) + 0.1);
	if(isObject(%hit = firstWord(%ray)))
	{
		if (%hit.getClassName() !$= "FxPlane" && strPos(%hit.getDatablock().uiName,"Ramp") > -1) //To prevent from spawned bricks from being invis in ramps.
			%pos = vectorAdd(%pos,"0 0 0.4");
		spawnGatherable(%mat,%pos,0); //EOTW_CreateBrick(EnvMaster, brick1x1Data, %pos, $EOTW::Colors["::"@%mat]);
	}
}

//%brick.canBeFireballed   // - - Implement this
//%brick.collectTime (in MS)
//%brick.destroyOnDay   //Collectable spawns only; don't apply this to built bricks
//%brick.forceVolume
//%brick.isBreakable   // - - - - Implement this
//%brick.isCollectable
//%brick.isFlammable   // - - - - Implement this
//%brick.isIgnitable   // - - - - Implement this
//%brick.material
//%brick.smashable   // - - - - - Implement this
//%brick.unrecolorable   // - - - Implement this

function getMaterialCount(%mat)
{
	%cnt = 0;
	for (%i = 0; %i<Gatherables.getCount(); %i++)
		if (Gatherables.getObject(%i).material $= %mat)
			%cnt++;
			
	return %cnt;

}
