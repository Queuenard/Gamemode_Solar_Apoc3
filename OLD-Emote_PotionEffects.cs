datablock ParticleData(HealParticle)
{
   dragCoefficient      = 5.0;
   gravityCoefficient   = -0.2;
   inheritedVelFactor   = 0.0;
   constantAcceleration = 0.0;
   lifetimeMS           = 1000;
   lifetimeVarianceMS   = 500;
   useInvAlpha          = false;
   textureName          = "./images/heal";
   colors[0]     = "1.0 1.0 1.0 1";
   colors[1]     = "1.0 1.0 1.0 1";
   colors[2]     = "0.0 0.0 0.0 0";
   sizes[0]      = 0.4;
   sizes[1]      = 0.6;
   sizes[2]      = 0.4;
   times[0]      = 0.0;
   times[1]      = 0.2;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(HealEmitter)
{
   ejectionPeriodMS = 35;
   periodVarianceMS = 0;
   ejectionVelocity = 0.5;
   ejectionOffset   = 1.0;
   velocityVariance = 0.49;
   thetaMin         = 0;
   thetaMax         = 120;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvance = false;
   particles = "HealParticle";

   uiName = "Emote - Heal";
};

datablock ShapeBaseImageData(HealImage)
{
	shapeFile = "base/data/shapes/empty.dts";
	emap = false;

	mountPoint = $HeadSlot;

	stateName[0]					= "Ready";
	stateTransitionOnTimeout[0]		= "FireA";
	stateTimeoutValue[0]			= 0.01;

	stateName[1]					= "FireA";
	stateTransitionOnTimeout[1]		= "Done";
	stateWaitForTimeout[1]			= True;
	stateTimeoutValue[1]			= 0.350;
	stateEmitter[1]					= HealEmitter;
	stateEmitterTime[1]				= 0.350;

	stateName[2]					= "Done";
	stateScript[2]					= "onDone";
};
function HealImage::onDone(%this,%obj,%slot)
{
	%obj.unMountImage(%slot);
}

datablock ParticleData(RegenHealParticle)
{
   dragCoefficient      = 5.0;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.5;
   constantAcceleration = 0.0;
   lifetimeMS           = 500;
   lifetimeVarianceMS   = 250;
   useInvAlpha          = false;
   textureName          = "./images/smallheal";
   colors[0]     = "1.0 1.0 1.0 1";
   colors[1]     = "0.0 0.0 0.0 0";
   colors[2]     = "0.0 0.0 0.0 0";
   colors[3]     = "0.0 0.0 0.0 0";
	  
   sizes[0]      = 0.2;
   sizes[1]      = 0.4;
   sizes[2]      = 0.2;
   times[0]      = 0.0;
   times[1]      = 0.2;
   times[2]      = 0.5;
};

datablock ParticleEmitterData(RegenHealEmitter)
{
   ejectionPeriodMS = 35;
   periodVarianceMS = 5;
   ejectionVelocity = 0.25;
   ejectionOffset   = 1.0;
   velocityVariance = 0.24;
   thetaMin         = 0;
   thetaMax         = 120;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvance = false;
   particles = "RegenHealParticle";

   uiName = "Emote - Regenerate";
};

datablock ShapeBaseImageData(RegenHealImage)
{
	shapeFile = "base/data/shapes/empty.dts";
	emap = false;

	mountPoint = $HeadSlot;

	stateName[0]					= "Ready";
	stateTransitionOnTimeout[0]		= "FireA";
	stateTimeoutValue[0]			= 0.01;

	stateName[1]					= "FireA";
	stateTransitionOnTimeout[1]		= "Done";
	stateWaitForTimeout[1]			= True;
	stateTimeoutValue[1]			= 0.15;
	stateEmitter[1]					= RegenHealEmitter;
	stateEmitterTime[1]				= 0.15;

	stateName[2]					= "Done";
	stateScript[2]					= "onDone";
};
function RegenHealImage::onDone(%this,%obj,%slot)
{
	%obj.unMountImage(%slot);
}

//Speed

datablock ParticleData(SpeedUpParticle)
{
   dragCoefficient      = 5.0;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.5;
   constantAcceleration = 0.0;
   lifetimeMS           = 500;
   lifetimeVarianceMS   = 250;
   useInvAlpha          = false;
   textureName          = "./images/speed";
   colors[0]     = "0.75 0.75 0.75 1";
   colors[0]     = "0.75 0.75 0.75 0.5";
   colors[1]     = "0.25 0.25 0.25 1";
   colors[2]     = "0.0 0.0 0.0 0";
	  
   sizes[0]      = 0.4;
   sizes[1]      = 0.6;
   sizes[2]      = 0.2;
   times[0]      = 1.0;
   times[1]      = 0.8;
   times[2]      = 1.2;
};

datablock ParticleEmitterData(SpeedUpEmitter)
{
   ejectionPeriodMS = 40;
   periodVarianceMS = 5;
   ejectionVelocity = 0.997;
   ejectionOffset   = 1.0;
   velocityVariance = 0.49;
   thetaMin         = 0;
   thetaMax         = 120;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvance = false;
   particles = "SpeedUpParticle";

   uiName = "Emote - Speed Powerup";
};

datablock ShapeBaseImageData(SpeedUpImage)
{
	shapeFile = "base/data/shapes/empty.dts";
	emap = false;

	mountPoint = $HeadSlot;

	stateName[0]					= "Ready";
	stateTransitionOnTimeout[0]		= "FireA";
	stateTimeoutValue[0]			= 0.01;

	stateName[1]					= "FireA";
	stateTransitionOnTimeout[1]		= "Done";
	stateWaitForTimeout[1]			= True;
	stateTimeoutValue[1]			= 10.0;
	stateEmitter[1]					= SpeedUpEmitter;
	stateEmitterTime[1]				= 10.0;

	stateName[2]					= "Done";
	stateScript[2]					= "onDone";
};
function SpeedUpImage::onDone(%this,%obj,%slot)
{
	%obj.unMountImage(%slot);
}
